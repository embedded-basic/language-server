# Embedded BASIC - Language Server

Dieses Projekt enthält den Quellcode zur Bachelor-Arbeit von Stefan Woyde:
> "Implementierung eines Language-Servers für Source-Code-Editoren" (Registriernummer: AI-2021-BA-010)

## Vorraussetzungen

* `.NET 5.0 SDK`
* Visual Studio Code oder auch Visual Studio 2019
