﻿using EmbeddedBasic.LanguageServer.Syntax;

namespace EmbeddedBasic.LanguageServer.Cli
{
    static class Program
    {
        static void Main(string[] args)
        {
            var parser = new EbParser();

            var filename = "test.bas";
            var inputDirpath = "../src";
            var outputDirpath = "../log";

            var syntaxTree = parser.Parse(inputDirpath, filename);

            syntaxTree.WriteErrorLog(outputDirpath, filename + ".log");
            new CodeWriter().WriteFile(syntaxTree, outputDirpath, filename => filename.Replace(".bas", ".code.bas"));
            new TreeListWriter().WriteFile(syntaxTree, outputDirpath);
        }
    }
}
