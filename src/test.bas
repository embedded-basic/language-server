import System.Analog from "C:\\Test"
// import System.Analog
// 4 + 2 * 5 - 7 + 3 / 6 // ((4 + (2 * 5)) - 7) + (3 / 6)
// 4 + 2 * 5 - (7 + 3) / 6 // (4 + (2 * 5)) - ((7 + 3) / 6)

#include "util"

//#uiui

//uiui

#define XXX(arg1, arg2) arg1(arg2, "Test", test)

// `TestFunction(1349, "Test", test)`
XXX(TestFunction, 1349)

//#define Test1
//#define Test2
//#define Test3

#ifdef Test1

    #ifdef Test2

        #ifdef Test3
        a = 4 + 2 * 5 - (7 + 3) / 6 // (4 + (2 * 5)) - ((7 + 3) / 6)
        #else
        b = 4 + 2 * 5 + 7 +30* 6 // 4 + 2 * 5 + 7 +30* 6
        #endif

    #else

        #ifdef Test3
        c = 4 + 2 * 5 - (7 + 3) / 6 // (4 + (2 * 5)) - ((7 + 3) / 6)
        #else
        d = 4 + 2 * 5 + 7 +30* 6 // 4 + 2 * 5 + 7 +30* 6
        #endif

    #endif

#else

    #ifdef Test2

        #ifdef Test3
        e = 4 + 2 * 5 - (7 + 3) / 6 // (4 + (2 * 5)) - ((7 + 3) / 6)
        #else
        f = 4 + 2 * 5 + 7 +30* 6 // 4 + 2 * 5 + 7 +30* 6
        #endif

    #else

        #ifdef Test3
        g = 4 + 2 * 5 - (7 + 3) / 6 // (4 + (2 * 5)) - ((7 + 3) / 6)
        #else
        h = 4 + 2 * 5 + 7 +30* 6 // 4 + 2 * 5 + 7 +30* 6
        j = 4 + 2 * (5 + 7) +30* 6
        #endif

    #endif

#endif

int a = 2

// A comment before node start
function int Sum(int a, int b)      // A comment after node start
    byte c = %11011, d = $90AC
    char str1[0] = "a", str2[2] = "b"

Start:

    loop
        result = c + d + low + SOME_CONSTANT

        while a<10
            result = a + b + high

            for x=100.5 to 0.5 step -2.0 // iterate down float
                result = true

                if a > b then           // first check a>b
                    a = b
                elseif a > c then       // then check a>c
                    a = c
                else                    // else set a=0
                    a = 0
                endif

                if a > 10 then a=0 : b=1
                if a > 10 then a=0 : goto lab1
                if a > 10 then a=0 : foo() : exit
                if a > 10 return                      // conditional return from a function
                if a > 10 exit                        // conditional exit from a loop
                if a > 10 goto lab1                   // conditional goto

                select a                // select a
                    case 1              // case a=1
                        a=2
                    case 2,3            // case a=2 or 3
                        a=a+1
                    case 4 to 7         // case a=4,5,6,7
                        a=a+2
                    case else           // case default
                        a=1
                endselect

                result = false
            endfor
        endwhile
    endloop

    print #1, "Test", str1[0], format(1), using("0#.##", 2)

    on Cmd goto Cmd0, Cmd1, Cmd2
    on n call foo0, foo1, foo2

    int b = 2

    wait rxd0 > 5, 1, timeout

    doMagic(
        2, // Erläuterung zum ersten Parameter
        "foo", // ...
        bar) // ...

    return a + b                        // return the sum of a and b
endfunction
