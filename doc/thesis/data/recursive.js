const TokenKind = {
    PLUS_SYM: 1,
    AST_SYM: 2,
    ID: 3
}

class Expr {
    constructor() { }

    toCode() { return ""; }
}

class BinaryExpr extends Expr {

    get operator() { return this._operator; }
    get operand1() { return this._operand1; }
    get operand2() { return this._operand2; }

    constructor(operand1, operator, operand2) {
        super();
        this._operand1 = operand1;
        this._operator = operator;
        this._operand2 = operand2;
    }

    toCode() {
        return ("(" +
            this._operand1.toCode() + " " +
            this._operator.toCode() + " " +
            this._operand2.toCode() + ")");
    }
}

class Token extends Expr {

    get tokenKind() { return this._tokenKind; }
    get lexeme() { return this._lexeme; }

    constructor(tokenKind, lexeme) {
        super();
        this._tokenKind = tokenKind;
        this._lexeme = lexeme;
    }

    toCode() { return this._lexeme; }
}

class Parser {

    // ...

    constructor(tokens) { this.tokens = tokens; }

    consume() { return this.tokens.shift(); }

    lookahead() {
        return this.tokens.length > 0 ? this.tokens[0] : null;
    }

    expr2(id1) {
        let asterisk = this.lookahead();
        if (asterisk != null) {
            if (asterisk.tokenKind == TokenKind.AST_SYM) {
                this.consume();
                if (this.lookahead().tokenKind == TokenKind.ID) {
                    return new BinaryExpr(id1, asterisk,
                        this.expr2(this.consume()));
                }
            }
        }
        return id1;
    }

    expr1(id1) {
        while (true) {
            let plus = this.lookahead();
            if (plus == null) break;
            if (plus.tokenKind == TokenKind.PLUS_SYM) {
                this.consume();
                if (this.lookahead().tokenKind == TokenKind.ID) {
                    return new BinaryExpr(id1, plus,
                        this.expr1(this.consume()));
                }
            } else id1 = this.expr2(id1);
        }
        return id1;
    }

    parse() {
        return this.lookahead().tokenKind == TokenKind.ID ?
            this.expr1(this.consume()) : null;
    }
}

let parser = new Parser([
    new Token(TokenKind.ID,         "id1"),
    new Token(TokenKind.AST_SYM,    "*"),
    new Token(TokenKind.ID,         "id2"),
    new Token(TokenKind.PLUS_SYM,   "+"),
    new Token(TokenKind.ID,         "id3"),
    new Token(TokenKind.AST_SYM,    "*"),
    new Token(TokenKind.ID,         "id4"),
    new Token(TokenKind.PLUS_SYM,   "+"),
    new Token(TokenKind.ID,         "id5")]);
let expr = parser.parse();
if (expr != null) console.log(expr.toCode());
