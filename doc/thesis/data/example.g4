grammar example;

program:    expr1;

expr1:      expr2 expr1_ext;
expr1_ext:  '+' expr2 expr1_ext | /* epsilon */;

expr2:      expr3 expr2_ext ;
expr2_ext:  '*' expr3 expr2_ext | /* epsilon */;

expr3:      '(' expr1 ')' | IDENTIFIER;

IDENTIFIER: [A-Za-z]+ ;

// "Versteckte" Token sind intern noch abrufbar.
COMMENT:    '//' ~[\r\n]* ('\r' '\n'? | '\n') -> channel(HIDDEN);

// "Geskippte" Token werden einfach verworfen.
WHITESPACE: [ \t\r\n] -> skip;
