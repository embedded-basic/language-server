DOTS		= $(wildcard diagrams/*.dot)
PUMLS		= $(wildcard diagrams/*.puml)
PUMLS		:= $(filter-out diagrams/Format.puml, $(PUMLS))
PUML_TEXES	:= $(DOTS:diagrams/%.puml=diagrams/%.latex)
DOT_PDFS	:= $(DOTS:diagrams/%.dot=diagrams/%.pdf)

# ----------------------------------------------------------------------------

# Genarate PDFs from DOTs
$(DOT_PDFS): $(DOTS)
	dot -Tpdf -o $@ $<

# Genarate latex-files from PlantUML-files
$(PUML_TEXES): $(PUMLS)
	plantuml -tlatex:nopreamble -o "./" -x "$<" "./diagrams"

# ----------------------------------------------------------------------------

generate-diagrams: $(DOT_PDFS) $(PUML_TEXES)

build:
	latexmk -pdf -interaction=nonstopmode \
		-pdflatex="lualatex -synctex=1 -interaction=nonstopmode -file-line-error -shell-escape %O %S" \
		$(shell pwd)/thesis.tex

clean:
	latexmk -c
	rm -f thesis.bbl

# ----------------------------------------------------------------------------

.PHONY: all clean generate-diagrams build

.DEFAULT_GOAL := all

all: clean generate-diagrams build
