\chapter{Umsetzung}
\label{sec:Umsetzung}

Um volle Flexibilität und Anpassbarkeit an die Bedürfnisse der zu analysierenden Sprache zu gewährleisten, wird der gesamte Code von Hand geschrieben. Die Umsetzung erfolgt in der Sprache \Csharp{} Version 9.0 mit dem \enquote{.NET SDK 5.0}.

Der Quellcode des Projekts ist in drei Teile aufgeteilt:
\begin{itemize}
	\item Die Kern-Bibliothek zielt auf das \enquote{.NET Standard 2.0} und ist damit auch auf dem auf Windows beschränkten \enquote{.NET Framework} lauffähig (\cite{website:net_std_2_0}). Diese Bibliothek enthält die eigentliche Logik.
	\item Ein Kommandozeilenprogramm für die direkte Auswertung von Quelldateien.
	\item Eine Test-Bibliothek für automatisierte Tests einzelner Features.
\end{itemize}

Für die größeren Komponenten wurden jene Methoden, die von der umzusetzenden Sprache unabhängig sind, in Basisklassen ausgelagert. Konkret betrifft das den Lexer, den Parser und den Syntaxbaum.

% ------------------------------------------------------------------------------
\section{Lexer}

Grundsätzlich liefert der Lexer eine Menge von Token. Da noch ein paar zusätzliche Funktionen nötig sind, gibt es als Verallgemeinerung eine Schnittstelle.

\subsection{\texttt{ITokenProvider}}

Die Schnittstelle \enquote{\texttt{ITokenProvider}} stellt die abstrakte Token-Quelle für einen Parser dar, sie beinhaltet deshalb die Schnittstelle \enquote{\texttt{IEnumerable<Token>}}. Zusätzlich dazu ist Folgendes enthalten:

\begin{itemize}
	\item Der im Aufbau befindliche Syntaxbaum
	\item Die Informationen zur derzeit in Bearbeitung befindlichen Quelldatei inklusive Quelltext
	\item Die Möglichkeit, eine weitere Quelldatei in die Verarbeitung einzufügen
	\item Die Möglichkeit, Kommentartext abzuholen, der zwischen den verschieden Token gesammelt wird
\end{itemize}

\subsection{Basis-Lexer}

Die Möglichkeiten, die der Basis-Lexer enthält sind:

\begin{itemize}
	\item \texttt{AddKeyword}: Registrierung von Schlüsselwörter bei Initialisierung des Lexers
	\item \texttt{NoteError}: Registrierung von Fehlern während der Analyse
	\item \texttt{PeekNextChar}: Prüfen eines einzelnen Zeichens unter Angabe eines Offsets relativ zur Endposition des vorigen Tokens
	\item \texttt{SeekNextChar}: Wie \enquote{\texttt{PeekNextChar}}, wird aber bei Erreichen des Quelldatei-Endes einen Fehler vermerken
	\item \texttt{AppendComment}, \texttt{ResetComment}, \texttt{ConsumeComment}: Verwaltung des derzeit gesammelten Kommentartextes
	\item \texttt{Set}, \texttt{SetLinebreak}, \texttt{Discard}, \texttt{DiscardLinebreak}, \texttt{TrySetLinebreak}, \texttt{TrySetKeyword}: Die Verwaltung erkannter oder verworfener Muster
\end{itemize}

Der Lexer sichert den Stand der Datei-\-Abarbeitung in einem eigens dafür vorgesehen Quell\-datei-\-Zustand (\texttt{SourceFile.State}). Bei Inkludieren einer anderen Quelldatei findet dann eine Verschachtelung der Zustände statt. Der Quelldatei-Zustand sichert das letzte gelesene Token und dessen Endposition im Quelltext als Referenz für die nächste Ermittlung des nächsten Tokens.

\subsection{\texttt{EbLexer}}

Der Lexer für \enquote{Embedded BASIC} muss \enquote{nur} noch die Methode beisteuern, die die Token liefert. Dazu nutzt der Lexer einen konkret an die Sprache angepassten DFA. Einige wenige Terminal-Symbole werden anders behandelt:
\begin{description}
	\item [Schlüsselwörter:] Werden indirekt über andere Terminal-Symbole erfasst, wobei nachträglich geprüft wird, ob es sich um ein Schlüsselwort handelt. Alle Schlüsselwörter müssen zu Beginn bei der Erstellung des Lexer-Objektes registriert werden (siehe Abschnitt~\ref{lis:lexer_kw_recog}).
	\item [Zeilenumbrüche:] Werden als eigene Terminal-Symbole erfasst und separat vermerkt, um die Zeileninformation zu sichern. Ihre Auslieferung als Token kann aber vorübergehend unterdrückt werden.
	\item [Kommentare:] Werden als Begleitinformation gesammelt, die separat konsumiert werden können.
\end{description}

\begin{lstlisting}[
	style=cs,
	label={lis:lexer_kw_recog},
	caption={Abschnitt zur Erkennung von Bezeichnern und Schlüsselwörtern},
]
else if (char.IsLetter(ch) || ch == '_')
{
	offset = 0;

	while (PeekNextChar(out ch, ++offset) &&
		(char.IsLetterOrDigit(ch) || ch == '_'))
	{
	}

	if (!TrySetKeyword(offset, out lexeme))
	{
		Set(TokenKind.Identifier, offset, lexeme);
	}
	return true;
}
\end{lstlisting}

% ------------------------------------------------------------------------------
\section{Parser}

\subsection{Basis-Parser}

Die Möglichkeiten, die der Basis-Parser enthält sind:

\begin{itemize}
	\item \texttt{NoteError}: Registrierung von Fehlern während der Analyse
	\item \texttt{LookAhead}, \texttt{TryConsume}, \texttt{Consume}, \texttt{TryConsumeAnyBut}, \texttt{ConsumeWhile}, \texttt{ConsumeUntil}, \texttt{ConsumeUntilIncluding}: Diese Methoden sollen den Zugriff auf anstehende Token ermöglichen. Sie bieten vielfältige Möglichkeiten für den Abruf der Token, und erleichtern die Registrierung von Fehlern.
	\item \texttt{Register}, \texttt{RegisterPrefix}, \texttt{RegisterPostfix}, \\
		\texttt{RegisterInfixLeft}, \texttt{RegisterInfixRight}, \texttt{RegisterLiterals}: \\
		Registrierung von Parselets für den Pratt-Parser-Teil.
	\item \texttt{ParseExpression}: Nutzung des Pratt-Parser-Teils zum Parsen von Expressions.
	\item \texttt{IsAllowed}: Zum Prüfen, ob eine bestimmte Token-Art (\enquote{\texttt{TokenKind}}) erlaubt ist. Anderenfalls wird das Auftreten von Token dieser Art unterdrückt.
\end{itemize}

\subsection{\texttt{EbPreprocessor}}

Der Präprozessor ist selbst bereits ein kleiner Parser, der aus Sicht des eigentlichen Parsers als \texttt{ITokenProvider} auftritt. Er bildet eine Weiche für Token. Es können Token über die Anwendung von Makros eingefügt werden, oder aber bei der Auswertung von \enquote{\texttt{\#ifdef}}s verschluckt werden. Um einen \enquote{\texttt{\#ifdef}}-Block richtig zu verarbeiten nutzt der Präprozessor einen Zähler für die aktiven Verschachtelungen. Damit sind jene Verzweigungen gemeint, deren Token-Inhalte durchgelassen werden. Bei den unterdrückten Verzweigungen reicht ein lokaler Zähler, bis das nächste Token auftritt, das passieren darf.

Der Präprozessor enthält für jedes der Präprozessor-Konstrukte eine Methode, deren Name mit \enquote{\texttt{Parse}} beginnt. Ein Beispiel dafür ist in Listing~\ref{lis:ebpreprocessor_parseinclude} angegeben.

\begin{lstlisting}[
	style=cs,
	label={lis:ebpreprocessor_parseinclude},
	caption={Methode zum Parsen eines \enquote{\texttt{\#include}}},
]
private void ParseInclude(Token includeToken)
{
	if (TryConsume(TokenKind.StringLiteral,
				out var stringToken, ErrorMessages.IncludePath))
	{
		var includeDirective = new IncludeDirective(
			ConsumeComment(), includeToken,
			new StringLiteral(stringToken));
		TokenProvider.IncludeSource(
			includeDirective.ToSourceFile(
				TokenProvider.CurrentSourceFile));
	}
}
\end{lstlisting}

\subsection{\texttt{EbParser}}

Der Parser für \enquote{Embedded BASIC} enthält für jedes der Präprozessor-Konstrukte eine Methode, deren Name mit \enquote{\texttt{Parse}} beginnt.

Die Methode \texttt{ParseStatements} hat eine Besonderheit. Sie sammelt Informationen zu mehr\-eren Anweisungen und ist so aufgebaut, dass sie das Ende des Anweisungsblocks über jene Token-Arten bestimmt, die ihr als Parameter geliefert wurden. Darüberhinaus verschluckt diese Methode Zeilenumbruchs-Token, weil diese nur dazu dienen, die Begrenzungen der einzelnen Anweisungen zu erkennen.

Der Parser enthält auch die Methoden \texttt{SuppressLinebreak} und \\ \texttt{ResetLinebreakSuppression}, die dazu dienen, die Möglichkeit des Auftretens von Zeilenumbrüchen zu steuern.

Ein Beispiel für eine der \texttt{Parse}-Methoden ist in Listing~\ref{lis:ebparser_parsewhile} angegeben.

\begin{lstlisting}[
	style=cs,
	label={lis:ebparser_parsewhile},
	caption={Methode zum Parsen einer bedingten Schleife},
]
private WhileLoop? ParseWhileLoop(Token whileToken)
{
	var expression = ParseExpression();
	TryConsume(TokenKind.Linebreak);
	var comment = ConsumeComment();
	var statements = ParseStatements(TokenKind.EndWhileKeyword);

	if (TryConsume(TokenKind.EndWhileKeyword,
				out var endWhileToken, ErrorMessages.EndWhile))
	{
		return new WhileLoop(comment, whileToken,
						endWhileToken, expression, statements);
	}
	return null;
}
\end{lstlisting}

\subsection{Pratt-Parser}
\label{sec:impl_pratt}

Der Teil des Parsers, der für das Parsen von Expressions zuständig ist, ist ein Pratt-Parser (siehe Kapitel~\ref{sec:fund_pratt}). Es werden zu Beginn registrierte Parselets genutzt \\ (\texttt{PrefixParselet}/\texttt{InfixParselet}). Bei Erzeugung der Parser-Instanz werden die Parselets mit ihrem jeweiligen, auslösenden Terminal-Symbole registriert (siehe Listing~\ref{lis:ebparser_ctor}).

\begin{lstlisting}[
	style=cs,
	label={lis:ebparser_ctor},
	caption={\texttt{EbParser}-Konstruktor},
]
public EbParser() : base(tokenProvider: new EbPreprocessor())
{
	RegisterLiterals(
		BooleanLiteral.ParseletInfo, BitLiteral.ParseletInfo,
		BinNumberLiteral.ParseletInfo, HexNumberLiteral.ParseletInfo,
		IntegerLiteral.ParseletInfo, DecimalLiteral.ParseletInfo,
		DateLiteral.ParseletInfo, TimeLiteral.ParseletInfo,
		CharLiteral.ParseletInfo, StringLiteral.ParseletInfo,
		DeviceNameLiteral.ParseletInfo, DeviceIdLiteral.ParseletInfo);

	Register(TokenKind.Identifier, _identifier = new(this));
	Register(TokenKind.ParenthesisLeft, new GroupParselet(this));
	Register(TokenKind.ParenthesisLeft, _call = new(this));

	RegisterPrefix(OperatorPrecedence.Prefix,
		TokenKind.PlusSymbol, TokenKind.MinusSymbol);

	RegisterInfixLeft(OperatorPrecedence.Comparison,
		TokenKind.LessThanSymbol, TokenKind.GreaterThanSymbol,
		TokenKind.EqualEqualSymbol,
		TokenKind.LessThanGreaterThanSymbol);

	RegisterInfixLeft(OperatorPrecedence.Sum,
		TokenKind.PlusSymbol, TokenKind.MinusSymbol);

	RegisterInfixLeft(OperatorPrecedence.Product,
		TokenKind.AsteriskSymbol, TokenKind.SlashSymbol,
		TokenKind.LessThanLessThanSymbol,
		TokenKind.GreaterThanGreaterThanSymbol);
}
\end{lstlisting}

Es gibt zwei Arten von Parselets, das \texttt{PrefixParselet} und das \texttt{InfixParselet}. Einem \texttt{InfixParselet} ist immer auch ein Rang zugeordnet, der für den Operator steht, den das Parselet umsetzt. Für ein \texttt{InfixParselet} muss auch vermerkt werden, ob dessen Operator links- oder rechts-assoziativ ist. Die Assoziativität sagt aus, in welcher Richtung der Beginn der Auswertung einer Gruppe gleichartiger Operationen liegt.

Das Hinzufügen eines neuen Rechen-Operators, wie beispielsweise des \enquote{\^{}} für die Exponentiation, wäre somit recht leicht möglich. Für den Operator müsste zunächst ein Rang festgelegt oder ergänzt werden. Mit diesem Rang müsste dann für den Operator ein Parselet registriert werden (siehe Listing~\ref{lis:ebparser_ctor_ext}). In der Klasse \enquote{BinaryExpression} müsste der Operator dann abschließend noch in die Aufzählung \texttt{BinaryOperatorKind} aufgenommen werden.

\begin{lstlisting}[
	style=cs,
	label={lis:ebparser_ctor_ext},
	caption={Registrierung eines neuen Operators},
]
RegisterInfixRight(
	OperatorPrecedence.Exponentiation, TokenKind.CaretSymbol);
\end{lstlisting}

% ------------------------------------------------------------------------------
\section{Syntaxbaum}

\subsection{Basis-Syntaxbaum}

Basis-Syntaxbaum sieht ein \texttt{Dictionary} vor, welches Fehlermeldungen nach dem Namen der Quelldatei auflisten, in der sie auftraten. Er bietet einen Weg, um Syntax-Knoten unter einem Namen als Symbol zu sichern und abzurufen.

Der Basis-Syntax-Knoten führt eine Liste von Kind-Knoten, sortiert nach deren Auftreten im Quellcode. Das ermöglicht, ein bestimmtes Token über die Angabe der Position in der ursprünglichen Quelldatei zu finden. Der Basis-Syntaxbaum bietet dafür geeignete Methoden mit dem Namen \texttt{TryGetToken} an.

\subsection{\texttt{EbSyntaxTree}}

Der abstrakte Syntaxbaum für \enquote{Embedded BASIC} ergänzt den Basis-Baum nur noch um einen konkreten Symbol-Speicher.

% ------------------------------------------------------------------------------
\section{Tree-Walker}

Die Tree-Walker im Language-Server nutzen das Visitor-Pattern. Die dafür vorgesehene Basisklasse \texttt{SyntaxNodeVisitor} hat verschiedene Methoden, deren Namen mit \enquote{\texttt{Visit}} beginnen und einem jeweiligen konkreten Syntaxknoten-Typen zugeordnet ist. Als Beispiel ist in Listing~\ref{lis:syntax_node_visitor_visit} die Methode zu sehen, die für Funktionsdeklarationen als Knoten zuständig ist.

\begin{lstlisting}[
	style=cs,
	label={lis:syntax_node_visitor_visit},
	caption={Abschnitt im Tree-Walker für Funktionsdeklarationen},
]
protected internal virtual void VisitFunctionDeclaration(
	FunctionDeclaration functionDeclaration)
	=> this.VisitChilds(functionDeclaration);
\end{lstlisting}

Jeder der Knoten implementiert seinerseits eine Methode, die den \texttt{SyntaxNodeVisitor} als Besucher akzeptiert. Passend zum vorigen Beispiel hier die akzeptierende Methode des Knotens für Funktionsdeklarationen (siehe Listing~\ref{lis:syntax_node_accept}).

\begin{lstlisting}[
	style=cs,
	label={lis:syntax_node_accept},
	caption={Abschnitt im Knoten \texttt{FunctionDeclaration} für den Tree-Walker},
]
protected internal override void Accept(
	SyntaxNodeVisitor visitor)
	=> visitor.VisitFunctionDeclaration(this);
\end{lstlisting}

Die grundlegenden Tree-Walker dienen zu Ausgabe des Inhalts des abstrakten Syntaxbaums.

\begin{description}
	\item[Tree-Writer: ] Der Tree-Writer gibt den Syntaxbaums in Form einer eingerückten Auflistung der Knotenbeschreibungen wieder.
	\item[Code-Writer: ] Der Code-Writer gibt den Syntaxbaum in Form der ursprünglich geparsten Sprache wieder. Bislang wird er nur genutzt, um auf einfache Weise zu prüfen, welche Inhalte vom Programm verstanden wurden.
\end{description}

\section{Semantic-Analyzer}

Der \texttt{SemanticAnalyzer} ist ein Tree-Walker für den Syntaxbaum, er durchsucht ihn nach Deklarationen von Symbolen, Konstanten, Variablen und Funktionen. Er wird diese Deklarationen geordnet nach ihrem Gültigkeitsbereich gruppieren und verschachteln. Das Ergebnis dieses Prozesses ist ein semantischer Baum. Der kann grundsätzlich bereits die mehrfache Verwendung von Symbolen, Variablen, Konstanten und Funktionen sichtbar machen. In diesen Fällen wird eine Fehlermeldung vermerkt.

\subsection{Semantic-Tree}

Jeder semantische Knoten besitzt einen Namen, über den der Knoten eindeutig bei seinem Eltern-Knoten gefunden werden kann. Der Semantik-Baum macht davon Gebrauch und bietet zur Suche eines Semantik-Knotens Methoden mit dem Namen \texttt{TryGetDeclaration} an. Als Ausgangspunkt zur Suche wird eine Position im Quelltext oder ein Token akzeptiert. Über die Methode \texttt{TryGetNode} lässt sich ein Semantik-Knoten über einen untergeordneten Syntax-Knoten finden.

\subsection{Goto Definition}

Die hauptsächliche und fortgeschrittene Funktion von Semantic-Analyzer und Semantic-Tree ist das Bereitstellen der Informationen, die für ein \enquote{Goto Definition} benötigt werden. Das Abholen der benötigten Informationen wird hier nun beschrieben.

Bei der lexikalischen Analyse werden zu den Zeilen die absoluten Positionsinformationen gesammelt. Die Klasse \texttt{TextLineList} hält diese Informationen, und bietet eine Umsetzung der Positionsinformationen in beide Richtungen an (siehe Listing~\ref{lis:textlinelist_trygetabsolute}).

\begin{lstlisting}[
	style=cs,
	label={lis:textlinelist_trygetabsolute},
	caption={Ermittlung der absoluten Position zu einer Zeile und Spalte},
]
public bool TryGetAbsolute(
	int line, int character, out int absolute)
{
	// Non-zero numbers are used for line/character combinations.
	if (line > 0 && line <= _items.Length)
	{
		absolute = _items[line - 1].Absolute + character - 1;
		return true;
	}
	absolute = -1;
	return false;
}

public bool TryGetLinePosition(
	int absolute, out LinePosition linePosition)
{
	// Custom Binary search for a value.
	if (_items.TryGetValueAtOrAbove(_size, absolute,
				out var textLine))
	{
		linePosition = new(
			textLine.Line, 1 + absolute - textLine.Absolute);
		return true;
	}
	linePosition = LinePosition.Invalid;
	return false;
}
\end{lstlisting}

Im Syntaxbaum enthalten alle Knoten abolute Positionensinformationen (Startpunkt und Endpunkt) und den Bezug zu dem Quelltext in dem sie auftraten. Und alle Kindknoten im Syntaxbaum sind nach der absoluten Position sortiert. So wird es möglich, genau den Token zu finden, der zu einer Positionsangabe passt. Vom Syntaxbaum aus wird die Position aufgelöst um dann damit im Wurzelknoten zu suchen (siehe Listing~\ref{lis:syntax_tree_trygettoken}).

\begin{lstlisting}[
	style=cs,
	label={lis:syntax_tree_trygettoken},
	caption={Auffinden eines Tokens nach Zeile und Spalte über den Syntaxbaum},
]
public bool TryGetToken(int line, int character,
	[MaybeNullWhen(false)] out Token token)
{
	if (SourceFile.TextLines.TryGetAbsolute(line, character,
				out var absolute) &&
		Root.TryGetToken(absolute, out token))
	{
		return true;
	}
	token = null;
	return false;
}}
\end{lstlisting}

Vom Syntax-Wurzelknoten aus wird dann rekursiv absteigend in allen Kindknoten die Suche fortgesetzt, bis das Token gefunden wurde (siehe Listing~\ref{lis:syntax_node_trygettoken}).

\begin{lstlisting}[
	style=cs,
	label={lis:syntax_node_trygettoken},
	caption={Auffinden eines Tokens nach absoluter Position über die Syntaxknoten},
]
public virtual bool TryGetToken(int absolute,
	[MaybeNullWhen(false)] out Token token)
{
	if (_childNodesByPosition.TryGetValue(absolute,
				out var childNode) &&
		childNode.TryGetToken(absolute, out token))
	{
		return true;
	}
	token = null;
	return false;
}
\end{lstlisting}

Dieser Mechanismus wird letztlich vom Semantic-Tree genutzt, um das Token zu finden. Damit wird dann die Suche nach dem semantischen Knoten fortgesetzt, der für die Deklaration steht, die auf die sich das gefundene Token bezieht (siehe Listing~\ref{lis:semantic_tree_trygetdeclaration1}).

\begin{lstlisting}[
	style=cs,
	label={lis:semantic_tree_trygetdeclaration1},
	caption={Auffinden eines Tokens einer Dekaration nach Zeile und Spalte},
]
public bool TryGetDeclaration(
	int line,
	int character,
	[MaybeNullWhen(false)] out Token token,
	[MaybeNullWhen(false)] out SemanticNode declaration)
{
	if (SyntaxTree.TryGetToken(line, character, out token) &&
			TryGetDeclaration(token, out declaration))
	{
		return true;
	}
	declaration = null;
	return false;
}
\end{lstlisting}

An jedem semantischen Knoten ist der Syntaxknoten vermerkt, der das semantische Konstrukt definiert. Bereits beim Anlegen des Semantic-Tree wird für jeden semantischen Knoten vermerkt, welche Syntaxknoten unter ihm auftreten. Dabei wird ein \texttt{Dictionary} benutzt, das jedem Syntaxknoten genau einen semantischen Knoten zuordnet (siehe Listing~\ref{lis:semantic_tree_finish}).

\begin{lstlisting}[
	style=cs,
	label={lis:semantic_tree_finish},
	caption={Abschließende Verknüpfung von Syntax- und Semantik-Knoten},
]
internal void Finish() => Finish(Module);

private void Finish(SemanticNode semanticNode)
{
	// We fill the `_nodeDictionary` to make all
	// semantic nodes searchable by their syntax nodes.
	foreach (var semanticChildNode in semanticNode.ChildNodes)
	{
		Finish(semanticChildNode);
	}
	var syntaxNodes = new Stack<SyntaxNode>();

	if (!_nodeDictionary.ContainsKey(semanticNode.SyntaxNode))
	{
		syntaxNodes.Push(semanticNode.SyntaxNode);

		while (syntaxNodes.TryPop(out var syntaxNode))
		{
			_nodeDictionary.Add(syntaxNode, semanticNode);

			foreach (var syntaxChildNode in syntaxNode.ChildNodes)
			{
				if (!_nodeDictionary.ContainsKey(syntaxChildNode))
				{
					syntaxNodes.Push(syntaxChildNode);
				}
			}
		}
	}
}
\end{lstlisting}

Mit dem zuvor ermittelten Token kann der Semantic-Tree so den semantischen Knoten finden, für den das Token registriert wurde. Ausgehend von diesem semantischen Knoten wird ermittelt, ob ein semantischer Kindknoten mit dem Bezeichner-Namen vorhanden ist (siehe Listing~\ref{lis:semantic_tree_trygetdeclaration2}). Das funktioniert, weil alle jeweiligen semantischen Kindknoten bereits in einer nach Namen sortierten Liste vorliegen.

\begin{lstlisting}[
	style=cs,
	label={lis:semantic_tree_trygetdeclaration2},
	caption={Auffinden eines semantischen Knotens für \enquote{Goto Defintion}},
]
public bool TryGetDeclaration(Token token,
	[MaybeNullWhen(false)] out SemanticNode declaration)
{
	// For now only identifiers may be used.
	if (token.Kind == TokenKind.Identifier)
	{
		var name = token.Lexeme;
		SemanticNode? node;

		if (_nodeDictionary.TryGetValue(token.ParentNode!, out node))
		{
			// We look through the scopes beginning
			// by the one nearest to the given token.
			for (; node != null;
				node = node.ParentNode)
			{
				if (node.ChildNodes.TryGetValue(name, out declaration))
				{
					return true;
				}
			}
		}
	}
	declaration = null;
	return false;
}
\end{lstlisting}

% ------------------------------------------------------------------------------
\section{Ergebnis}
\label{sec:Ergebnis}

Das Hauptziel war, das Feature \enquote{Goto Definition} umzusetzen. Das gelang. Und es wurde auch eine Basis für die Umsetzung weiterer Features geschaffen. Das Prinzip der Tree-Walker (Node-Visitors) bietet Möglichkeiten die für eine weiterführende Analyse oder Optimierung an.
