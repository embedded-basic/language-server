\chapter{Grundlagen}
\label{sec:Grundlagen}

Das Verstehen von Inhalten einer formalen Sprache, zumeist einer kontext-freien Sprache, beginnt mit der syntaktischen Analyse. Diese beinhaltet die lexikalische Analyse. Die Struktur einer Sprache lässt sich in zwei Ebenen einteilen. Die Wortebene, sie wird durch das Teilproblem der lexikalischen Analyse abgedeckt. Und die Phrasenebene, die durch den verbleibenden Teil der syntaktischen Analyse abgedeckt wird (siehe~\cite{website:scannerless_parsing}).

Die lexikalische Analyse muss nicht getrennt von der syntaktischen Analyse erfolgen. Zumeist geschieht das aber, um das Design zu vereinfachen und die Effizienz zu verbessern (siehe \cite{aho2014compilers}, Kapitel 3.1.1). Die Programmteile zur lexikalischen Analyse nennt man Lexer oder auch Scanner (siehe~\cite{website:lexical_analysis}).

Bei der syntaktischen Analyse soll in Zeichenketten, die Syntax einer formalen Sprache, zumeist einer kontext-freien Sprache, erkannt werden. Der Vorgang wird auch als Parsing bezeichnet. Die Programme dafür nennt man Parser. Aus den erkannten Symbolen wird entweder ein Syntaxbaum erstellt, oder aber es wird eine dem jeweiligen Symbol zugewiesene Aktion ausgeführt.

Es gibt zwei Arten von Symbolen (\cite{website:symbols}):

\begin{description}
	\item[Terminal-Symbole:] Sie sind die Symbole der Wortebene und damit elementar. Sie können keine anderen Symbole enthalten.
	\item[Non-Terminal-Symbole:] Sie sind die Symbole der Phrasenebene und stehen für Kombinationen von Symbolen.
\end{description}

Es gibt zwei Kategorien von Parsern (\cite{website:parser_types}, \cite{aho2014compilers}):

\begin{description}
	\item [Bottom-Up-Parser:] Hierbei wird versucht, von einzelnen Terminal-Symbolen ausgehend, übergeordnete Muster zu erkennen. Ihre am weitesten verbreitete Untergruppe sind die LR(\textit{k})-Parser. Das \enquote{L} in LR(\textit{k}) steht für die Verarbeitung der Eingaben von Links nach Rechts. Das \enquote{R} steht für eine umgekehrte \enquote{rechtskanonische Ableitung}. Damit ist die Art gemeint wie, auf dem während der Verarbeitung aufgebauten Stack, Symbol-Muster erkannt werden. Hierbei werden immer die letzten, neuesten Symbole betrachtet. Das \enquote{\textit{k}} sagt aus, um wie viele Token der Parser vorausschauen muss, um Entscheidungen zu treffen. Das wird auch \enquote{Lookahead} genannt.
	\item [Top-Down-Parser:] Hierbei wird, ausgehend vom erwarteten End-Symbol, immer tiefer in die möglichen Verzweigungen hinabgestiegen. Diese absteigen Prüfung endet letztlich bei den Terminal-Symbolen. Ihre am weitesten verbreitete Untergruppe sind die LL(\textit{k})-Parser. Die Bedeutung der Buchstaben ist die gleiche wie bei LR. Abgesehen vom zweiten \enquote{L}, das steht für die Verarbeitung der Eingaben von Rechts nach Links.
\end{description}

% ------------------------------------------------------------------------------
\section{Lexer}

In den Fällen, in denen die lexikalische Analyse ausgelagert wird, lässt sie sich mit einer regulären Sprache beschreiben. Deshalb kommt hierbei ein deterministischer endlicher Automat (DFA, \enquote{Deterministic Finite Automaton}) zu Einsatz. Der DFA soll auf die einzelnen Zeichen einer Zeichenkette reagieren. Jedes Zeichen veranlasst den DFA von seinem derzeitigen Zustand in einen ganz bestimmten Folgezustand zu wechseln. Der Wechsel von einem Endzustand in den Startzustand  bedeutet, dass ein Lexem erkannt wurde. Ein Lexem ist eine konkrete Zeichenkette, die einem Terminal-Symbol der zu parsenden formalen Sprache zugeordnet werden kann. Ein Paar aus Lexem und zugehörigem Terminal-Symbol wird Token genannt (\cite{aho2014compilers}, Kapitel 2.2).

\begin{figure}[!ht]
	\centering
	\caption{Ein DFA für das Schlüsselwort \enquote{for} und alphabetische Identifier}
	\label{fig:DFA}

	\begin{tikzpicture}[
		->, % makes the edges directed
		>=stealth', % makes the arrow heads bold
		node distance=2.5cm,
	]
		\node[state, initial] (q0) {$q_0$};
		\node[state, right of=q0] (q1) {$q_1$};
		\node[state, right of=q1] (q2) {$q_2$};
		\node[state, accepting, right of=q2] (q3) {$q_3$};
		\node[state, accepting, below of=q1] (q4) {$q_4$};
		\draw
			% `for` hit
			(q0) edge[left, anchor=south] node{f} (q1)
			(q1) edge[left, anchor=south] node{o} (q2)
			(q2) edge[left, anchor=south] node{r} (q3)

			% `for` missed
			(q1) edge[below, anchor=center] node{a-n, p-z} (q4)
			(q2) edge[bend left, below, sloped, anchor=north] node{a-q, s-z} (q4)
			(q3) edge[bend left = 50, below, sloped, anchor=north] node{a-z} (q4)

			% identifier hit
			(q0) edge[bend right, below, sloped, anchor=north] node{a-e, g-z} (q4)
			(q4) edge[loop below, anchor=north] node{a-z} (q4)
			;
	\end{tikzpicture}
\end{figure}

Eine ausführliche Beschreibung von DFAs ist in \cite{hopcroft2014introduction}, Kapitel 2.2 zu finden. Die Anwendung von DFAs in Lexern ist in \cite{aho2014compilers}, Kapitel 3.4.1 beschrieben.

% ------------------------------------------------------------------------------
\section{LR-Parser}

% Helper to print the `id` with index number.
\newcommand{\id}[1]{\textbf{id}\textsubscript{#1}}

Die LR-Parser gehören zu den Shift-Reduce-Parsen, und damit zu den Bottom-Up-Parsern. Ihren Kern bilden zwei Arten von Aktionen, Shift und Reduce (siehe \cite{aho2014compilers}, Kapitel 4.6).

\begin{description}
	\item [Shift:] Es wird das derzeit anstehende Token konsumiert und auf dem Stack abgelegt.
	\item [Reduce:] Eine, zur Regel passende, Anzahl an Symbolen wird vom Stack entfernt und auf ein einziges Non-Terminal-Symbol reduziert. Bei Erstellung eines Parsebaums (konkreter Syntaxbaum) stellt dieses neue Symbol einen Knoten dar, der die reduzierten Symbole als Unterknoten enthält.
\end{description}

Bei der Auswahl einer anzuwendenden Aktion kann ein Konflikt auftreten, zwischen Shift \& Reduce oder zwischen zwei verschiedenen Reduce-Aktionen. Solche Konflikte werden unter zusätzlichem Lookahead aufgelöst. Falls das nicht möglich ist, liegt ein Fehler vor.

Für LR-Parser werden üblicherweise Parser-Generatoren eingesetzt, um die zum Teil umfangreichen Parser-Tabellen mit dem Regelwerk nicht von Hand schreiben zu müssen. Anhand einer einfachen Grammatik für Expressions (Abbildung~\ref{equ:expression_grammar_lr}, siehe \cite{aho2014compilers}, Kapitel 4.1.2) sind im Folgenden die LR-Parsing-Schritte angegeben, die bei der Eingabe \enquote{\id{1}~$+$~\id{2}~$*$~\id{3}} durchlaufen wird (Abbildung~\ref{tab:lr_parser_steps}). Das Ergebnis, der daraus resultierende, konkrete Syntaxbaum ist ebenfalls angegeben (Abbildung~\ref{fig:lr_parse_tree}).

\begin{figure}[!ht]
	\centering
	\caption{Eine einfache LR-Grammatik für Expressions}
	\label{equ:expression_grammar_lr}

	\begin{tabular}{lll}
		$E$ & \rightarrow & $E$~$+$~$T$~|~$T$ \\
		$T$ & \rightarrow & $T$~$*$~$F$~|~$F$ \\
		$F$ & \rightarrow & $($~$E$~$)$~|~$id$
	\end{tabular}
\end{figure}

% Compare similar table in the "Dragon Book", Page 253 (id * id + id)

\begin{figure}[!ht]
	\centering
	\caption{LR-Parser-Schritte für Eingabe \enquote{\id{1}~$+$~\id{2}~$*$~\id{3}}}
	\label{tab:lr_parser_steps}

	\begin{tabular}{rllll}
		\toprule
				& Token-Stream					& Stack											& Folgeaktion	& Ursache \\
		\midrule
		(1)		& \id{1}~$+$~\id{2}~$*$~\id{3}	& 												& Shift			& \\
		(2)		& $+$~\id{2}~$*$~\id{3}			& \id{1}										& Reduce		& $F \rightarrow \textbf{id}$ \\
		(3)		& $+$~\id{2}~$*$~\id{3}			& \textit{F}									& Reduce		& $T \rightarrow F$ \\
		(4)		& $+$~\id{2}~$*$~\id{3}			& \textit{T}									& Reduce		& $E \rightarrow T$ \\
		(5)		& $+$~\id{2}~$*$~\id{3}			& \textit{E}									& Shift			& Mehr möglich, kein Accept \\
		(6)		& \id{2}~$*$~\id{3}				& \textit{E}~$+$								& Shift			& \\
		(7)		& \ast~\id{3}					& \textit{E}~$+$~\id{2}							& Reduce		& $F \rightarrow \textbf{id}$ \\
		(8)		& \ast~\id{3}					& \textit{E}~$+$~\textit{F}						& Reduce		& $T \rightarrow F$ \\
		(9)		& \ast~\id{3}					& \textit{E}~$+$~\textit{T}						& Shift			& Mehr möglich, kein Reduce \\
		(10)	& \id{3}						& \textit{E}~$+$~\textit{T}~$*$					& Shift			& \\
		(11)	& 								& \textit{E}~$+$~\textit{T}~$*$~\id{3}			& Reduce 		& $F \rightarrow \textbf{id}$ \\
		(12)	& 								& \textit{E}~$+$~\textit{T}~$*$~\textit{F}		& Reduce 		& $T \rightarrow T * F$ \\
		(13)	& 								& \textit{E}~$+$~\textit{T}						& Reduce		& $E \rightarrow E + T$ \\
		(14)	& 								& \textit{E}									& Accept		& Keine weiteren Möglichkeiten \\
		\bottomrule
	\end{tabular}
\end{figure}

\begin{figure}[!ht]
	\centering
	\caption{Der aus den LR-Parser-Schritten resultierende Parsebaum}
	\label{fig:lr_parse_tree}

	\begin{tikzpicture}[
		tlabel/.style={pos=0.4,right=-1pt,font=\footnotesize\color{red!70!black}}, %
		node distance=1.0cm,
	]
		\node{\textit{E}}
			child { node{\textit{E}}
				child { node{\textit{T}}
					child { node{\textit{F}}
						child { node{\id{1}} }
					}
				}
			}
			child { node{+} }
			child { node{\textit{T}}
				child { node{\textit{T}}
					child { node{\textit{F}}
						child { node{\id{2}} }
					}
				}
				child { node{\ast} }
				child { node{\textit{F}}
					child { node{\id{3}} }
				}
			};
	\end{tikzpicture}
\end{figure}

% \clearpage

% ------------------------------------------------------------------------------
\section{LL-Parser}

Die LL-Parser gehören zu den Top-Down-Parsern. Die kleine Expression-Grammatik aus Abbildung~\ref{equ:expression_grammar_lr} muss für die Verwendung mit LL-Parsern umgeschrieben werden. Das geschieht, um eine unendliche Rekursion bei der Auswertung von der linken Seite her zu vermeiden (\enquote{Links-Rekursion}). Das Ergebnis ist in Abbildung~\ref{equ:expression_grammar_ll} zu sehen (siehe auch \cite{aho2014compilers}, Kapitel 4.4). Die Menge der LL-Grammatiken ist eine echte Teilemenge der Menge der LR-Grammatiken (\cite{aho2014compilers}, Kapitel 4.1). Sie sind weniger ausdrucksstark.

\begin{figure}[!ht]
	\centering
	\caption{Eine einfache LL-Grammatik für Expressions}
	\label{equ:expression_grammar_ll}

	\begin{tabular}{lll}
		$E$		& \rightarrow & $T$~$E'$ \\
		$E'$	& \rightarrow & $+$~$T$~$E'$~|~\epsilon \\
		$T$		& \rightarrow & $F$~$T'$ \\
		$T'$	& \rightarrow & $*$~$F$~$T'$~|~\epsilon \\
		$F$		& \rightarrow & $($~$E$~$)$~|~\textbf{id} \\
	\end{tabular}
\end{figure}

Beim Aufbau von Top-Down-Parsern (aber auch von Bottom-Up-Parsern) sind die Funktionen \enquote{\texttt{FIRST}} und \enquote{\texttt{FOLLOW}} hilfreich. Sie dienen der Auswahl der anzuwendenden Produktionsregel. Ihre Beschreibung erfolgt hier anhand der LL-Beispiel-Grammatik.

\begin{description}
	\item [\texttt{FIRST}:] Das Ergebnis von \texttt{FIRST}$(E')$ ist die Menge $\{$~$+$~$\epsilon$~$\}$. Das heisst, der Beginn der Regel $E'$ kann durch das Non-Terminal-Symbol $+$ oder ein fehlendes Non-Terminal-Symbol erkannt werden. Und das Ergebnis von \texttt{FIRST}$(E)$~$=$ \texttt{FIRST}$(T)$~$=$ \texttt{FIRST}$(F)$ ist die Menge $\{$~$($,~\texttt{id}~$\}$. Das Ergebnis von \texttt{FIRST}$(E')$ 
	\item [\texttt{FOLLOW}:] Sagt aus, welche Non-Terminal-Symbole einer Regel folgen können. Alle möglichen Kombination müssen hierfür berücksichtigt werden. Beispielsweise ist das Ergebnis von \texttt{FOLLOW}$(E)$~$=$ \texttt{FOLLOW}$(E')$~$=$ \texttt{FOLLOW}$(F)$ die Menge $\{$~$+$,~$*$,~$)$,~$\$$~$\}$, wobei $\$$ für das Ende der Eingabe steht.
\end{description}

\subsection{Non-Recursive Predictive Parser}

Diese Art von LL-Parser nutzt Tabellen für ihr Regelwerk. Anhand einer einfachen LL-Grammatik für Expressions (Abbildung~\ref{equ:expression_grammar_ll}) sind im Folgenden die LL-Parsing-Schritte angegeben, die bei der Eingabe \enquote{\id{1}~$*$~\id{2}~$+$~\id{3}} durchlaufen werden (Abbildung~\ref{tab:ll_parser_steps}). Das Ergebnis, der daraus resultierende, konkrete Syntaxbaum ist ebenfalls angegeben (Abbildung~\ref{fig:ll_parse_tree}).

% Compare similar table in the "Dragon Book", Page 228 (id + id * id)

\begin{figure}[!ht]
	\centering
	\caption{LL-Parser-Schritte für Eingabe \enquote{\id{1}~$*$~\id{2}~$+$~\id{3}}}
	\label{tab:ll_parser_steps}

	\begin{tabular}{rlrl}
		\toprule
				& Token-Stream					& Stack											& Folgeaktion (mit Ursache) \\
		\midrule
		(1)		& \id{1}~$*$~\id{2}~$+$~\id{3}	& $E$											& \\
		(2)		& \id{1}~$*$~\id{2}~$+$~\id{3}	& $T$~$E'$										& Ausgabe $E$~\rightarrow~$T$~$E'$ \\
		(3)		& \id{1}~$*$~\id{2}~$+$~\id{3}	& $F$~$T'$~$E'$									& Ausgabe $T$~\rightarrow~$F$~$T'$ \\
		(4)		& \id{1}~$*$~\id{2}~$+$~\id{3}	& $\textbf{id}$~$T'$~$E'$						& Ausgabe $F$~\rightarrow~$\textbf{id}$ \\
		(5)		& $*$~\id{2}~$+$~\id{3}			& $T'$~$E'$										& Vergleich $\textbf{id}$ \\
		(6)		& $*$~\id{2}~$+$~\id{3}			& $*$~$F$~$T'$~$E'$								& Ausgabe $T'$~\rightarrow~$*$~$F$~$T'$ \\
		(7)		& \id{2}~$+$~\id{3}				& $F$~$T'$~$E'$									& Vergleich $*$ \\
		(8)		& \id{2}~$+$~\id{3}				& $\textbf{id}$~$T'$~$E'$						& Ausgabe $F$~\rightarrow~$\textbf{id}$ \\
		(9)		& $+$~\id{3}					& $T'$~$E'$										& Vergleich $\textbf{id}$ \\
		(10)	& $+$~\id{3}					& $E'$											& Ausgabe $T'$~\rightarrow~$\epsilon$ \\
		(11)	& $+$~\id{3}					& $+$~$T$~$E'$									& Ausgabe $E'$~\rightarrow~$+$~$T$~$E'$ \\
		(12)	& \id{3}						& $T$~$E'$										& Vergleich $+$ \\
		(13)	& \id{3}						& $F$~$T'$~$E'$									& Ausgabe $T$~\rightarrow~$F$~$T'$ \\
		(14)	& \id{3}						& $\textbf{id}$~$T'$~$E'$						& Ausgabe $F$~\rightarrow~$\textbf{id}$ \\
		(15)	& 								& $T'$~$E'$										& Vergleich $\textbf{id}$ \\
		(16)	& 								& $E'$											& Ausgabe $T'$~\rightarrow~$\epsilon$ \\
		(17)	& 								&												& Ausgabe $E'$~\rightarrow~$\epsilon$ \\
		\bottomrule
	\end{tabular}
\end{figure}

% \clearpage

\begin{figure}[!ht]
	\centering
	\caption{Der aus den LL-Parser-Schritten resultierende Parsebaum}
	\label{fig:ll_parse_tree}

	\begin{tikzpicture}[
		tlabel/.style={pos=0.4,right=-1pt,font=\footnotesize\color{red!70!black}}, %
		% node distance=1.0cm,
	]
		\node{$E$}
			child { node{$T$}
				child { node{$F$}
					child { node{\id{1}} }
					child[missing] { node{} }
					child[missing] { node{} }
				}
				child { node{$T'$}
					child { node{$*$} }
					child { node{$F$}
						child { node{\id{2}} }
					}
					child { node{$T'$}
						child { node{$\epsilon$} }
					}
				}
				child[missing] { node{} }
			}
			child[missing] { node{} }
			child { node{$E'$}
				child { node{$+$} }
				child { node{$T$}
					child[missing] { node{} }
					child { node{$F$}
						child { node{\id{3}} }
					}
					child { node{$T'$}
						child { node{$\epsilon$} }
					}
				}
				child { node{$E'$}
					child[missing] { node{} }
					child[missing] { node{} }
					child { node{$\epsilon$} }
				}
			};
	\end{tikzpicture}
\end{figure}

% \clearpage

\subsection{Recursive Descent Parser}

Die \enquote{Recursive Descent Parser} gehören zu den Top-Down-Parsern. Ihr Parsing-Stack liegt implizit, über den Aufruf rekursiver Funktionen, vor.

Das Beispiel in Listing~\ref{lis:recursive} zeigt die Umsetzung einer Expression-Grammatik (Abbildung~\ref{equ:expression_grammar_ll}) - einzig die runden Klammern fehlen hierbei.

\lstinputlisting[
	style=javascript,xleftmargin=.0\textwidth,
	label={lis:recursive},
	caption=Beispiel für einen Recursive-Descent-Parser (recursive.js),
	linerange={48-48,50-50,60-72,74-87,89-93},
]{data/recursive.js}

% \clearpage

\subsection{Pratt Parser}
\label{sec:fund_pratt}

Der Ausdruck \enquote{Pratt Parser} bezieht sich auf einen Artikel von Vaughan R. Pratt mit dem Titel \enquote{Top Down Operator Precedence} \cite{pratt1973tdop}. Darin wird ein generalisierter Weg zum Parsen von Expressions über Präfixe, Suffixe und Infixe mit Operatorrangfolge beschrieben. Mit \enquote{Expressions} sind Sprach-Ausdrücke gemeint, die Werte liefern (z.B. \enquote{\texttt{$2$}} oder \enquote{\texttt{$3 + 5$}}). Ein praktisches Besipiel zur Umsetzung ist in einem Blog-Eintrag des Programmiersprachen-Entwicklers Bob Nystrom zu finden \cite{website:pratt_parser}.

Expressions werden hierbei über Parsing-Fragmente (\enquote{Parselets}) erschlossen, die über vorgemerkte Terminal-Symbole getriggert werden. Zu Beginn wird nach einem Präfix gesucht. Das Ergebnis daraus wird in einer Schleife auf mögliche Suffixe und Infixe angewandt, wobei der jeweilge Operatorrang höher als der Referenzrang sein kann, aber niemals darunter liegen darf (Abbildung~\ref{fig:pratt_actions}).

\begin{figure}[!ht]
	\centering
	\caption{Schritte eines Pratt-Parsers}
	\label{fig:pratt_actions}

	\texttt{\input{./diagrams/pratt-steps.latex}}
\end{figure}

% \clearpage

% ------------------------------------------------------------------------------
\section{Error-Recovery-Strategien}

Es gibt vier grundlegende Strategien (\cite{aho2014compilers}, Kapitel 4.1.4). Diese werden hier kurz beschrieben.

\begin{description}
	\item [Panic-Mode:] Bei Auftreten eines Fehlers werden solange Eingabe-Token \enquote{verschluckt}, bis ein synchronisierendes Token auftritt. Ein solches Token markiert das Ende des derzeitigen, fehlerhaften und damit auch den Beginn eines neuen Bereichs. Je nach Sprache und Parsing-Kontext kommt mehr als eine Token-Art (Terminal-Symbol) in Frage.
	\item [Phrase-Level-Recovery:] Bei Auftreten eines Fehlers wird versucht, die Eingabe zu korrigieren. Token können gelöscht oder eingefügt werden. Es wird also gewissermaßen \enquote{geraten}, welches Muster zutrifft. Hierbei besteht allerdings die Gefahr von Endlosschleifen.
	\item [Error-Produktionsregeln:] Bei Entwurf der Sprache werden die Muster bekannter Fehler als eigene Regeln aufgenommen. Fehler werden so gezielt gesucht und erkannt.
	\item [Globale Korrektur:] Hierbei wird die gesamte Eingabe darauf geprüft, welcher mögliche Parsebaum der Eingabe am ehesten entspricht. Diese Methode wird eher dazu genutzt, die besten Änderungsmöglichkeiten für die Phrase-Level-Recovery festzustellen.
\end{description}

% \clearpage

% ------------------------------------------------------------------------------
\section{Parser-Generatoren}

Parser-Generatoren sollen dem Entwickler das Schreiben eines Parsers ersparen. Sie kommen insbesondere dann zum Einsatz, wenn die Sprache von großem Umfang und in Bezug auf das Parsen deutlich definiert ist. Auch zu bedenken ist, inwieweit der generierte Code ausreicht, oder anderenfalls Anpassungen und Erweiterung notwendig werden.

\subsection{Yacc \& Lex}

Yacc steht für \enquote{yet another compiler-compiler} und ist ein LALR-Parser-Generator. LALR steht für \enquote{Lookahead-LR} und stellt eine leicht vereinfachte Variante von LR dar. Yacc wurde in den frühen 1970er-Jahren entwickelt, und war Bestandteil von UNIX (siehe \cite{aho2014compilers}, Kapitel 4.9). Yacc setzt einen Lexer voraus, wofür Lex üblich ist. Lex ist ein Lexer-Generator, der 1975 entwickelt wurde \cite{website:lex}. Auch wenn die Originalprogramme heute nicht mehr genutzt werden, gibt es moderne Abkömmlinge dieser Generatoren. Zwei der bekanntesten sind Bison \& Flex \cite{website:yacc}. Die beiden Programme unterstützen praktisch nur C und C++.

Sowohl Lex als auch Yacc sehen bei erkannten Mustern den Aufruf von zuvor angegebenen Code in der Zielsprache vor. Wenn ein Syntaxbaum benötigt wird, muss dieser selbst erstellt und verwaltet werden, die Generatoren geben hierbei keine Unterstützung.

In Bezug auf Error-Recovery ist bei Yacc/Bison vorgesehen, dass beim Entwurf der Sprache Error-Produktionsregeln eingeplant werden.

% \clearpage
% \lstinputlisting[
% 	style=cpp,
% 	label={lis:helloworld},
% 	caption={Beispiel für eine Flex-Definition},
% 	linerange={1-43}
% ]{data/example.l}

% \clearpage

\subsection{ANTLR}

ANTLR (\enquote{ANother Tool for Language Recognition}) ist ein Adaptive-LL(*)-Parser-Generator (und Lexer-Generator), der in vielen bekannten Projekten zum Einsatz kommt \cite{website:antlr} \cite{parr2014all}.

Wie auch mit Yacc/Lex ist es mit ANTLR möglich, Code für die Zielsprache mit Regeln zu assoziieren. ANTLR besitzt aber Vorteile gegenüber Yacc/Lex. 

\begin{itemize}
	\item Wenn die Regeln nicht mit Aktionen assoziiert werden, erzeugt ANTLR standardmäßig einen Syntaxbaum.
	\item ANTLR erlaubt die Auswahl aus einer Vielzahl an Zielsprachen für die erzeugten Parser/Lexer.
	\item Es wird bereits eine grundlegende Error-Recovery eingebaut. Darüberhinaus sind Möglichkeiten vorgesehen, das Standard-Verhalten zu überschreiben.
\end{itemize}

Das zuvor bereits genutzte Beispiel für einfache Expression-Grammatik (Abbildung~\ref{equ:expression_grammar_ll}) ist in Listing~\ref{lis:expression_grammar} für ANTLR4 umgesetzt.

% \clearpage
\lstinputlisting[
	style=antlr,
	label={lis:expression_grammar},
	caption={Beispiel für eine ANTLR4-Grammatik-Definition (example.g4)},
	% linerange={1-79},
]{data/example.g4}

% ------------------------------------------------------------------------------
\section{Manuell erstellte Parser}

Für manuell erstellte Parser wird fast ausschließlich der Ansatz der \enquote{Recursive Descent Parser} genutzt. Der rekursive Ansatz wird genutzt, weil die manuelle Erstellung von Parser-Tabellen, meist zu aufwändig ist. Es gibt noch eine weitere Variante, die \enquote{Recursive Ascent Parser}, die zu den Bottom-Up-Parsern gehören und kaum zum Einsatz kommen. In der Praxis lassen sich manuell erstellte, rekursive Parser-Funktionen leicht um Kniffe erweitern, durch die ein Hybrid der beiden Varianten geformt wird, ein \enquote{Recursive Ascent-Descent Parser} (\cite{nh2003cpsap}).

Gegenüber generierten Parsern, haben manuell erstellte den Vorteil, dass sie sich am besten an die zu parsende Sprache anpassen lassen. Das erfordert aber mehr Hintergrundwissen und zumeist auch Entwicklungsaufwand.

% \clearpage
