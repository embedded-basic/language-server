\chapter{Konzeption des Language Servers}
\label{sec:Konzeption}

Der Zweck des Language-Servers soll sein, Entwicklern, bei der Nutzung einer Programmiersprache Hilfe zu leisten.

% ------------------------------------------------------------------------------
\section{Skriptsprache \enquote{Embedded BASIC}}
\label{sec:Skriptsprache}

Im Zentrum dieser Arbeit steht die domänenspezifische Skriptsprache \enquote{Embedded BASIC} (\cite{website:altenburg}). Diese Sprache kommt beim Programmieren von eingebetteten Systemen, innerhalb einer kleinen Laufzeitumgebung, zum Einsatz. Ihre Zielgruppe sind Elektronik-Ingenieure und keine Informatiker, weshalb sie sich an bekannten BASIC-\-Dia\-lekten orientert. Ihre Eigenschaften und Anforderungen entsprechen in etwa dem \enquote{Strukturierten Text} (\enquote{Structured text}, \enquote{ST}/\enquote{STX}, \cite{website:stx}).

Blöcke werden in dieser Sprache also nicht über Klammern begrenzt, sondern über Schlüsselwörter. Und Anweisungen werden im Normalfall durch Zeilenendkennungen voneinander getrennt. Bei Ausdrücken in Klammern werden Zeilenendkennungen hingegen ignoriert, wodurch sich beispielsweise ein Funktionsaufruf über mehrere Zeilen erstrecken kann (siehe Abbildung~\ref{lis:func_call}).

\begin{lstlisting}[
	style=embeddedbasic,
	label={lis:func_call},
	caption={Funktionsaufruf},
]
doSomething(
	2, // Erläuterung zum ersten Parameter
	"foo", // ...
	bar)
\end{lstlisting}

Embedded BASIC kennt auch Präprozessor-Anweisungen, weshalb ein Zwischenschritt beim Parsen notwendig wird. Wie auch bei der Programmiersprache \enquote{C}, gibt es die Möglichkeit über ein \enquote{\texttt{\#include}} andere Quellcode-Dateien einzubinden. Ebenso ist sind \enquote{\texttt{\#define}}, \enquote{\texttt{\#undef}} und \enquote{\texttt{\#ifdef}}-Blöcke möglich.

Zur Veranschaulichung werden hier im Folgenden als Beispiel ein paar Sprachkonstrukte genauer beschrieben.

\subsection{Variablendeklaration}

Variablendeklarationen werden grundsätzlich wie in \enquote{C} erwartet. Sie beginnt mit dem Datentyp und fährt dann mit dem Namen der Variable fort. Optional können dann eckige Klammern und eine darin angegebene Ganzzahl die Variable als Array kennzeichnen. Noch in der Deklaration kann anschließend über ein einleitendes Gleichheitszeichen ein initialer Wert zugewiesenen werden (Abbildung~\ref{lis:var_decl}).

\begin{lstlisting}[
	style=embeddedbasic,
	label={lis:var_decl},
	caption={Variablendeklaration},
]
byte c = %11011, d = $90AC
char str1[2] = "a", str2[2] = "b"
\end{lstlisting}

\subsection{Einfache bedingte Anweisung}

Eine einfache bedingte Anweisung stellt die \enquote{If-Else-Anweisung} dar. Sie beginnt mit einem \enquote{\texttt{if}} und endet normalerweise mit einem \enquote{\texttt{endif}}. Das Ende einer Bedingung und der Beginn der damit assoziierten Anweisungen wird durch ein \enquote{\texttt{then}} markiert. Ab der zweiten Bedingung beginnt der zugehörige Block mit einem \enquote{\texttt{elseif}} (Abbildung~\ref{lis:if_else}). In einer einfachen Sprungbedingung kann das Ende der Bedingung, und auch der Anweisung, auch ganz einfach durch eine Sprunganweisung erfolgen (Abbildung~\ref{lis:if_jump}).

\begin{lstlisting}[
	style=embeddedbasic,
	label={lis:if_else},
	caption={If-Else-Anweisung},
]
if a > b then
	// Anweisungen...
elseif a > c then
	// ...
else
	// ...
endif
\end{lstlisting}

\begin{lstlisting}[
	style=embeddedbasic,
	label={lis:if_jump},
	caption={Direkte If-Jump-Anweisung},
]
if a > 10 return
\end{lstlisting}

\subsection{Zählschleife}

Die Schleife beginnt mit einem \enquote{\texttt{for}} und fährt mit einer Variablenzuweisung fort. Es folgt ein \enquote{\texttt{to}} mit anschließender Angabe des Zielwerts, den die Zählschleife erreichen soll. Optional kann dann ein \enquote{\texttt{step}} und ein Wert folgen, durch den die Schrittweite und Richtung bestimmt werden kann. Die Zählschleife endet mit einem \enquote{\texttt{endfor}} (Abbildung~\ref{lis:for_loop}).

\begin{lstlisting}[
	style=embeddedbasic,
	label={lis:for_loop},
	caption={For-Loop-Schleife},
]
for x=100 to 1 step -2
	// Anweisungen...
endfor
\end{lstlisting}

\subsection{Funktionsdeklaration}

Als zusammenhängendes Beispiel soll hier die Funktionsdeklaration dienen. Das Beispiel zeigt, dass auch komplexe Konstrukte möglich sind (siehe Listing~\ref{lis:function}).

\begin{lstlisting}[
	style=embeddedbasic,
	label={lis:function},
	caption={Funktion zum Vergleich von ASCII-Zeichenketten (Case-Insensitive)},
]
function bool StrCompareNoCase(char str1[], char str2[])
	int len1 = len(str1)
	int len2 = len(str2)
	int chrpos
	char ch1, ch2

	if len1 == len2 then
		for chrpos = 0 to len1
			if str1[chrpos] <> str2[chrpos] then
				ch1 = str1[chrpos]
				
				if (ch1 >= 'A') and (ch1 <= 'Z') then inc ch1, 32
	
				ch2 = str2[chrpos]
				
				if (ch2 >= 'A') and (ch2 <= 'Z') then inc ch2, 32
				
				if ch1 <> ch2 return false
			endif
		endfor

		return true
	endif
endfunction
\end{lstlisting}

% ------------------------------------------------------------------------------
\section{Eingabeunterstützung in Editoren}
\label{sec:Eingabe}

Die Art und Weise, wie eine Eingabeunterstützung angeboten werden kann, hängt vom Editor ab. Es kann sich  um einen selbst geschriebenen oder angepassten Editor handeln. Oder aber es kann ein Editor sein, der das Einbinden über Erweiterungen ermöglicht, wie beispielsweise Visual Studio Code.

Am einfachsten lässt sich das Syntax-Highlighting umsetzen, da hierbei zumeist eine lexikalische Analyse ausreicht. Zur Beschreibung lassen sich Reguläre Ausdrücke nutzen. Als Beispiel der Muster-Beschreibung für das Syntax-Highlighting ist in der Abbildung~\ref{lis:tmlanguage} ein Auzug des Musters angegeben, durch das \enquote{Embedded BASIC} beschrieben wird.

Um umfassendere Informationen zugänglich zu machen, reicht ein einfaches Beschreibungsmuster nicht aus.

\begin{lstlisting}[
	style=javascript,
	label={lis:tmlanguage},
	caption={Muster für das Syntax-Highlighting (Auszug)},
]
{
  "$schema": "https://raw.githubusercontent.com/martinring/tmlanguage/master/tmlanguage.json",
  "name": "Embedded BASIC",
  "scopeName": "source.embedded-basic",
  "patterns": [
    { "include": "#preprocessor" },
    { "include": "#keywords" },
    { "include": "#constants" },
    { "include": "#strings" },
    { "include": "#chars" },
    { "include": "#numbers" },
    { "include": "#operators" },
    { "include": "#puncuation" },
    { "include": "#comments" }
  ],
  "repository": {
    "preprocessor": {
      "patterns": [
        {
          "name": "keyword.preprocessor.embedded-basic",
          "match": "(#include|#region|#endregion|#target|#define|#undef|#ifdef|#ifndef|#else|#endif|#error)"
        }
      ]
    },
    "strings": {
      "name": "string.quoted.double.embedded-basic",
      "begin": "\"",
      "end": "\"",
      "patterns": [
        {
          "name": "constant.character.escape.embedded-basic",
          "match": "\\\\."
        }
      ]
    }
  },
  // ...
}
\end{lstlisting}

\subsection{Fehlermeldungen und Hinweise}

Zu Syntax und Semantik sollen grundlegende Fehlermeldungen und Hinweise, also Diagnose-Informationen, aufgelistet werden können. Beispielsweise soll feststellbar sein, ob und wo im Code unerwartete Wörter und Symbole gefunden wurde. Ebenso soll festgestellt werden können, ob eine Variable oder Funktion existiert, oder bereits an anderer Stelle definiert wurde. Die Diagnose-Informationen sollen dem Editor, und damit dem Entwickler zur Verfügung gestellt werden.

\subsection{Goto Definition}

Bei der Einarbeitung in fremden oder älteren Code kommt es vor, dass man nähere Informationen zu einer genutzten Variable oder Funktion erhalten möchte. Man möchte vielleicht zur Definition springen, um sich das Symbol und auch seine Umgebung genauer anzusehen, oder eine Änderung vorzunehmen. Die gesuchte Stelle befindet sich nicht zwangsläufig in der gleichen Definitionsebene oder gar Source-Datei, wie die Stelle der Benutzung. Die Ausgangsinformation ist also eine Positionsangabe innerhalb der geöffneten Quelldatei und das Ziel ist, die Position und Quelldatei der Definition des fraglichen Symbols anzuspringen.

\subsection{Tooltipps}

Mit \enquote{Tooltipp} ist die Informationsanzeige gemeint, die nach längerem Verweilen des Mauszeigers über einem Wort oder Symbol im Editor angezeigt wird. Beispielsweise möchte der Entwickler genauere Informationen zu einer Variable oder Funktion als Anzeige erhalten. Die Ausgangsinformation ist also eine Positionsangabe innerhalb der geöffneten Quelldatei. Die gesuchten Informationen sind bei einer Variablen der Datentyp und die ihr zugeordnete Beschreibung in den Kommentaren davor und auf der selben Zeile danach. Bei einer Funktion wird die Signatur mit Rückgabe-Datentyp, Parameter-Datentypen und -Namen und jeweiligen Beschreibungen erwartet (Beispiel siehe Abbildung~\ref{fig:tooltip}).

\begin{figure}[!ht]
	\centering
	\caption{Beispiel für einen \Csharp{}-Tooltipp}
	\label{fig:tooltip}
	\begin{displayquote}
		\texttt{EbSemanticTree SemanticAnalyzer.Analyze(EbSyntaxTree syntaxTree)} \\
		Analyze a syntax tree to produce a semantic tree. \\
		Rückgabewerte: \\
		The produced semantic tree.
	\end{displayquote}
\end{figure}

\subsection{Code-Vervollständigung}

Beim Schreiben von Code wird natürlich auf bestehende Funktionen zurückgegriffen. Bei einer umfangreichen API (\enquote{Programmierschnittstelle}) kommt es immer wieder vor, dass beim Entwickleln Teile der API noch nicht, oder noch nicht ausreichend, bekannt sind. Gesucht wird vielleicht der korrekte Name einer Funktion. Es kann auch einfach sein, dass das Schreiben langer Funktionsnamen vermieden werden soll. In diesen Fällen kann eine Code-Vervollständigung Abhilfe schaffen.

\subsection{Parameter-Infos}

Als Ergänzung zur Code-Vervollständigung lässt sich die Anzeige von Parameter-Infos sehen. Beim Schreiben eines Funktionsaufrufs ist die Anzeige zu Art und Position der Funktionsparameter während der Eingabe hilfreich.

\subsection{Referenzierungen}

In dem Maß, in dem Code anwächst, fällt es schrittweise schwerer den Überblick über den Code zu behalten. Um beispielsweise die Benutzung einer Variable oder Funktion zu verfolgen, möchte man die vielleicht fraglichen Stellen hervorheben. Um exakte Resultate zu liefern, reicht ein reiner Textvergleich nicht aus. Auch hier werden semantische Informationen benötigt.

% Variable-References (Doppelklick auf Variable)

\subsection{Bereichsgrenzen}

Mit den Bereichsgrenzen sind die Begrenzungen der Anweisungsblöcke, wie etwa der Zählschleife oder der If-Else-Anweisung, gemeint. Wenn eines der Strukturelemente angesteuert wird, sollen alle Strukturelemente des Anweisungsblocks hervorgehoben werden. Je nach Editor muss beispielsweise der Cursor positioniert oder ein Strukturelemente selektiert werden.

% ------------------------------------------------------------------------------
\section{Struktur der Syntax-Informationen}

Beim Blick auf die Informationen die bei der Analyse des Textes einer Sprache gesammelt werden können, fällt auf, dass diese Informationen verschachtelt sind. In der Bedeutung können einem Non-Terminal-Symbol mehrere andere Symbole untergeordnet sein. Die Terminal-Symbole bilden hierbei den Abschluss der Verschachtelung. Diese Sturktur entspricht einem Baum mit Wurzel. Es handelt sich um einen Syntaxbaum. Dieser Baum soll beim Parsen erstellt werden. Ein Ausschnitt als Beispiel für diesen Syntaxbaum ist in Abbildung~\ref{fig:syntax_tree} zu sehen.

\begin{figure}[!ht]
	\centering
	\caption{Ausschnitt aus dem Syntaxbaum (von Links nach Rechts)}
	\label{fig:syntax_tree}

	\includegraphics[width=0.66\textwidth]{diagrams/syntax-tree.pdf}
\end{figure}

Zu jedem Knoten (jedem Symbol) sollen detaillierte Informationen enthalten sein. Dazu gehören auch genaue Positionsangaben. Das heisst es soll möglich sein auszusagen, wo das Symbol im Quelltext beginnt und wo es endet. Zu jedem Knoten soll auch vermerkt sein, zu welchem Elternknoten es gehört, also unter welchem anderen Knoten es sich innerhalb des Baumes befindet.

% ------------------------------------------------------------------------------
\section{Tree-Walker}

Die Erstellung ist nur der erste Schritt, denn es fehlen noch semantische Informationen. Um diese Informationen zu sammeln, muss der Syntaxbaum analysiert werden. Dafür wird ein Tree-Walker benötigt. Das ist ein Programmteil, der den gesamten Syntaxbaum, Knoten für Knoten, durchlaufen kann. Es beginnt beim Wurzelknoten und schreitet rekursiv voran. Der Knoten wird \enquote{besucht} und daraufhin werden auch dessen Kind-Knoten besucht. Und so weiter. Jeder konkrete Tree-Walker kann dabei die für ihn vorgesehene Aufgabe erfüllen, wie etwa die Analyse der jeweiligen Knoten.

% ------------------------------------------------------------------------------
\section{Schnittstelle zum Editor}

Grundsätzlich soll es möglich sein, den Language-Server direkt in einen Editor einzubinden. Aber die Umstände dieses Anwendungsfalls schränken die Benutzbarkeit dennoch enorm ein. Eine Möglichkeit zum erleichterten Einbinden in einen Editor wird benötigt.

\subsection{Ein- und Ausgabedaten}

Wie man an den unter Kapitel \ref{sec:Eingabe} erkennen kann, müssen bei der Kommunikation mit dem Language-Server umfangreiche Informationen ausgetauscht werden.

\begin{description}
	\item [Eingabe:] Grundsätzlich müssen dem Language-Server die URIs (\enquote{Uniform Resource Identifier}) der geöffneten und editierten Dateien mitgeteilt werden. Im Idealfall werden beim Öffnen und Ändern auch bereits die Textinhalte mit übertragen, was deren inkrementelle Verarbeitung ermöglicht. Eine andere typische Ausgangsinformation ist beispielsweise eine Positionsangabe.
	\item [Ausgabe:] Die zu liefernden Informationen sind die Namen von Typen, Variablen und Funktionen mitsamt der zu ihnen gesammelten Beschreibungen und ihrer jeweiligen Quell\-text-Position.
\end{description}

\subsection{Language-Server-Protokoll}

Seit Mitte 2016 gibt es ein Protokoll, das die Entwicklung von Sprachunterstützungen erleichtert \cite{website:lsp_launch}. Das \enquote{Language Server Protocol} wird unter anderem von Microsoft und Red Hat unterstützt. Durch die Nutzung des \enquote{Language Server Protocol} ist es dem Anbieter einer domänenspezifische Sprache möglich, die Integration in bestehende IDEs und Source-Code-Editoren deutlich zu vereinfachen.

Das Protokoll basiert auf dem JSON-RPC, einem leichgewichtigen Remote-Procedure-Call-Protokoll \cite{website:json_rpc}. Im HTTP-Header wird der \enquote{Content-Type} \enquote{application/vscode-jsonrpc; charset=utf-8} genutzt, und der Inhalt folgt dem Muster in Listing~\ref{lis:lsp_base}.

\lstinputlisting[
	style=javascript,
	label={lis:lsp_base},
	caption={Besipiel für einen LSP-Inhalt (didOpen-request-rpc.json)},
	% linerange={1-79},
]{data/didOpen-request-rpc.json}

Das Protokoll ist sehr umfangreich, und ermöglicht auch die Teil-Registrierung von Fähigkeiten. Client und Server können somit genau in Erfahrung bringen, welche Möglichkeiten ihr Gegenüber unterstützt.

Zu den Möglichkeiten gehören unter Anderem die Methoden \enquote{textDocument/didOpen}, \enquote{textDocument/didChange}, \enquote{textDocument/didClose}, \enquote{textDocument/codeLens}, \enquote{textDocument/hover}, \enquote{textdocument/definition} und \enquote{textDocument/completion}.

Als Beispiel für die möglichen Nachrichten sind hier das \enquote{Goto Definition} und die Code-Vervollständigung kurz beschrieben.

\lstinputlisting[
	style=javascript,
	label={lis:def_req},
	caption={Besipiel-Inhalt für einen Definiton-Request (definition-request-rpc.json)},
	% linerange={1-79},
]{data/definition-request-rpc.json}

\lstinputlisting[
	style=javascript,
	label={lis:def_res},
	caption={Besipiel-Inhalt für einen Definiton-Response (definition-response-rpc.json)},
	% linerange={1-79},
]{data/definition-response-rpc.json}

\begin{figure}[!ht]
	\centering
	\caption{LPS-Methode \enquote{Completion}}
	\label{tab:lps_completion}

	\begin{tabular}{p{2cm}p{11cm}}
		\toprule
		Methode		&	\enquote{textDocument/completion} \\
		Eingaben	&	Soweit verfügbar, ein einzelnes Eingabezeichen, über das die Vervollständigung ausgelöst wurde. \par
						Die Einordnung, ob der Auslöser ohne konkrete Eingabe ausgelöst wurde. \\
		Ausgaben	&	Die Aufzählung der verfügbaren Vervollständigungs-Optionen. \\
		Link		&	\url{https://microsoft.github.io/language-server-protocol/specifications/specification-current/#textDocument_completion} \\
		\bottomrule
	\end{tabular}
\end{figure}
