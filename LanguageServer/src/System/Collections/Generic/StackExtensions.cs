#if NETSTANDARD2_0

using System.Diagnostics.CodeAnalysis;

namespace System.Collections.Generic
{
    public static class StackExtensions
    {
        public static bool TryPop<T>(this Stack<T> stack, [MaybeNullWhen(false)] out T result)
        {
            if (stack.Count == 0)
            {
                result = default(T);
                return false;
            }
            result = stack.Pop();
            return true;
        }
    }
}

#endif
