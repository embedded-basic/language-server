﻿using System.Globalization;

namespace EmbeddedBasic.Util
{
    public static class StringExtensions
    {
        public static bool ParseBool(this string value)
            => bool.TryParse(value, out var result) ? result : false;

        public static char ParseChar(this string value)
            => char.TryParse(value, out var result) ? result : char.MinValue;

        public static int ParseInt32(this string value)
            => int.TryParse(value, NumberStyles.Any, CultureInfo.InvariantCulture, out var result) ? result : 0;

        public static float ParseFloat(this string value)
            => float.TryParse(value, NumberStyles.Any, CultureInfo.InvariantCulture, out var result) ? result : 0.0f;
    }
}
