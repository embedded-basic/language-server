﻿using System.IO;

namespace EmbeddedBasic.Util
{
    public static class IO
    {
        /// <summary>
        /// Ensure that the directory with the given path exists.
        /// </summary>
        /// <param name="path">The path of the directory th check.</param>
        /// <returns>Does the directory with the given path exist?</returns>
        public static bool EnsureDirectory(string path)
        {
            path = Path.GetFullPath(path);

            if (File.Exists(path)) return false;
            else if (!Directory.Exists(path))
            {
                if (!EnsureDirectory(Path.GetDirectoryName(path)!)) return false;
                Directory.CreateDirectory(path);
            }
            return true;
        }
    }
}
