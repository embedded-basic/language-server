﻿using System.Collections.Generic;

namespace EmbeddedBasic.Util
{
    public static class ICollectionExtensions
    {
        /// <summary>
        /// Add the element to the list, if the element is not null.
        /// </summary>
        /// <param name="list">The list to append.</param>
        /// <param name="element">The element to add.</param>
        /// <returns>Could the element be added to the list?</returns>
        public static bool AddIfNonNull<T>(this ICollection<T> list, T? element)
        {
            if (element != null)
            {
                list.Add(element);
                return true;
            }
            return false;
        }
    }
}
