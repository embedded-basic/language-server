﻿using System.Globalization;

namespace EmbeddedBasic.Util
{
    public static class FloatExtensions
    {
        public static string ToCode(this float number) => number.ToString(CultureInfo.InvariantCulture);
    }
}
