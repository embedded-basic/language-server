// -----------------------------------------------------------------------------
// The MIT License (MIT)

// Copyright (c) .NET Foundation and Contributors

// All rights reserved.

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// -----------------------------------------------------------------------------
// Most of the code in this class was copied from the the following classes:
// * "System.Collections.Generic.List<T>"
// * "System.Collections.Generic.SortedList<TKey, TValue>"
// -----------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace EmbeddedBasic.Util
{
    /// <summary>
    /// A list as array of sorted items.
    /// </summary>
    /// <remarks>
    /// Appending out of descendant order will cause runtime overhead!
    /// Based on code from <see cref="System.Collections.Generic.List{T}" /> and
    /// <see cref="System.Collections.Generic.SortedList{TKey, TValue}" />.
    /// </remarks>
    /// <param name="TValue">The type of values stored in this list.</param>
    [DebuggerDisplay("Count = {Count}")]
    public partial class SortedArrayList<TValue> :
        IReadOnlyList<TValue>, IList<TValue>, IList,
        IReadOnlyCollection<TValue>, ICollection<TValue>, ICollection,
        IEnumerable<TValue>, IEnumerable
        where TValue : notnull
    {
        protected TValue[] _items;

        protected int _size;

        private int _version;

        protected readonly IComparer<TValue> _comparer;

        private static readonly TValue[] _emptyArray = new TValue[0];

        public int Capacity
        {
            get => _items.Length;
            set
            {
                if (value < _size) throw new IndexOutOfRangeException();
                if (value == _items.Length) return;
                if (value > 0)
                {
                    var array = new TValue[value];
                    if (_size > 0) Array.Copy(_items, array, _size);
                    _items = array;
                }
                else _items = _emptyArray;
            }
        }

        public int Count => _size;

        bool IList.IsFixedSize => false;

        bool ICollection<TValue>.IsReadOnly => false;

        bool IList.IsReadOnly => false;

        bool ICollection.IsSynchronized => false;

        object ICollection.SyncRoot => this;

        public TValue this[int index]
        {
            get
            {
                if ((uint)index >= (uint)_size) throw new IndexOutOfRangeException();
                return _items[index];
            }
            set
            {
                if ((uint)index >= (uint)_size) throw new IndexOutOfRangeException();
                _items[index] = value;
                _version++;
            }
        }

        object? IList.this[int index]
        {
            get => this[index];
            set
            {
                if (value == null) throw new ArgumentNullException();

                try
                {
                    this[index] = (TValue)value;
                }
                catch (InvalidCastException)
                {
                    throw;
                }
            }
        }

        public SortedArrayList(int capacity = 0, IComparer<TValue>? comparer = null)
        {
            if (capacity == 0) _items = _emptyArray;
            else if (capacity > 0) _items = new TValue[capacity];
            else throw new IndexOutOfRangeException();
            _comparer = comparer ?? Comparer<TValue>.Default;
        }

        public SortedArrayList(ICollection<TValue> collection, IComparer<TValue>? comparer = null) :
            this(collection.Count, comparer)
        {
            foreach (var item in collection) AddInternal(item);
        }

        public SortedArrayList(IEnumerable<TValue> enumerable, IComparer<TValue>? comparer = null) :
            this(0, comparer)
        {
            foreach (var item in enumerable) Add(item);
        }

        private static bool IsCompatibleObject(object value)
        {
            if (!(value is TValue))
            {
                if (value == null) return default(TValue) == null;
                return false;
            }
            return true;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected int AddInternal(TValue item)
        {
            if (item == null) throw new ArgumentNullException("item");
            int index = Array.BinarySearch(_items, 0, _size, item, _comparer);
            if (index >= 0) return -1;
            Insert(~index, item);
            return ~index;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Add(TValue item) => AddInternal(item);

        int IList.Add(object? item)
        {
            if (item == null) throw new ArgumentNullException();

            try
            {
                return AddInternal((TValue)item);
            }
            catch (InvalidCastException)
            {
                throw;
            }
        }

        public void AddRange(IEnumerable<TValue> collection)
        {
            foreach (var item in collection) Add(item);
        }

        public ReadOnlyCollection<TValue> AsReadOnly() => new ReadOnlyCollection<TValue>(this);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Clear()
        {
            _version++;
            int size = _size;
            _size = 0;
            if (size > 0) Array.Clear(_items, 0, size);
        }

        public bool Contains(TValue item)
        {
            if (_size != 0) return IndexOf(item) != -1;
            return false;
        }

        bool IList.Contains(object? item)
        {
            if (item != null && IsCompatibleObject(item)) return Contains((TValue)item);
            return false;
        }

        public void CopyTo(TValue[] array) => CopyTo(array, 0);

        void ICollection.CopyTo(Array array, int arrayIndex)
        {
            if (array != null && array.Rank != 1) throw new ArgumentException();

            try
            {
                Array.Copy(_items, 0, array!, arrayIndex, _size);
            }
            catch (ArrayTypeMismatchException)
            {
                throw new ArgumentException();
            }
        }

        public void CopyTo(int index, TValue[] array, int arrayIndex, int count)
        {
            if (_size - index < count) throw new ArgumentException();
            Array.Copy(_items, index, array, arrayIndex, count);
        }

        public void CopyTo(TValue[] array, int arrayIndex) => Array.Copy(_items, 0, array, arrayIndex, _size);

        private void EnsureCapacity(int min)
        {
            if (_items.Length < min)
            {
                int num = ((_items.Length == 0) ? 4 : (_items.Length * 2));
                if ((uint)num > 2146435071u) num = 2146435071;
                if (num < min) num = min;
                Capacity = num;
            }
        }

        public bool Exists(Predicate<TValue> match) => FindIndex(match) != -1;

        public TValue? Find(Predicate<TValue> match)
        {
            if (match == null) throw new ArgumentNullException();
            for (int i = 0; i < _size; i++)
            {
                if (match(_items[i])) return _items[i];
            }
            return default(TValue);
        }

        public int FindIndex(Predicate<TValue> match) => FindIndex(0, _size, match);

        public int FindIndex(int startIndex, Predicate<TValue> match)
            => FindIndex(startIndex, _size - startIndex, match);

        public int FindIndex(int startIndex, int count, Predicate<TValue> match)
        {
            if ((uint)startIndex > (uint)_size) throw new ArgumentOutOfRangeException();
            if (count < 0 || startIndex > _size - count) throw new ArgumentOutOfRangeException();
            if (match == null) throw new ArgumentNullException();

            int num = startIndex + count;
            for (int i = startIndex; i < num; i++)
            {
                if (match(_items[i])) return i;
            }
            return -1;
        }

        public TValue? FindLast(Predicate<TValue> match)
        {
            if (match == null) throw new ArgumentNullException();
            for (int num = _size - 1; num >= 0; num--)
            {
                if (match(_items[num])) return _items[num];
            }
            return default(TValue);
        }

        public int FindLastIndex(Predicate<TValue> match) => FindLastIndex(_size - 1, _size, match);

        public int FindLastIndex(int startIndex, Predicate<TValue> match)
            => FindLastIndex(startIndex, startIndex + 1, match);

        public int FindLastIndex(int startIndex, int count, Predicate<TValue> match)
        {
            if (match == null) throw new ArgumentNullException();
            if (_size == 0)
            {
                if (startIndex != -1) throw new ArgumentOutOfRangeException();
            }
            else if ((uint)startIndex >= (uint)_size) throw new ArgumentOutOfRangeException();

            if (count < 0 || startIndex - count + 1 < 0) throw new ArgumentOutOfRangeException();
            int num = startIndex - count;
            for (int num2 = startIndex; num2 > num; num2--)
            {
                if (match(_items[num2])) return num2;
            }
            return -1;
        }

        public void ForEach(Action<TValue> action)
        {
            if (action == null) throw new ArgumentNullException();
            int version = _version;
            for (int i = 0; i < _size; i++)
            {
                if (version != _version)
                {
                    break;
                }
                action(_items[i]);
            }
            if (version != _version) throw new InvalidOperationException();
        }

        public Enumerator GetEnumerator() => new Enumerator(this);

        IEnumerator<TValue> IEnumerable<TValue>.GetEnumerator() => new Enumerator(this);

        IEnumerator IEnumerable.GetEnumerator() => new Enumerator(this);

        public int IndexOf(TValue item)
        {
            int num = Array.BinarySearch(_items, 0, _size, item, _comparer);
            if (num < 0) return -1;
            return num;
        }

        int IList.IndexOf(object? item)
        {
            if (item != null && IsCompatibleObject(item)) return IndexOf((TValue)item);
            return -1;
        }

        public int IndexOf(TValue item, int index)
        {
            if (index > _size) throw new IndexOutOfRangeException();
            return Array.BinarySearch(_items, index, _size - index, item, _comparer);
        }

        public int IndexOf(TValue item, int index, int count)
        {
            if (index > _size) throw new IndexOutOfRangeException();
            if (count < 0 || index > _size - count) throw new ArgumentOutOfRangeException();
            return Array.BinarySearch(_items, index, count, item, _comparer);
        }

        private void Insert(int index, TValue item)
        {
            if ((uint)index > (uint)_size) throw new IndexOutOfRangeException();
            if (_size == _items.Length) EnsureCapacity(_size + 1);
            if (index < _size) Array.Copy(_items, index, _items, index + 1, _size - index);
            _items[index] = item;
            _size++;
            _version++;
        }

        void IList<TValue>.Insert(int index, TValue? item) => throw new NotSupportedException();

        void IList.Insert(int index, object? item) => throw new NotSupportedException();

        public bool Remove(TValue item)
        {
            int num = IndexOf(item);
            if (num >= 0)
            {
                RemoveAt(num);
                return true;
            }
            return false;
        }

        void IList.Remove(object? item)
        {
            if (item != null && IsCompatibleObject(item)) Remove((TValue)item);
        }

        public int RemoveAll(Predicate<TValue> match)
        {
            if (match == null) throw new ArgumentNullException();
            int i;
            for (i = 0; i < _size && !match(_items[i]); i++) { }
            if (i >= _size) return 0;
            int j = i + 1;
            while (j < _size)
            {
                for (; j < _size && match(_items[j]); j++) { }
                if (j < _size) _items[i++] = _items[j++];
            }
            Array.Clear(_items, i, _size - i);
            int result = _size - i;
            _size = i;
            _version++;
            return result;
        }

        public void RemoveAt(int index)
        {
            if ((uint)index >= (uint)_size)
            {
                throw new IndexOutOfRangeException();
            }
            _size--;
            if (index < _size)
            {
                Array.Copy(_items, index + 1, _items, index, _size - index);
            }
            _items[_size] = default(TValue)!;
            _version++;
        }

        public void RemoveRange(int index, int count)
        {
            if (index < 0) throw new ArgumentOutOfRangeException();
            if (count < 0) throw new IndexOutOfRangeException();
            if (_size - index < count) throw new ArgumentException();
            if (count > 0)
            {
                _size -= count;
                if (index < _size) Array.Copy(_items, index + count, _items, index, _size - index);
                _version++;
                Array.Clear(_items, _size, count);
            }
        }

        public void Reverse() => Reverse(0, Count);

        public void Reverse(int index, int count)
        {
            if (index < 0) throw new IndexOutOfRangeException();
            if (count < 0) throw new IndexOutOfRangeException();
            if (_size - index < count) throw new ArgumentException();
            if (count > 1) Array.Reverse(_items, index, count);
            _version++;
        }

        public TValue[] ToArray()
        {
            if (_size == 0) return _emptyArray;
            var array = new TValue[_size];
            Array.Copy(_items, array, _size);
            return array;
        }

        public void TrimExcess()
        {
            int num = (int)((double)_items.Length * 0.9);
            if (_size < num) Capacity = _size;
        }

        public bool TrueForAll(Predicate<TValue> match)
        {
            if (match == null) throw new ArgumentNullException();
            for (int i = 0; i < _size; i++)
            {
                if (!match(_items[i])) return false;
            }
            return true;
        }

        public TValue First()
        {
            if (_size == 0) throw new InvalidOperationException();
            return _items[0];
        }

        public TValue Last()
        {
            if (_size == 0) throw new InvalidOperationException();
            return _items[_size - 1];
        }
    }
}
