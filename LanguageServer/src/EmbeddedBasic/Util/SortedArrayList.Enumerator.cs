using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace EmbeddedBasic.Util
{
    partial class SortedArrayList<TValue>
    {
        [DebuggerStepThrough]
        public struct Enumerator : IEnumerator<TValue>, IDisposable, IEnumerator
        {
            private readonly SortedArrayList<TValue> _list;

            private int _index;

            private readonly int _version;

            private TValue _current;

            public TValue Current => _current;

            object? IEnumerator.Current
            {
                get
                {
                    if (_index == 0 || _index == _list._size + 1) throw new InvalidOperationException();
                    return Current;
                }
            }

            internal Enumerator(SortedArrayList<TValue> list)
            {
                _list = list;
                _index = 0;
                _version = list._version;
                _current = default(TValue)!;
            }

            public void Dispose() { }

            public bool MoveNext()
            {
                SortedArrayList<TValue> list = _list;
                if (_version == list._version && (uint)_index < (uint)list._size)
                {
                    _current = list._items[_index];
                    _index++;
                    return true;
                }
                return MoveNextRare();
            }

            private bool MoveNextRare()
            {
                if (_version != _list._version) throw new InvalidOperationException();
                _index = _list._size + 1;
                _current = default(TValue)!;
                return false;
            }

            void IEnumerator.Reset()
            {
                if (_version != _list._version) throw new InvalidOperationException();
                _index = 0;
                _current = default(TValue)!;
            }
        }
    }
}
