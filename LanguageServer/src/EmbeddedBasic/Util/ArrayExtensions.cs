﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace EmbeddedBasic.Util
{
    public static class ArrayExtensions
    {
        public static int BinarySearch<TKey, TValue>(
            this TValue[] array,
            int index,
            int length,
            TKey key)
            where TKey : notnull
            where TValue : notnull, IComparable<TKey>
        {
            int lowerIndex = index;
            int upperIndex = index + length - 1;

            while (lowerIndex <= upperIndex)
            {
                int middleIndex = lowerIndex + (upperIndex - lowerIndex >> 1);
                int cmp = array[middleIndex].CompareTo(key);
                if (cmp == 0) return middleIndex;
                if (cmp < 0) lowerIndex = middleIndex + 1;
                else upperIndex = middleIndex - 1;
            }
            return ~lowerIndex;
        }

        public static bool TryGetValueAtOrAbove<TKey, TValue>(
            this TValue[] items,
            int size,
            TKey key,
            [MaybeNullWhen(false)] out TValue value)
            where TKey : notnull
            where TValue : notnull, IComparable<TKey>
        {
            int num = items.BinarySearch(0, size, key);
            if (num < 0) num = (~num) - 1;

            if (num >= 0 && num < items.Length)
            {
                value = items[num];
                return true;
            }
            value = default(TValue);
            return false;
        }

        public static bool TryGetValueAtOrBelow<TKey, TValue>(
            this TValue[] items,
            int size,
            TKey key,
            [MaybeNullWhen(false)] out TValue value)
            where TKey : notnull
            where TValue : notnull, IComparable<TKey>
        {
            int num = items.BinarySearch(0, size, key);
            if (num < 0) num = ~num;

            if (num >= 0 && num < items.Length)
            {
                value = items[num];
                return true;
            }
            value = default(TValue);
            return false;
        }

        public static bool TryGetValue<TKey, TValue>(
            this TValue[] items,
            int size,
            TKey key,
            [MaybeNullWhen(false)] out TValue value)
            where TKey : notnull
            where TValue : notnull, IComparable<TKey>
        {
            int num = items.IndexOf(size, key);
            if (num >= 0)
            {
                value = items[num];
                return true;
            }
            value = default(TValue);
            return false;
        }

        public static int IndexOf<TKey, TValue>(this TValue[] items, int size, TKey key)
            where TKey : notnull
            where TValue : notnull, IComparable<TKey>
        {
            int num = items.BinarySearch(0, size, key);
            if (num < 0) return -1;
            return num;
        }
    }
}
