using EmbeddedBasic.LanguageServer.Syntax.Nodes;

namespace EmbeddedBasic.LanguageServer
{
    public static partial class ErrorMessages
    {
        public static string GetString(string name) => ResourceManager.GetString(name) ?? name;

        public static string GetString(TokenKind kind) => GetString(kind.ToString() + "Token");
    }
}
