namespace EmbeddedBasic.LanguageServer
{
    /// <summary>
    /// A line position inside a source text.
    /// </summary>
    public readonly struct LinePosition
    {
        /// <summary>
        /// An invalid line position.
        /// </summary>
        public static readonly LinePosition Invalid = new(-1, -1);

        /// <summary>
        /// The line number inside the source text.
        /// </summary>
        public readonly int Line;

        /// <summary>
        /// The character position inside the source text.
        /// </summary>
        public readonly int Character;

        /// <summary>
        /// Constructs a copy of the given text position.
        /// </summary>
        /// <param name="line">The number of the line.</param>
        /// <param name="character">The position of the character on the line.</param>
        public LinePosition(int line, int character)
        {
            Line = line;
            Character = character;
        }

        public override string ToString() => $"{Line}/{Character}";
    }
}
