using System;

namespace EmbeddedBasic.LanguageServer
{
    public readonly struct TextLine : IComparable<TextLine>, IComparable<int>
    {
        /// <summary>
        /// The line number inside the source text.
        /// </summary>
        public readonly int Line;

        /// <summary>
        /// The absolute position of the line start inside the source text.
        /// </summary>
        public readonly int Absolute;

        /// <summary>
        /// Constructs a copy of the given text position.
        /// </summary>
        /// <param name="line">The number of the line.</param>
        /// <param name="absolute">The absolute position of the line start.</param>
        public TextLine(int line, int absolute)
        {
            Line = line;
            Absolute = absolute;
        }

        public int CompareTo(TextLine other) => this.Absolute.CompareTo(other.Absolute);

        public int CompareTo(int other) => this.Absolute.CompareTo(other);
    }
}
