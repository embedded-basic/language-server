using System;
using System.Collections.ObjectModel;
using EmbeddedBasic.Util;

namespace EmbeddedBasic.LanguageServer
{
    public class TextLineList : SortedArrayList<TextLine>
    {
        public class ReadOnly : ReadOnlyCollection<TextLine>
        {
            private TextLineList _linePositions;

            public ReadOnly(TextLineList linePositions) : base(linePositions) { _linePositions = linePositions; }

            /// <summary>
            /// Try to get an abolute position.
            /// </summary>
            /// <param name="linePosition">The position with line number and character number to get.</param>
            /// <param name="absolute">The absolute position or -1.</param>
            /// <returns>Does the position exist?</returns>
            public bool TryGetAbsolute(LinePosition linePosition, out int absolute)
                => _linePositions.TryGetAbsolute(linePosition, out absolute);

            /// <summary>
            /// Try to get an abolute position.
            /// </summary>
            /// <param name="line">The line number or -1.</param>
            /// <param name="character">The character number on the line or -1.</param>
            /// <param name="absolute">The absolute position or -1.</param>
            /// <returns>Does the position exist?</returns>
            public bool TryGetAbsolute(int line, int character, out int absolute)
                => _linePositions.TryGetAbsolute(line, character, out absolute);

            /// <summary>
            /// Try to get the line number and character number by its absolute position.
            /// </summary>
            /// <param name="absolute">The absolute position of the line.</param>
            /// <param name="linePosition">The text position or invalid.</param>
            /// <returns>Does the position exist?</returns>
            public bool TryGetLinePosition(int absolute, out LinePosition linePosition)
                => _linePositions.TryGetLinePosition(absolute, out linePosition);
        }

        private ReadOnly? _readOnly = null;

        public new ReadOnly AsReadOnly()
        {
            if (_readOnly != null) return _readOnly;
            return _readOnly = new(this);
        }

        private static readonly Func<TextLine, int> GetPosition = linePosition => linePosition.Absolute;

        /// <summary>
        /// Try to get an abolute position.
        /// </summary>
        /// <param name="linePosition">The position with line number and character number to get.</param>
        /// <param name="absolute">The absolute position or -1.</param>
        /// <returns>Does the position exist?</returns>
        public bool TryGetAbsolute(LinePosition linePosition, out int absolute)
            => TryGetAbsolute(linePosition.Line, linePosition.Character, out absolute);

        /// <summary>
        /// Try to get an abolute position.
        /// </summary>
        /// <param name="line">The line number or -1.</param>
        /// <param name="character">The character number on the line or -1.</param>
        /// <param name="absolute">The absolute position or -1.</param>
        /// <returns>Does the position exist?</returns>
        public bool TryGetAbsolute(int line, int character, out int absolute)
        {
            // Non-zero numbers are used for line/character combinations.
            if (line > 0 && line <= _items.Length)
            {
                absolute = _items[line - 1].Absolute + character - 1;
                return true;
            }
            absolute = -1;
            return false;
        }

        /// <summary>
        /// Try to get the line number and character number by its absolute position.
        /// </summary>
        /// <param name="absolute">The absolute position of the line.</param>
        /// <param name="linePosition">The text position or invalid.</param>
        /// <returns>Does the position exist?</returns>
        public bool TryGetLinePosition(int absolute, out LinePosition linePosition)
        {
            if (_items.TryGetValueAtOrAbove(_size, absolute, out var textLine))
            {
                linePosition = new(textLine.Line, 1 + absolute - textLine.Absolute);
                return true;
            }
            linePosition = LinePosition.Invalid;
            return false;
        }
    }
}
