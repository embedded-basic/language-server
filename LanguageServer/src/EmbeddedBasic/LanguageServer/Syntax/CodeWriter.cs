using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using EmbeddedBasic.Util;
using EmbeddedBasic.LanguageServer.Syntax.Nodes;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Directives;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions.Literals;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Statements;

namespace EmbeddedBasic.LanguageServer.Syntax
{
    public sealed class CodeWriter : SyntaxNodeVisitor
    {
        private StringBuilder _output = new();

        private readonly Dictionary<int, string> _indentationByLevel = new();

        private readonly Dictionary<int, string> _commentIndentationByLevel = new();

        private int _suppressCommentsCount = 0;

        private int _indentationLevel;

        private int IndentationLevel
        {
            get => _indentationLevel;
            set
            {
                _indentationLevel = value;
                if (!_indentationByLevel.ContainsKey(value))
                {
                    string indentation = string.Empty;

                    for (int i = 0; i < value; i++)
                    {
                        indentation += "    ";
                    }
                    _indentationByLevel[value] = indentation;
                    _commentIndentationByLevel[value] = "\n" + indentation + "// ";
                }
            }
        }

        private string Indentation => _indentationByLevel[_indentationLevel];

        private string CommentIndentation => _commentIndentationByLevel[_indentationLevel];

        public CodeWriter() => IndentationLevel = 0;

        public string GetText(EbSyntaxTree tree)
        {
            this.Visit(tree.Root);
            return _output.ToString();
        }

        public void WriteFile(EbSyntaxTree tree, string dirpath, Func<string, string> filenameSelector)
        {
            this.Visit(tree.Root);
            var code = _output.ToString();
            var filepath = PathHelper.Join(dirpath, filenameSelector(tree.SourceFile.FileName));
            IO.EnsureDirectory(dirpath);
            File.WriteAllText(filepath, code, Encoding.UTF8);
        }

        public void WriteFile(EbSyntaxTree tree, string dirpath)
            => WriteFile(tree, dirpath, DefaultFilenameSelector);

        public static readonly Func<string, string> DefaultFilenameSelector = (filename) => filename;

        private void WriteIndent()
        {
            if (_suppressCommentsCount == 0) _output.Append(Indentation);
        }

        private void WriteIndentLine()
        {
            if (_suppressCommentsCount == 0) _output.AppendLine(Indentation);
        }

        private void WriteIndent(object obj)
        {

            if (_suppressCommentsCount == 0) _output.Append(Indentation);
            _output.Append(obj);
        }

        private void Write(SyntaxNode node) => _output.Append(node);

        private void Write(object obj) => _output.Append(obj);

        private void WriteLine(SyntaxNode node) => _output.AppendLine(node.ToString());

        private void WriteLine(object obj) => _output.AppendLine(obj.ToString());

        private void WriteIndentLine(object obj)
        {
            WriteIndent();
            _output.AppendLine(obj.ToString());
        }

        private void WriteLine() => _output.AppendLine();

        private void WriteComment(CommentableNode commentableNode)
        {
            if (!string.IsNullOrWhiteSpace(commentableNode.Comment) && _suppressCommentsCount == 0)
            {
                WriteIndent();
                Write("// ");
                var comment = commentableNode.Comment;
                comment = comment.Replace("\r\n", "\n");
                comment = comment.Replace("\n", CommentIndentation);
                WriteLine(comment);
            }
        }

        // protected internal override void VisitToken(Token token) => Write(token);

        protected internal override void VisitDataType(DataType dataType) => Write(dataType.Kind switch
        {
            DataTypeKind.Bool => "bool",
            DataTypeKind.Byte => "byte",
            DataTypeKind.Char => "char",
            DataTypeKind.Float => "float",
            DataTypeKind.Int => "int",
            DataTypeKind.Short => "short",
            DataTypeKind.Word => "word",
            DataTypeKind.Void => "void",
            _ => string.Empty
        });

        protected internal override void VisitBooleanLiteral(BooleanLiteral boolLiteral)
        => Write(boolLiteral.ToCodeString());

        protected internal override void VisitBitLiteral(BitLiteral bitLiteral)
        => Write(bitLiteral.ToCodeString());

        protected internal override void VisitBinNumberLiteral(BinNumberLiteral binNumberLiteral)
        => Write(binNumberLiteral.ToCodeString());

        protected internal override void VisitHexNumberLiteral(HexNumberLiteral hexNumberLiteral)
        => Write(hexNumberLiteral.ToCodeString());

        protected internal override void VisitIntegerLiteral(IntegerLiteral integerLiteral)
        => Write(integerLiteral.ToCodeString());

        protected internal override void VisitDecimalLiteral(DecimalLiteral decimalLiteral)
        => Write(decimalLiteral.ToCodeString());

        protected internal override void VisitDateLiteral(DateLiteral dateLiteral)
        => Write(dateLiteral.ToCodeString());

        protected internal override void VisitTimeLiteral(TimeLiteral timeLiteral)
        => Write(timeLiteral.ToCodeString());

        protected internal override void VisitCharLiteral(CharLiteral charLiteral)
        => Write(charLiteral.Value);

        protected internal override void VisitStringLiteral(StringLiteral stringLiteral)
        {
            Write('"');
            Write(stringLiteral.StringValue);
            Write('"');
        }

        protected internal override void VisitDeviceNameLiteral(DeviceNameLiteral deviceNameLiteral)
        {
            Write('#');
            Write(deviceNameLiteral.DeviceName);
        }

        protected internal override void VisitDeviceIdLiteral(DeviceIdLiteral deviceIdLiteral)
        {
            Write('#');
            Write(deviceIdLiteral.DeviceId);
        }

        protected internal override void VisitIdentifierExpression(IdentifierExpression identifier)
        => Write(identifier.FullName);

        protected internal override void VisitPostfixExpression(PostfixExpression postfixExpression)
        {
            Write(postfixExpression.Operand);
            Write(postfixExpression.Operator);
        }

        private static string OperatorToCode(PrefixOperatorKind kind) => kind switch
        {
            PrefixOperatorKind.Positive => "+",
            PrefixOperatorKind.NegateNumber => "-",
            PrefixOperatorKind.NegateLogical => "not ",
            PrefixOperatorKind.NegateBitwise => "neg ",
            _ => string.Empty
        };

        protected internal override void VisitPrefixExpression(PrefixExpression prefixExpression)
        {
            Write(OperatorToCode(prefixExpression.Operator));
            this.Visit(prefixExpression.Operand);
        }

        private static string OperatorToCode(BinaryOperatorKind kind) => kind switch
        {
            BinaryOperatorKind.Add => "+",
            BinaryOperatorKind.Subtract => "-",
            BinaryOperatorKind.Multiply => "*",
            BinaryOperatorKind.Divide => "/",
            BinaryOperatorKind.Modulo => "mod",
            BinaryOperatorKind.ShiftLeft => "<<",
            BinaryOperatorKind.ShiftRight => ">>",
            BinaryOperatorKind.And => "and",
            BinaryOperatorKind.Or => "or",
            BinaryOperatorKind.Xor => "xor",
            BinaryOperatorKind.LessThan => "<",
            BinaryOperatorKind.LessThanOrEqualTo => "<=",
            BinaryOperatorKind.GreaterThan => ">",
            BinaryOperatorKind.GreaterThanOrEqualTo => ">=",
            BinaryOperatorKind.NotEqualTo => "<>",
            BinaryOperatorKind.EqualTo => "==",
            _ => string.Empty
        };

        protected internal override void VisitBinaryExpression(BinaryExpression binaryExpression)
        {
            var useGrouping1 = binaryExpression.Operand1.NeedsGrouping(binaryExpression);
            var useGrouping2 = binaryExpression.Operand2.NeedsGrouping(binaryExpression);

            if (useGrouping1) Write("(");
            this.Visit(binaryExpression.Operand1);
            if (useGrouping1) Write(")");
            Write(" ");
            Write(OperatorToCode(binaryExpression.Operator));
            Write(" ");
            if (useGrouping2) Write("(");
            this.Visit(binaryExpression.Operand2);
            if (useGrouping2) Write(")");
        }

        protected internal override void VisitCallExpression(CallExpression callExpression)
        {
            Write(callExpression.Name.FullName);
            Write("(");
            this.Visit(
                callExpression.Arguments,
                inBetween: () => Write(", "));
            Write(")");
        }

        protected internal override void VisitArrayIndexingExpression(ArrayIndexingExpression arrayIndexingExpression)
        {
            Write(arrayIndexingExpression.Name.FullName);
            Write("[");
            this.Visit(arrayIndexingExpression.Index);
            Write("]");
        }

        protected internal override void VisitVariableDeclaration(VariableDeclaration variableDeclaration)
        {
            WriteIndent();
            this.Visit(variableDeclaration.DataType);
            Write(" ");
            this.Visit(
                variableDeclaration.SubDeclarations,
                inBetween: () => Write(", "));
        }

        protected internal override void VisitVariableSubDeclaration(VariableSubDeclaration variableSubDeclaration)
        {
            Write(variableSubDeclaration.Name);
            if (variableSubDeclaration.Value != null)
            {
                Write(" = ");
                this.Visit(variableSubDeclaration.Value);
            }
        }

        protected internal override void VisitVariableArraySubDeclaration(VariableArraySubDeclaration variableArraySubDeclaration)
        {
            Write(variableArraySubDeclaration.Name);
            Write("[");
            Write(variableArraySubDeclaration.ElementCount);
            Write("]");
            if (variableArraySubDeclaration.Value != null)
            {
                Write(" = ");
                this.Visit(variableArraySubDeclaration.Value);
            }
        }

        protected internal override void VisitConstantDeclaration(ConstantDeclaration constantDeclaration)
        {
            WriteComment(constantDeclaration);
            WriteIndent();
            Write("const ");
            Write(constantDeclaration.Name);
            Write(" = ");
            this.Visit(constantDeclaration.Value);
        }

        protected internal override void VisitImportDirective(ImportDirective importDirective)
        {
            WriteComment(importDirective);
            Write("import ");
            Write(importDirective.Identifier.FullName);
            if (importDirective.Path != null)
            {
                Write(" from \"");
                Write(importDirective.Path.StringValue);
                Write("\"");
            }
        }

        protected internal override void VisitIncludeDirective(IncludeDirective includeDirective)
        {
            Write("#include ");
            Write(" from \"");
            Write(includeDirective.Path.StringValue);
            Write("\"");
        }

        protected internal override void VisitTargetDirective(TargetDirective targetDirective)
        {
            Write("#target ");
            this.Visit(targetDirective.Feature);
        }

        protected internal override void VisitDefineDirective(DefineDirective defineDirective)
        {
            Write("#define ");
            Write(defineDirective.Name);
            if (defineDirective.ParameterIdentifiers.Count > 0)
            {
                Write("(");
                Write(defineDirective.ParameterIdentifiers[0].Lexeme);
                if (defineDirective.ParameterIdentifiers.Count > 1)
                {
                    Write(", ");
                    for (int i = 0; i < defineDirective.ParameterIdentifiers.Count; i++)
                    {
                        Write(defineDirective.ParameterIdentifiers[i].Lexeme);
                    }
                }
                Write(")");
            }
            foreach (var token in defineDirective.ContentTokens)
            {
                Write(token.Lexeme);
            }
        }

        protected internal override void VisitBlock(Block block)
        {
            IndentationLevel++;
            this.Visit(
                block.Statements,
                inBetween: () => Write("\n\n"));
            IndentationLevel--;
        }

        protected internal override void VisitLoop(Loop loop)
        {
            WriteComment(loop);
            WriteIndent();
            WriteLine("loop");
            IndentationLevel++;
            this.Visit(
                loop.Statements,
                inBetween: () => Write("\n\n"));
            IndentationLevel--;
            WriteLine();
            WriteIndent();
            Write("endloop");
        }

        protected internal override void VisitWhileLoop(WhileLoop whileLoop)
        {
            WriteComment(whileLoop);
            WriteIndent();
            Write("while ");
            this.Visit(whileLoop.Condition);
            WriteLine();
            IndentationLevel++;
            this.Visit(
                whileLoop.Statements,
                inBetween: () => Write("\n\n"));
            IndentationLevel--;
            WriteLine();
            WriteIndent();
            Write("endwhile");
        }

        protected internal override void VisitForLoop(ForLoop forLoop)
        {
            WriteComment(forLoop);
            WriteIndent();
            Write("for ");
            Write(forLoop.Assignment.Name);
            Write(" = ");
            this.Visit(forLoop.Assignment.Value);
            Write(" to ");
            this.Visit(forLoop.Target);
            if (forLoop.Step != null)
            {
                Write(" step ");
                this.Visit(forLoop.Step);
            }
            WriteLine();
            IndentationLevel++;
            this.Visit(
                forLoop.Statements,
                inBetween: () => Write("\n\n"));
            IndentationLevel--;
            WriteLine();
            WriteIndent();
            Write("endfor");
        }

        protected internal override void VisitIfStatement(IfStatement ifStatement)
        {
            WriteComment(ifStatement);
            if (ifStatement.IfBranches.Count == 1 && ifStatement.ElseBranch == null)
            {
                WriteIfBranchOnOneLine(ifStatement.IfBranches[0]);
            }
            else
            {
                WriteIndent();
                this.Visit(
                    ifStatement.IfBranches,
                    inBetween: () => { WriteLine(); WriteIndent("else"); });

                if (ifStatement.ElseBranch != null)
                {
                    WriteLine();
                    WriteIndentLine("else");
                    this.Visit(ifStatement.ElseBranch);
                }
                WriteLine();
                WriteIndent("endif");
            }
        }

        private void WriteIfBranchOnOneLine(IfBranch ifBranch)
        {
            if (ifBranch.Statements.Count == 1)
            {
                WriteComment(ifBranch.Statements[0]);
            }
            WriteIndent();
            Write("if ");
            this.Visit(ifBranch.Condition);
            Write(" ");

            if (ifBranch.Statements.Count < 1 || !(ifBranch.Statements[0] is JumpStatement))
            {
                Write("then ");
            }
            _suppressCommentsCount++;
            this.Visit(
                ifBranch.Statements,
                inBetween: () => Write(" : "));
            _suppressCommentsCount--;
        }

        protected internal override void VisitIfBranch(IfBranch ifBranch)
        {
            Write("if ");
            this.Visit(ifBranch.Condition);
            WriteLine(" then");
            IndentationLevel++;
            this.Visit(
                ifBranch.Statements,
                inBetween: () => Write("\n\n"));
            IndentationLevel--;
        }

        protected internal override void VisitSelectStatement(SelectStatement selectStatement)
        {
            WriteComment(selectStatement);
            WriteIndent();
            Write("select ");
            WriteLine(selectStatement.Identifier);
            IndentationLevel++;
            this.Visit(
                selectStatement.Cases,
                inBetween: () => WriteLine());
            WriteLine();
            if (selectStatement.DefaultCase != null)
            {
                // if (selectStatement.Cases.Length > 0) Write("\n\n");
                this.Visit(selectStatement.DefaultCase);
                WriteLine();
            }
            IndentationLevel--;
            WriteIndent("endselect");
        }

        protected internal override void VisitSelectDefaultCase(SelectDefaultCase defaultCase)
        {
            WriteIndentLine("case else");
            // WriteLine(defaultCase.);
            IndentationLevel++;
            this.Visit(
                defaultCase.Statements,
                inBetween: () => Write("\n\n"));
            IndentationLevel--;
        }

        protected internal override void VisitSelectSingleCase(SelectSingleCase singleCase)
        {
            WriteIndent();
            Write("case ");
            if (singleCase.Condition != null)
            {
                this.Visit(singleCase.Condition);
            }
            WriteLine();
            IndentationLevel++;
            this.Visit(
                singleCase.Statements,
                inBetween: () => Write("\n\n"));
            IndentationLevel--;
        }

        protected internal override void VisitSelectListCase(SelectListCase listCase)
        {
            WriteIndent();
            Write("case ");
            this.Visit(
                listCase.Conditions,
                inBetween: () => Write(", "));
            WriteLine();
            IndentationLevel++;
            this.Visit(
                listCase.Statements,
                inBetween: () => Write("\n\n"));
            IndentationLevel--;
        }

        protected internal override void VisitSelectRangeCase(SelectRangeCase rangeCase)
        {
            WriteIndent();
            Write("case ");
            this.Visit(rangeCase.FromValue);
            Write(" to ");
            this.Visit(rangeCase.ToValue);
            WriteLine();
            IndentationLevel++;
            this.Visit(
                rangeCase.Statements,
                inBetween: () => Write("\n\n"));
            IndentationLevel--;
        }

        protected internal override void VisitLabelStatement(LabelStatement labelStatement)
        {
            var indentationLevel = IndentationLevel;
            if (indentationLevel > 0) IndentationLevel--;
            WriteComment(labelStatement);
            WriteIndent();
            Write(labelStatement.Label);
            Write(":");
            if (indentationLevel > 0) IndentationLevel++;
        }

        protected internal override void VisitReturnStatement(ReturnStatement returnStatement)
        {
            WriteComment(returnStatement);
            WriteIndent();
            Write("return");
            if (returnStatement.Expression != null)
            {
                Write(" ");
                this.Visit(returnStatement.Expression);
            }
        }

        protected internal override void VisitExitStatement(ExitStatement exitStatement)
        {
            WriteComment(exitStatement);
            WriteIndent();
            Write("exit");
        }

        protected internal override void VisitGotoStatement(GotoStatement gotoStatement)
        {
            WriteComment(gotoStatement);
            WriteIndent();
            Write("goto ");
            Write(gotoStatement.Label);
        }

        protected internal override void VisitWaitStatement(WaitStatement waitStatement)
        {
            WriteComment(waitStatement);
            WriteIndent();
            Write("wait ");
            this.Visit(waitStatement.Condition);

            if (waitStatement.Timeout != null)
            {
                Write(", ");
                this.Visit(waitStatement.Timeout);

                if (waitStatement.JumpStatement != null)
                {
                    Write(", ");
                    _suppressCommentsCount++;
                    if (waitStatement.JumpStatement is GotoStatement gotoStatement) Write(gotoStatement.Label);
                    else this.Visit(waitStatement.JumpStatement);
                    _suppressCommentsCount--;
                }
            }
        }

        protected internal override void VisitOnGotoStatement(OnGotoStatement onGotoStatement)
        {
            WriteComment(onGotoStatement);
            WriteIndent();
            Write("on ");
            Write(onGotoStatement.IndexName);
            Write(" goto ");
            Write(string.Join(", ", onGotoStatement.Labels));
        }

        protected internal override void VisitOnFunctionCall(OnFunctionCall onFunctionCall)
        {
            WriteComment(onFunctionCall);
            WriteIndent();
            Write("on ");
            Write(onFunctionCall.IndexName);
            Write(" call ");
            Write(string.Join(", ", onFunctionCall.Names));
        }

        protected internal override void VisitFunctionCall(FunctionCall callStatement)
        {
            WriteComment(callStatement);
            WriteIndent();
            this.Visit(callStatement.Expression);
        }

        protected internal override void VisitAssignmentStatement(AssignmentStatement assignment)
        {
            WriteComment(assignment);
            WriteIndent();
            Write(assignment.Name);
            Write(" = ");
            this.Visit(assignment.Value);
        }

        protected internal override void VisitCommandCall(CommandCall builtinStatement)
        {
            WriteComment(builtinStatement);
            WriteIndent();
            Write(builtinStatement.Name);
            Write(" ");
            this.Visit(
                builtinStatement.Arguments,
                inBetween: () => Write(", "));
        }

        protected internal override void VisitFunctionDeclaration(FunctionDeclaration functionDeclaration)
        {
            WriteComment(functionDeclaration);
            Write("function ");
            if (functionDeclaration.ReturnType.Kind != DataTypeKind.Void)
            {
                this.Visit(functionDeclaration.ReturnType);
                Write(" ");
            }
            Write(functionDeclaration.Name);
            Write("(");
            this.Visit(
                functionDeclaration.Parameters,
                inBetween: () => Write(", "));
            WriteLine(")");
            IndentationLevel++;
            this.Visit(
                functionDeclaration.Statements,
                inBetween: () => Write("\n\n"));
            IndentationLevel--;
            WriteLine();
            WriteLine("endfunction");
        }

        protected internal override void VisitFunctionParameterDeclaration(FunctionParameterDeclaration parameterDeclaration)
        {
            this.Visit(parameterDeclaration.DataType);
            Write(" ");
            Write(parameterDeclaration.Name);
        }

        protected internal override void VisitRoot(RootNode root)
    => this.Visit(root.TopLevelNodes, inBetween: () => Write("\n\n"));
    }
}
