using EmbeddedBasic.LanguageServer.Syntax.Nodes;

namespace EmbeddedBasic.LanguageServer.Syntax
{
    partial class SourceFile
    {
        /// <summary>
        /// A source file state used while lexing.
        /// </summary>
        public sealed class State
        {
            /// <summary>
            /// The state of the source file from which this states source file has been included or imported.
            /// </summary>
            public State? OuterState { get; set; }

            /// <summary>
            /// The syntax tree to which this state belongs.
            /// </summary>
            private readonly SyntaxTree _tree;

            public readonly SourceFile SourceFile;

            /// <summary>
            /// A reference to the source text, to avoid indirection.
            /// </summary>
            public readonly string Source;

            /// <summary>
            /// The current absolute position inside the source text.
            /// </summary>
            public int Absolute { get; private set; }

            /// <summary>
            /// The current line number inside the source text.
            /// </summary>
            public int Line { get; private set; }

            /// <summary>
            /// Create a source file state used while lexing.
            /// </summary>
            public State(SyntaxTree tree, SourceFile sourceFile, State? outerState = null)
            {
                _tree = tree;
                SourceFile = sourceFile;
                Source = sourceFile.Text;
                OuterState = outerState;
                RegisterLinebreak();
            }

            public void Reset()
            {
                SourceFile._linePositions.Clear();
                Absolute = 0;
                Line = 0;
                RegisterLinebreak();
            }

            private void RegisterLinebreak() => SourceFile._linePositions.Add(new TextLine(++Line, Absolute));

            public string GetLexeme(int length) => Source.Substring(Absolute, length);

            public void Discard(int length = 1) => Absolute += length;

            public void DiscardLinebreak(int length = 1)
            {
                Absolute += length;
                RegisterLinebreak();
            }

            public Token Set(TokenKind kind, int length, string lexeme)
                => new Token(_tree, kind, SourceFile, Absolute, Absolute += length, lexeme);

            public Token SetLinebreak(TokenKind kind, int length, string lexeme)
            {
                var token = new Token(_tree, kind, SourceFile, Absolute, Absolute += length, lexeme);
                RegisterLinebreak();
                return token;
            }
        }
    }
}
