using System;
using System.Collections.Generic;
using EmbeddedBasic.LanguageServer.Syntax.Nodes;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Directives;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions.Literals;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Statements;

namespace EmbeddedBasic.LanguageServer.Syntax
{
    public abstract class SyntaxNodeVisitor
    {
        public void Visit(SyntaxNode? node) => node?.Accept(this);

        public void VisitChilds(SyntaxNode? node)
        {
            if (node != null)
            {
                foreach (var childNode in node.ChildNodes) { childNode?.Accept(this); }
            }
        }

        public void Visit(IReadOnlyList<SyntaxNode?> nodes)
        {
            foreach (var node in nodes) { node?.Accept(this); }
        }

        public void Visit(
            IReadOnlyList<SyntaxNode?> nodes,
            Action inBetween,
            Action? before = null,
            Action? after = null)
        {
            int index = 0;
            SyntaxNode? node = null;

            while (index < nodes.Count)
            {
                node = nodes[index++];
                if (node != null) break;
            }

            if (node != null)
            {
                before?.Invoke();
                node.Accept(this);

                while (index < nodes.Count)
                {
                    node = nodes[index++];
                    if (node != null)
                    {
                        inBetween();
                        node.Accept(this);
                    }
                }
                after?.Invoke();
            }
        }

        protected internal virtual void VisitToken(Token token) { }
        protected internal virtual void VisitDataType(DataType dataType) => this.VisitChilds(dataType);
        protected internal virtual void VisitBooleanLiteral(BooleanLiteral boolLiteral) => this.VisitChilds(boolLiteral);
        protected internal virtual void VisitBitLiteral(BitLiteral bitLiteral) => this.VisitChilds(bitLiteral);
        protected internal virtual void VisitBinNumberLiteral(BinNumberLiteral binNumberLiteral) => this.VisitChilds(binNumberLiteral);
        protected internal virtual void VisitHexNumberLiteral(HexNumberLiteral hexNumberLiteral) => this.VisitChilds(hexNumberLiteral);
        protected internal virtual void VisitIntegerLiteral(IntegerLiteral integerLiteral) => this.VisitChilds(integerLiteral);
        protected internal virtual void VisitDecimalLiteral(DecimalLiteral decimalLiteral) => this.VisitChilds(decimalLiteral);
        protected internal virtual void VisitDateLiteral(DateLiteral dateLiteral) => this.VisitChilds(dateLiteral);
        protected internal virtual void VisitTimeLiteral(TimeLiteral timeLiteral) => this.VisitChilds(timeLiteral);
        protected internal virtual void VisitCharLiteral(CharLiteral charLiteral) => this.VisitChilds(charLiteral);
        protected internal virtual void VisitStringLiteral(StringLiteral stringLiteral) => this.VisitChilds(stringLiteral);
        protected internal virtual void VisitDeviceNameLiteral(DeviceNameLiteral deviceNameLiteral) => this.VisitChilds(deviceNameLiteral);
        protected internal virtual void VisitDeviceIdLiteral(DeviceIdLiteral deviceIdLiteral) => this.VisitChilds(deviceIdLiteral);
        protected internal virtual void VisitIdentifierExpression(IdentifierExpression identifier) => this.VisitChilds(identifier);
        protected internal virtual void VisitPostfixExpression(PostfixExpression postfixExpression) => this.VisitChilds(postfixExpression);
        protected internal virtual void VisitPrefixExpression(PrefixExpression prefixExpression) => this.VisitChilds(prefixExpression);
        protected internal virtual void VisitBinaryExpression(BinaryExpression binaryExpression) => this.VisitChilds(binaryExpression);
        protected internal virtual void VisitCallExpression(CallExpression callExpression) => this.VisitChilds(callExpression);
        protected internal virtual void VisitArrayIndexingExpression(ArrayIndexingExpression arrayIndexingExpression) => this.VisitChilds(arrayIndexingExpression);
        protected internal virtual void VisitVariableDeclaration(VariableDeclaration variableDeclaration) => this.VisitChilds(variableDeclaration);
        protected internal virtual void VisitVariableSubDeclaration(VariableSubDeclaration variableSubDeclaration) => this.VisitChilds(variableSubDeclaration);
        protected internal virtual void VisitVariableArraySubDeclaration(VariableArraySubDeclaration variableArraySubDeclaration) => this.VisitChilds(variableArraySubDeclaration);
        protected internal virtual void VisitSymbolDeclaration(SymbolDeclaration symbolDeclaration) => this.VisitChilds(symbolDeclaration);
        protected internal virtual void VisitConstantDeclaration(ConstantDeclaration constantDeclaration) => this.VisitChilds(constantDeclaration);
        protected internal virtual void VisitImportDirective(ImportDirective importDirective) => this.VisitChilds(importDirective);
        protected internal virtual void VisitIncludeDirective(IncludeDirective includeDirective) => this.VisitChilds(includeDirective);
        protected internal virtual void VisitTargetDirective(TargetDirective targetDirective) => this.VisitChilds(targetDirective);
        protected internal virtual void VisitDefineDirective(DefineDirective defineDirective) => this.VisitChilds(defineDirective);
        protected internal virtual void VisitBlock(Block block) => this.VisitChilds(block);
        protected internal virtual void VisitLoop(Loop loop) => this.VisitChilds(loop);
        protected internal virtual void VisitWhileLoop(WhileLoop whileLoop) => this.VisitChilds(whileLoop);
        protected internal virtual void VisitForLoop(ForLoop forLoop) => this.VisitChilds(forLoop);
        protected internal virtual void VisitIfStatement(IfStatement ifStatement) => this.VisitChilds(ifStatement);
        protected internal virtual void VisitIfBranch(IfBranch ifBranch) => this.VisitChilds(ifBranch);
        protected internal virtual void VisitSelectStatement(SelectStatement selectStatement) => this.VisitChilds(selectStatement);
        protected internal virtual void VisitSelectDefaultCase(SelectDefaultCase defaultCase) => this.VisitChilds(defaultCase);
        protected internal virtual void VisitSelectSingleCase(SelectSingleCase singleCase) => this.VisitChilds(singleCase);
        protected internal virtual void VisitSelectListCase(SelectListCase listCase) => this.VisitChilds(listCase);
        protected internal virtual void VisitSelectRangeCase(SelectRangeCase rangeCase) => this.VisitChilds(rangeCase);
        protected internal virtual void VisitLabelStatement(LabelStatement labelStatement) => this.VisitChilds(labelStatement);
        protected internal virtual void VisitReturnStatement(ReturnStatement returnStatement) => this.VisitChilds(returnStatement);
        protected internal virtual void VisitExitStatement(ExitStatement exitStatement) => this.VisitChilds(exitStatement);
        protected internal virtual void VisitGotoStatement(GotoStatement gotoStatement) => this.VisitChilds(gotoStatement);
        protected internal virtual void VisitWaitStatement(WaitStatement waitStatement) => this.VisitChilds(waitStatement);
        protected internal virtual void VisitOnGotoStatement(OnGotoStatement onGotoStatement) => this.VisitChilds(onGotoStatement);
        protected internal virtual void VisitOnFunctionCall(OnFunctionCall onFunctionCall) => this.VisitChilds(onFunctionCall);
        protected internal virtual void VisitFunctionCall(FunctionCall callStatement) => this.VisitChilds(callStatement);
        protected internal virtual void VisitAssignmentStatement(AssignmentStatement assignment) => this.VisitChilds(assignment);
        protected internal virtual void VisitCommandCall(CommandCall builtinStatement) => this.VisitChilds(builtinStatement);
        protected internal virtual void VisitFunctionDeclaration(FunctionDeclaration functionDeclaration) => this.VisitChilds(functionDeclaration);
        protected internal virtual void VisitFunctionParameterDeclaration(FunctionParameterDeclaration parameterDeclaration) => this.VisitChilds(parameterDeclaration);
        protected internal virtual void VisitRoot(RootNode root) => this.VisitChilds(root);
    }
}
