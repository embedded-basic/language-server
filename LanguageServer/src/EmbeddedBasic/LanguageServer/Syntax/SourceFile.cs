using System;
using System.IO;
using System.Text;
using EmbeddedBasic.Util;

namespace EmbeddedBasic.LanguageServer.Syntax
{
    /// <summary>
    /// A source file.
    /// </summary>
    public sealed partial class SourceFile : IComparable<SourceFile>, IEquatable<SourceFile>
    {
        private readonly TextLineList _linePositions = new();

        /// <summary>
        /// The absolute positions of all lines in the source text.
        /// </summary>
        public TextLineList.ReadOnly TextLines => _linePositions.AsReadOnly();

        /// <summary>
        /// The extension of source files.
        /// </summary>
        public static readonly string FileExt = ".bas";

        /// <summary>
        /// The path of the directory in which the source file is to be found.
        /// </summary>
        public readonly string DirectoryPath;

        /// <summary>
        /// Name and extension of the source file without path.
        /// </summary>
        public readonly string FileName;

        /// <summary>
        /// The absolute path of the source file.
        /// </summary>
        public readonly string FilePath;

        /// <summary>
        /// The absolute path of the source file.
        /// </summary>
        public readonly bool FileExists;

        /// <summary>
        /// The text of the source file.
        /// </summary>
        public readonly string Text;

        /// <summary>
        /// Create the description of a source file location.
        /// </summary>
        /// <param name="filepath">The path to a source file.</param>
        private SourceFile(string directoryPath, string fileName)
        {
            DirectoryPath = directoryPath;
            FileName = fileName + FileExt;
            FilePath = PathHelper.Join(DirectoryPath, FileName);
            FileExists = GetText(FilePath, out Text);
        }

        /// <summary>
        /// Create the description of a source file location.
        /// </summary>
        /// <param name="filepath">The path to a source file.</param>
        public static SourceFile FromInclude(string filePath, SourceFile? refLocation = null)
        {
            var directoryPath = PathHelper.IsPathFullyQualified(filePath) ?
                Path.GetDirectoryName(filePath) :
                PathHelper.Join(refLocation?.DirectoryPath, Path.GetDirectoryName(filePath));
            var fileName = Path.GetFileNameWithoutExtension(filePath);
            return new SourceFile(directoryPath!, fileName);
        }

        /// <summary>
        /// Create the description of a source file location.
        /// </summary>
        /// <param name="fromPath">A path to import from.</param>
        /// <param name="importName">The name of the import.</param>
        public static SourceFile FromImport(string? fromPath, string importName, SourceFile? refLocation = null)
        {
            if (importName.EndsWith(".bas")) importName = importName.Substring(0, importName.Length - 4);

            var directoryPath = fromPath != null && PathHelper.IsPathFullyQualified(fromPath) ?
                fromPath : Path.GetFullPath(PathHelper.Join(refLocation?.DirectoryPath, fromPath));
            string fileName;

            if (importName.Contains("."))
            {
                var index = importName.LastIndexOf('.');
                directoryPath = PathHelper.Join(directoryPath, importName.Substring(0, index).Replace('.', Path.DirectorySeparatorChar));
                fileName = importName.Substring(index + 1);
            }
            else fileName = importName;

            return new SourceFile(directoryPath, fileName);
        }

        private static bool GetText(string filePath, out string text)
        {
            if (File.Exists(filePath))
            {
                text = File.ReadAllText(filePath, Encoding.UTF8);
                return true;
            }
            text = string.Empty;
            return false;
        }

        public int CompareTo(SourceFile? other) => other != null ? FilePath.CompareTo(other.FilePath) : 1;

        public override bool Equals(object? obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            if (obj is SourceFile sourceFile) return FilePath.Equals(sourceFile.FilePath);

            return base.Equals(obj);
        }

        public bool Equals(SourceFile? obj)
            => object.ReferenceEquals(obj, null) ? false : FilePath.Equals(obj.FilePath);

        public override int GetHashCode() => FilePath.GetHashCode();

        public static bool operator ==(SourceFile? first, SourceFile? second)
            => object.ReferenceEquals(first, null) ? false : first.Equals(second);

        public static bool operator !=(SourceFile? first, SourceFile? second)
            => first == null ? true : !first.Equals(second);

        public override string ToString() => $"\"{FilePath}\"";
    }
}
