namespace EmbeddedBasic.LanguageServer.Syntax
{
    /// <summary>
    /// The precedence of operators.
    /// </summary>

    public enum OperatorPrecedence
    {
        /// <summary>
        /// No precedence
        /// </summary>
        None,
        /// <summary>
        /// Assignment
        /// </summary>
        Assignment,
        /// <summary>
        /// Less/Greater-than, Less/Greater-than-or-Equal, Equal, Not-Equal
        /// </summary>
        Comparison,
        /// <summary>
        /// Add, Subtract, Or, Xor
        /// </summary>
        Sum,
        /// <summary>
        /// Multiply, Divide, Mod, Shift, And
        /// </summary>
        Product,
        /// <summary>
        /// Exponentiation
        /// </summary>
        Exponent,
        /// <summary>
        /// Prefix operator
        /// </summary>
        Prefix,
        /// <summary>
        /// Postfix operator
        /// </summary>
        Postfix,
        /// <summary>
        /// Function call
        /// </summary>
        Call
    }
}
