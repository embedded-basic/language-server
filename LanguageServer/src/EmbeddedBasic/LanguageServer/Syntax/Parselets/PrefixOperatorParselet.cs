using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Parselets
{
    /// <summary>
    /// Parsing fragment for unary prefix operator expressions like `-3`.
    /// </summary>
    public sealed class PrefixOperatorParselet : PrefixParselet
    {
        public int Precedence { get; private set; }

        public PrefixOperatorParselet(Parser parser, int precedence) :
            base(parser)
        {
            Precedence = precedence;
        }

        public override Expression? Parse(Token token)
        {
            var right = _parser.ParseExpression(Precedence);

            return right == null ? right : new PrefixExpression(token, right);
        }
    }
}
