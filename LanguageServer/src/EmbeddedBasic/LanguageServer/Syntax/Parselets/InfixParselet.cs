using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Parselets
{
    /// <summary>
    /// Parsing fragment for infix expressions.
    /// </summary>
    public abstract class InfixParselet
    {
        protected readonly Parser _parser;

        public int Precedence { get; }

        protected InfixParselet(Parser parser, int precedence)
        {
            _parser = parser;
            Precedence = precedence;
        }

        public abstract Expression? Parse(Expression? left, Token token);
    }
}
