using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Parselets
{
    /// <summary>
    /// Parsing fragment for explicitly grouped expressions like `5 - (7 + 3)`.
    /// </summary>
    public sealed class GroupParselet : PrefixParselet
    {
        private readonly new EbParser _parser;

        public GroupParselet(EbParser parser) : base(parser) => _parser = parser;

        public override Expression? Parse(Token token)
        {
            // Opened parenthesis will allow expression to span multiple lines.
            _parser.SuppressLinebreak();
            var expression = _parser.ParseExpression();
            if (!_parser.TryConsume(TokenKind.ParenthesisRight, "Closing parenthesis were expected"))
            {
                _parser.ConsumeUntilIncluding(TokenKind.ParenthesisRight);
            }
            _parser.ResetLinebreakSuppression();
            return expression;
        }
    }
}
