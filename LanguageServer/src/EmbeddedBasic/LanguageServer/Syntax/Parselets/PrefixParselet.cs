using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Parselets
{
    /// <summary>
    /// Parsing fragment for prefix expressions.
    /// </summary>
    public abstract class PrefixParselet
    {
        protected readonly Parser _parser;

        protected PrefixParselet(Parser parser) => _parser = parser;

        public abstract Expression? Parse(Token token);
    }
}
