using System.Collections.Generic;
using EmbeddedBasic.Util;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Parselets
{
    /// <summary>
    /// Parsing fragment for a function call expression like `doMagic(2, "foo", bar)`.
    /// </summary>
    public sealed class CallParselet : InfixParselet
    {
        private readonly new EbParser _parser;

        public CallParselet(EbParser parser) : base(parser, (int)OperatorPrecedence.Call) => _parser = parser;

        public override Expression? Parse(Expression? left, Token token)
        {
            var args = new List<Expression>();
            var lastToken = token;
            _parser.SuppressLinebreak();

            if (!_parser.TryConsume(TokenKind.ParenthesisRight))
            {
                do if (!args.AddIfNonNull(_parser.ParseExpression()))
                    {
                        _parser.NoteError(ErrorMessages.ExpressionStart, _parser.LookAhead());
                    }
                while (_parser.TryConsume(TokenKind.CommaSymbol));

                // Without closing parenthesis there may be leftover tokens,
                // that may have been intended to be included as parameters.
                _parser.TryConsume(TokenKind.ParenthesisRight, out lastToken, ErrorMessages.ClosingParenthesis);
            }
            _parser.ResetLinebreakSuppression();

            if (left is IdentifierExpression)
            {
                return new CallExpression((IdentifierExpression)left, lastToken!, args);
            }
            _parser.NoteError(ErrorMessages.CallIdentifier);
            return left;
        }

        public CallExpression? ParseFunctionCall(Expression? left, Token token) => Parse(left, token) as CallExpression;
    }
}
