using System;
using System.Collections.Generic;
using EmbeddedBasic.Util;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Parselets
{
    /// <summary>
    /// Parsing fragment for indexing arrays like `numbers[3]`.
    /// </summary>
    public sealed class ArrayIndexingParselet : InfixParselet
    {
        private readonly new EbParser _parser;

        public ArrayIndexingParselet(EbParser parser) : base(parser, (int)OperatorPrecedence.Call)
        {
            _parser = parser;
        }

        public override Expression? Parse(Expression? left, Token token)
        {
            var args = new List<Expression>();
            var lastToken = token;
            _parser.SuppressLinebreak();

            var index = _parser.ParseExpression();
            _parser.TryConsume(TokenKind.BracketRight, out lastToken, ErrorMessages.ClosingBrackets);

            _parser.ResetLinebreakSuppression();

            if (left is IdentifierExpression)
            {
                if (index != null)
                {
                    return new ArrayIndexingExpression((IdentifierExpression)left, ((SyntaxNode?)lastToken) ?? index, index);
                }
                else _parser.NoteError(ErrorMessages.ArrayIndexMissing);
            }
            else _parser.NoteError(ErrorMessages.CallIdentifier);
            return left;
        }

        public ArrayIndexingExpression? ParseArrayIndexing(Expression? left, Token token) => Parse(left, token) as ArrayIndexingExpression;
    }
}
