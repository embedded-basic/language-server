using System.Collections.Generic;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Parselets
{
    /// <summary>
    /// Parsing fragment for identifiers like `Foo.Bar`.
    /// </summary>
    public sealed class IdentifierParselet : PrefixParselet
    {
        public IdentifierParselet(Parser parser) : base(parser) { }

        public override Expression? Parse(Token identifier) => ParseIdentifier(identifier);

        public IdentifierExpression ParseIdentifier(Token identifier)
        {
            var identifiers = new List<Token>() { identifier };

            while (_parser.TryConsume(TokenKind.DotSymbol))
            {
                if (_parser.TryConsume(TokenKind.Identifier, out var otherIdentifier, ErrorMessages.Identifier))
                {
                    identifiers.Add(otherIdentifier);
                }
                else break;
            }

            return new IdentifierExpression(identifiers);
        }
    }
}
