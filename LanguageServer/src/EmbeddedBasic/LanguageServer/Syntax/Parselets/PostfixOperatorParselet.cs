using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Parselets
{
    /// <summary>
    /// Parsing fragment for unary postfix operator expressions like `5!`.
    /// </summary>
    public sealed class PostfixOperatorParselet : InfixParselet
    {
        public PostfixOperatorParselet(Parser parser, int precedence) : base(parser, precedence) { }

        public override Expression? Parse(Expression? left, Token token)
        {
            return left == null ? left : new PostfixExpression(token, left);
        }
    }
}
