using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions.Literals;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Parselets
{
    /// <summary>
    /// Parsing fragment for literal expressions like `2`, `"foo"` or `bar`.
    /// </summary>
    public sealed class LiteralParselet : PrefixParselet
    {
        public delegate Literal LiteralFactory(Token token);

        public readonly struct Info
        {
            public readonly LiteralFactory Factory;

            public readonly TokenKind[] Kinds;

            public Info(LiteralFactory factory, params TokenKind[] kinds)
            {
                Factory = factory;
                Kinds = kinds;
            }
        }

        private readonly LiteralFactory _factory;

        public LiteralParselet(Parser parser, LiteralFactory factory) : base(parser) => _factory = factory;

        public override Expression? Parse(Token token) => _factory(token);
    }
}
