using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Parselets
{
    /// <summary>
    /// Parsing fragment for binary infix operator expressions like `2 + 3`.
    /// </summary>
    public sealed class BinaryOperatorParselet : InfixParselet
    {
        private readonly bool _isRightAssociative;

        public BinaryOperatorParselet(Parser parser, int precedence, bool isRightAssociative) :
            base(parser, precedence)
        {
            _isRightAssociative = isRightAssociative;
        }

        public override Expression? Parse(Expression? left, Token token)
        {
            var right = _parser.ParseExpression(Precedence - (_isRightAssociative ? 1 : 0));

            return right == null || left == null ? left : new BinaryExpression(left, token, right);
        }
    }
}
