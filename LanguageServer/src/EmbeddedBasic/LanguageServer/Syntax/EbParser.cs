using System.Collections.Generic;
using System.Runtime.CompilerServices;
using EmbeddedBasic.Util;
using EmbeddedBasic.LanguageServer.Syntax.Nodes;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Directives;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions.Literals;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Statements;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Parselets;

namespace EmbeddedBasic.LanguageServer.Syntax
{
    /// <summary>
    /// The parser for `Embedded BASIC`.
    /// </summary>
    public sealed partial class EbParser : Parser
    {
        private int _linebreakSuppressionCount = 0;

        private readonly IdentifierParselet _identifier;

        private readonly CallParselet _call;

        /// <summary>
        /// Create a parser for `Embedded BASIC`.
        /// </summary>
        /// <param name="context">The parsing context to use.</param>
        public EbParser() :
            base(tokenProvider: new EbPreprocessor())
        {
            RegisterLiterals(
                BooleanLiteral.ParseletInfo, BitLiteral.ParseletInfo,
                BinNumberLiteral.ParseletInfo, HexNumberLiteral.ParseletInfo,
                IntegerLiteral.ParseletInfo, DecimalLiteral.ParseletInfo,
                DateLiteral.ParseletInfo, TimeLiteral.ParseletInfo,
                CharLiteral.ParseletInfo, StringLiteral.ParseletInfo,
                DeviceNameLiteral.ParseletInfo, DeviceIdLiteral.ParseletInfo);

            Register(TokenKind.Identifier, _identifier = new(this));
            Register(TokenKind.ParenthesisLeft, new GroupParselet(this));
            Register(TokenKind.ParenthesisLeft, _call = new(this));
            Register(TokenKind.BracketLeft, new ArrayIndexingParselet(this));

            RegisterPrefix(OperatorPrecedence.Prefix, TokenKind.PlusSymbol, TokenKind.MinusSymbol);

            RegisterInfixLeft(OperatorPrecedence.Comparison,
                TokenKind.LessThanSymbol, TokenKind.GreaterThanSymbol,
                TokenKind.EqualEqualSymbol, TokenKind.LessThanGreaterThanSymbol);

            RegisterInfixLeft(OperatorPrecedence.Sum, TokenKind.PlusSymbol, TokenKind.MinusSymbol);

            RegisterInfixLeft(OperatorPrecedence.Product,
                TokenKind.AsteriskSymbol, TokenKind.SlashSymbol,
                TokenKind.LessThanLessThanSymbol, TokenKind.GreaterThanGreaterThanSymbol);
        }

        /// <summary>
        /// Is the appearance of the given token kind allowed?
        /// </summary>
        /// <param name="kind">The token kind to check for allowance.</param>
        /// <returns>Is the appearance of the given token allowed?</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected override bool IsAllowed(TokenKind kind)
            => kind != TokenKind.Linebreak || _linebreakSuppressionCount == 0;

        /// <summary>
        /// Will suppress the appearance of linebreaks. Don't forget to reset!
        /// </summary>
        public void SuppressLinebreak()
        {
            _linebreakSuppressionCount++;
            TryConsume(TokenKind.Linebreak);
        }

        /// <summary>
        /// Will reset the suppression of linebreaks.
        /// </summary>
        public void ResetLinebreakSuppression()
        {
            if (_linebreakSuppressionCount > 0) _linebreakSuppressionCount--;
        }

        /// <summary>
        /// Parse a syntax tree.
        /// </summary>
        /// <returns>A syntax tree or else null.</returns>
        public EbSyntaxTree Parse(string dirPath, string filename)
        {
            var sourceFile = SourceFile.FromImport(dirPath, filename);
            var topLevelNodes = new List<TopLevelNode>();
            var root = new RootNode(sourceFile, topLevelNodes.AsReadOnly());
            var tree = new EbSyntaxTree(root);
            SyntaxTree = tree;

            if (TokenProvider.Reset(sourceFile))
            {
                while (true)
                {
                    switch (LookAhead().Kind)
                    {
                        case TokenKind.Eof: break;
                        case TokenKind.Linebreak: Consume(); continue;
                        default: topLevelNodes.AddIfNonNull(ParseTopLevelNode()); continue;
                    }
                    break;
                }
                root.SetEnd(tree, LookAhead().End);
            }
            else tree.AddErrorMessage(sourceFile.FilePath, ErrorMessages.FileOpen);
            return tree;
        }

        /// <summary>
        /// Parse a top level node (function declaration or statement).
        /// </summary>
        /// <returns>A top level node or null.</returns>

        private TopLevelNode? ParseTopLevelNode()
        {
            switch (LookAhead().Kind)
            {
                case TokenKind.Eof: return null;
                // case TokenKind.ImportPPKeyword: return ParseImport(Consume());
                case TokenKind.FunctionKeyword: return ParseFunction(Consume());
                default: return ParseStatement();
            }
        }

        /// <summary>
        /// Parse an import directive.
        /// </summary>
        /// <returns>An import directive or else null.</returns>
        private ImportDirective? ParseImport(Token import)
        {
            if (TryConsume(TokenKind.Identifier, out var identifierToken))
            {
                var identifier = _identifier.ParseIdentifier(identifierToken);

                if (TryConsume(TokenKind.FromPPKeyword, out var fromToken))
                {
                    if (TryConsume(TokenKind.StringLiteral, out var stringToken, ErrorMessages.ImportFrom))
                    {
                        return new ImportDirective(
                            ConsumeComment(), import, identifier, new StringLiteral(stringToken));
                    }
                }
                else
                {
                    return new ImportDirective(ConsumeComment(), import, identifier);
                }
            }
            return null;
        }

        /// <summary>
        /// Parse a function declaration.
        /// </summary>
        /// <returns>A function declaration or else null.</returns>
        private FunctionDeclaration ParseFunction(Token functionToken)
        {
            var nameIdentifier = Consume();
            var parameters = new List<FunctionParameterDeclaration>();
            var statements = new List<Statement>();
            DataType returnType;

            if (nameIdentifier.Kind == TokenKind.Identifier)
            {
                returnType = DataType.CreateVoid(nameIdentifier);
            }
            else
            {
                returnType = DataType.CreateNonVoid(nameIdentifier) ?? DataType.CreateVoid(nameIdentifier);
                TryConsume(TokenKind.Identifier, out nameIdentifier, ErrorMessages.FunctionParameter);
            }

            if (TryConsume(TokenKind.ParenthesisLeft))
            {
                // Suppress(TokenKind.Linebreak);
                SuppressLinebreak();
                ParseFunctionParameter(parameters);
                while (TryConsume(TokenKind.CommaSymbol)) ParseFunctionParameter(parameters);
                TryConsume(TokenKind.ParenthesisRight);
                // ResetSuppression(TokenKind.Linebreak);
                ResetLinebreakSuppression();
            }
            var comment = ConsumeComment();

            ParseStatements(statements, TokenKind.EndFunctionKeyword);
            TryConsume(TokenKind.EndFunctionKeyword, out var endFunctionToken);

            return new FunctionDeclaration(
                comment, functionToken, endFunctionToken!,
                nameIdentifier!, returnType!, parameters, statements);
        }

        private void ParseFunctionParameter(List<FunctionParameterDeclaration> parameters)
        {
            var typeOrNameToken = Consume();
            var dataType = DataType.Create(typeOrNameToken);

            if (dataType.Kind == DataTypeKind.Void) NoteError(ErrorMessages.FunctionParameterDataType);
            typeOrNameToken = Consume();
            int elementCount = 1;

            if (TryConsume(TokenKind.BracketLeft) &&
                TryConsume(TokenKind.IntegerLiteral, out var intergerToken) &&
                TryConsume(TokenKind.BracketRight))
            {
                if (TryConsume(TokenKind.IntegerLiteral))
                {
                    elementCount = intergerToken.Lexeme.ParseInt32();

                    TryConsume(TokenKind.BracketRight, ErrorMessages.ArrayEnd);
                }
            }

            if (typeOrNameToken.Kind == TokenKind.Identifier)
            {
                parameters.Add(new FunctionParameterDeclaration(typeOrNameToken, dataType, elementCount, dataType.End));
            }
        }

        /// <summary>
        /// Parse a statement.
        /// </summary>
        /// <returns>A statement or null.</returns>
        private Statement? ParseStatement()
        {
            var token = LookAhead();

            switch (token.Kind)
            {
                case TokenKind.Eof: return null;
                case TokenKind.Identifier: return ParseStatement(Consume());
                case TokenKind.ReturnKeyword: return ParseReturn(Consume());
                case TokenKind.ExitKeyword: return ParseExit(Consume());
                case TokenKind.GotoKeyword: return ParseGoto(Consume());
                case TokenKind.WaitKeyword: return ParseWait(Consume());
                case TokenKind.OnKeyword: return ParseOn(Consume());
                case TokenKind.IfKeyword: return ParseIf(Consume());
                case TokenKind.SelectKeyword: return ParseSelect(Consume());
                case TokenKind.LoopKeyword: return ParseLoop(Consume());
                case TokenKind.WhileKeyword: return ParseWhileLoop(Consume());
                case TokenKind.ForKeyword: return ParseForLoop(Consume());
                case TokenKind.SymbolKeyword: return ParseSymbolDeclaration(Consume());
                case TokenKind.ConstKeyword: return ParseConstantDeclaration(Consume());
                case TokenKind.BoolKeyword:
                case TokenKind.ByteKeyword:
                case TokenKind.WordKeyword:
                case TokenKind.ShortKeyword:
                case TokenKind.IntKeyword:
                case TokenKind.FloatKeyword:
                case TokenKind.CharKeyword:
                    return ParseVariableDeclaration(Consume());
            }

            NoteError(ErrorMessages.StatementTrigger, token);
            ConsumeUntil(TokenKind.Linebreak);
            return null;
        }

        /// <summary>
        /// Got an identifier, now parse a statement.
        /// </summary>
        /// <returns>A statement or null.</returns>
        private Statement? ParseStatement(Token identifier)
        {
            switch (LookAhead().Kind)
            {
                case TokenKind.ColonSymbol:
                    return ParseLabel(identifier, Consume());
                case TokenKind.ParenthesisLeft:
                    return ParseFunctionCall(identifier, Consume());
                case TokenKind.EqualSymbol:
                    return ParseAssignment(identifier, Consume());
            }

            return ParseCommandCall(identifier);
        }

        /// <summary>
        /// Parse a list of statements.
        /// </summary>
        /// <param name="statements">The list of statements to add to.</param>
        /// <param name="endKind1">The first kind of token that signals the end of the statement list.</param>
        /// <param name="endKind2">The second kind of token that signals the end of the statement list.</param>
        /// <param name="endKind3">The third kind of token that signals the end of the statement list.</param>
        private void ParseStatements(
            List<Statement> statements,
            TokenKind endKind1,
            TokenKind endKind2 = TokenKind.Eof,
            TokenKind endKind3 = TokenKind.Eof)
        {
            while (true)
            {
                var token = LookAhead();
                switch (token.Kind)
                {
                    case TokenKind.Eof:
                        break;
                    case TokenKind.Linebreak:
                        if (endKind1 == TokenKind.Linebreak) break;
                        Consume();
                        continue;
                    case TokenKind.ColonSymbol:
                        Consume();
                        // When the statement list ends with a linebreak,
                        // multiple statements on one line separated by colon become possible.
                        if (endKind1 == TokenKind.Linebreak) continue;

                        NoteError(ErrorMessages.StatementEndColon);
                        break;
                    default:
                        if (endKind1 == token.Kind) break;
                        if (endKind2 == token.Kind) break;
                        if (endKind3 == token.Kind) break;

                        statements.AddIfNonNull(ParseStatement());
                        continue;
                }
                break;
            }
        }

        /// <summary>
        /// Parse a list of statements.
        /// </summary>
        /// <param name="endKind1">The first kind of token that signals the end of the statement list.</param>
        /// <param name="endKind2">The second kind of token that signals the end of the statement list.</param>
        /// <param name="endKind3">The third kind of token that signals the end of the statement list.</param>
        /// <returns>The list of statements.</returns>
        private List<Statement> ParseStatements(
            TokenKind endKind1,
            TokenKind endKind2 = TokenKind.Eof,
            TokenKind endKind3 = TokenKind.Eof)
        {
            var statements = new List<Statement>();
            ParseStatements(statements, endKind1, endKind2, endKind3);
            return statements;
        }

        /// <summary>
        /// Parse a simple loop.
        /// </summary>
        /// <returns>A simple loop or else null.</returns>
        private Loop? ParseLoop(Token loopToken)
        {
            TryConsume(TokenKind.Linebreak);
            var comment = ConsumeComment();
            var statements = ParseStatements(TokenKind.EndLoopKeyword);

            if (TryConsume(TokenKind.EndLoopKeyword, out var endLoopToken, ErrorMessages.EndLoop))
            {
                return new Loop(comment, loopToken, endLoopToken, statements);
            }

            return null;
        }

        /// <summary>
        /// Parse a while loop.
        /// </summary>
        /// <returns>A while loop or else null.</returns>
        private WhileLoop? ParseWhileLoop(Token whileToken)
        {
            var expression = ParseExpression();
            TryConsume(TokenKind.Linebreak);
            var comment = ConsumeComment();
            var statements = ParseStatements(TokenKind.EndWhileKeyword);

            if (TryConsume(TokenKind.EndWhileKeyword, out var endWhileToken, ErrorMessages.EndWhile))
            {
                return new WhileLoop(comment, whileToken, endWhileToken, expression, statements);
            }

            return null;
        }

        /// <summary>
        /// Parse a for loop.
        /// </summary>
        /// <returns>A for loop or else null.</returns>
        private ForLoop? ParseForLoop(Token forToken)
        {
            var comment = ConsumeComment();

            if (TryConsume(TokenKind.Identifier, out var identifierToken, ErrorMessages.AssignmentStart))
            {
                if (TryConsume(TokenKind.EqualSymbol, out var equalToken, ErrorMessages.AssignmentSymbol))
                {
                    var assignment = ParseAssignment(identifierToken, equalToken);
                    Expression? stepExpression = null;

                    if (TryConsume(TokenKind.ToKeyword, ErrorMessages.ForTo))
                    {
                        var targetExpression = ParseExpression();
                        if (TryConsume(TokenKind.StepKeyword))
                        {
                            stepExpression = ParseExpression();
                        }
                        TryConsume(TokenKind.Linebreak);

                        var statements = ParseStatements(TokenKind.EndForKeyword);
                        if (TryConsume(TokenKind.EndForKeyword, out var endForToken, ErrorMessages.EndFor))
                        {
                            TryConsume(TokenKind.Linebreak);
                            return new ForLoop(
                                comment, forToken, endForToken,
                                assignment, targetExpression, stepExpression, statements);
                        }
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Parse an if statement.
        /// </summary>
        /// <returns>An if statement or else null.</returns>
        private IfStatement? ParseIf(Token ifToken)
        {
            Statement? statement = null;
            var ifBranches = new List<IfBranch>();

            var expression = ParseExpression();
            var token = LookAhead();

            switch (token.Kind)
            {
                case TokenKind.ExitKeyword:
                    statement = ParseExit(Consume());
                    return new IfStatement(ifToken, new IfBranch(ifToken, expression, statement));
                case TokenKind.ReturnKeyword:
                    statement = ParseReturn(Consume());
                    return new IfStatement(ifToken, new IfBranch(ifToken, expression, statement));
                case TokenKind.GotoKeyword:
                    statement = ParseGoto(Consume());
                    if (statement == null)
                    {
                        return new IfStatement(ifToken, new IfBranch(ifToken, expression));
                    }
                    return new IfStatement(ifToken, new IfBranch(ifToken, expression, statement));
                case TokenKind.ThenKeyword:
                    Consume();
                    if (!TryConsume(TokenKind.Linebreak))
                    {
                        // We try to get multiple statements on this line.
                        ifBranches.AddIfNonNull(
                            new IfBranch(token, expression, ParseStatements(TokenKind.Linebreak)));
                        TryConsume(TokenKind.Linebreak, out token);
                        return new IfStatement(ifToken, token, ifBranches, null);
                    }

                    ifBranches.AddIfNonNull(
                        new IfBranch(token, expression,
                                ParseStatements(
                                    TokenKind.ElseIfKeyword,
                                    TokenKind.ElseKeyword,
                                    TokenKind.EndIfKeyword)));
                    break;
                default:
                    ConsumeUntilIncluding(TokenKind.Linebreak);
                    NoteError(ErrorMessages.IfSpecial);
                    return null;
            }

            // Multiline or else Singleline?
            if (statement == null)
            {
                Block? elseBranch = null;
                Token? endIfToken;

                while (true)
                {
                    token = Consume();
                    switch (token.Kind)
                    {
                        case TokenKind.ElseIfKeyword:
                            expression = ParseExpression();
                            if (TryConsume(TokenKind.ThenKeyword, out var thenToken))
                            {
                                TryConsume(TokenKind.Linebreak);
                                ifBranches.AddIfNonNull(
                                    new IfBranch(token, thenToken, expression,
                                            ParseStatements(
                                                TokenKind.ElseIfKeyword,
                                                TokenKind.ElseKeyword,
                                                TokenKind.EndIfKeyword)));
                            }
                            continue;
                        case TokenKind.ElseKeyword:
                            TryConsume(TokenKind.Linebreak);
                            elseBranch = new(token, ParseStatements(
                                                        TokenKind.ElseIfKeyword,
                                                        TokenKind.ElseKeyword,
                                                        TokenKind.EndIfKeyword));
                            TryConsume(TokenKind.EndIfKeyword, out endIfToken);
                            break;
                        case TokenKind.EndIfKeyword:
                            endIfToken = token;
                            break;
                        default:
                            ConsumeUntilIncluding(TokenKind.Linebreak);
                            return null;
                    }
                    break;
                }

                TryConsume(TokenKind.Linebreak);
                return new IfStatement(ifToken, endIfToken, ifBranches, elseBranch);
            }

            TryConsume(TokenKind.Linebreak);
            return new IfStatement(ifToken, new IfBranch(ifToken, statement, expression, statement));
        }

        /// <summary>
        /// Parse a select statement.
        /// </summary>
        /// <returns>A select statement or else null.</returns>
        private SelectStatement? ParseSelect(Token selectToken)
        {
            if (TryConsume(TokenKind.Identifier, out var identifierToken, ErrorMessages.SelectIdentifier))
            {
                TryConsume(TokenKind.Linebreak);
                var comment = ConsumeComment();
                var cases = new List<SelectDefaultCase>();
                SelectDefaultCase? defaultCase = null;

                while (TryConsume(TokenKind.CaseKeyword, out var caseToken))
                {
                    if (!cases.AddIfNonNull(ParseSelectCase(caseToken))) ConsumeUntilIncluding(TokenKind.Linebreak);
                }

                if (cases.Count > 0)
                {
                    var index = cases.Count - 1;
                    defaultCase = cases[index];
                    cases.RemoveAt(index);
                }

                TryConsume(TokenKind.EndSelectKeyword, out var endSelectToken);
                TryConsume(TokenKind.Linebreak);

                return new SelectStatement(comment, selectToken, endSelectToken!, identifierToken, cases, defaultCase);
            }

            return null;
        }

        private SelectDefaultCase ParseSelectCase(Token caseToken)
        {
            var statements = new List<Statement>();

            if (TryConsume(TokenKind.ElseKeyword))
            {
                TryConsume(TokenKind.Linebreak);
                ParseStatements(statements, TokenKind.CaseKeyword, TokenKind.EndSelectKeyword);
                return new SelectDefaultCase(caseToken, statements);
            }

            var firstExpression = ParseExpression();

            if (TryConsume(TokenKind.CommaSymbol))
            {
                var expressions = new List<Expression>();

                if (expressions.AddIfNonNull(firstExpression))
                {
                    do expressions.AddIfNonNull(ParseExpression()); while (TryConsume(TokenKind.CommaSymbol));
                }
                else ConsumeUntilIncluding(TokenKind.Linebreak);

                TryConsume(TokenKind.Linebreak);
                ParseStatements(statements, TokenKind.CaseKeyword, TokenKind.EndSelectKeyword);
                return new SelectListCase(caseToken, expressions, statements);
            }
            else if (TryConsume(TokenKind.ToKeyword))
            {
                var lastExpression = ParseExpression();

                TryConsume(TokenKind.Linebreak);
                ParseStatements(statements, TokenKind.CaseKeyword, TokenKind.EndSelectKeyword);
                return new SelectRangeCase(caseToken, firstExpression, lastExpression, statements);
            }

            TryConsume(TokenKind.Linebreak);
            ParseStatements(statements, TokenKind.CaseKeyword, TokenKind.EndSelectKeyword);
            return new SelectSingleCase(caseToken, firstExpression, statements);
        }

        /// <summary>
        /// Parse a builtin statement.
        /// </summary>
        /// <returns>A builtin statement or else null.</returns>
        private CommandCall ParseCommandCall(Token identifierToken)
        {
            var args = new List<Expression>();

            do if (!args.AddIfNonNull(ParseExpression()))
                {
                    NoteError(ErrorMessages.ExpressionStart, LookAhead());
                }
            while (TryConsume(TokenKind.CommaSymbol));

            return new CommandCall(ConsumeComment(), identifierToken, args);
        }

        /// <summary>
        /// Parse a function call statement.
        /// </summary>
        /// <returns>A function call statement or else null.</returns>
        private FunctionCall? ParseFunctionCall(Token identifierToken, Token parethesisRightToken)
        {
            var identifier = _identifier.Parse(identifierToken);
            var call = _call.ParseFunctionCall(identifier, parethesisRightToken);
            LookAhead(0); // Make sure we get a possible comment line after the call, but on the same line
            if (call != null) return new FunctionCall(ConsumeComment(), call!);
            return null;
        }

        /// <summary>
        /// Parse a label statement.
        /// </summary>
        /// <returns>A label statement or else null.</returns>
        private LabelStatement ParseLabel(Token labelIdentifier, Token colonToken)
            => new LabelStatement(ConsumeComment(), labelIdentifier, colonToken);

        /// <summary>
        /// Parse a goto statement.
        /// </summary>
        /// <returns>A goto statement or else null.</returns>
        private GotoStatement? ParseGoto(Token gotoToken)
        {
            if (TryConsume(TokenKind.Identifier, out var identifierToken, ErrorMessages.GotoLabel))
            {
                return new GotoStatement(ConsumeComment(), gotoToken, identifierToken);
            }

            return null;
        }

        /// <summary>
        /// Parse a wait statement.
        /// </summary>
        /// <returns>A wait statement or else null.</returns>
        private WaitStatement? ParseWait(Token waitToken)
        {
            if (ParseExpression(out var condition))
            {
                IntegerLiteral? integerLiteral = null;
                JumpStatement? jumpStatement = null;

                if (TryConsume(TokenKind.CommaSymbol) &&
                    TryConsume(TokenKind.IntegerLiteral, out var integerToken, ErrorMessages.Identifier))
                {
                    integerLiteral = new(integerToken);

                    if (TryConsume(TokenKind.CommaSymbol))
                    {
                        switch (LookAhead().Kind)
                        {
                            case TokenKind.ExitKeyword: jumpStatement = ParseExit(Consume()); break;
                            case TokenKind.ReturnKeyword: jumpStatement = ParseReturn(Consume()); break;
                            case TokenKind.Identifier: jumpStatement = ParseGoto(LookAhead()); break;
                            default: NoteError(ErrorMessages.WaitJump); break;
                        }
                    }
                }
                ConsumeUntil(TokenKind.Linebreak, TokenKind.Eof, ErrorMessages.Linebreak);
                return new WaitStatement(ConsumeComment(), waitToken, condition, integerLiteral, jumpStatement);
            }

            return null;
        }

        /// <summary>
        /// Parse a on goto/call statement.
        /// </summary>
        /// <returns>A on goto/call statement or else null.</returns>
        private OnStatement? ParseOn(Token onToken)
        {
            if (TryConsume(TokenKind.Identifier, out var indexIdentifierToken, ErrorMessages.Identifier))
            {
                var commandToken = LookAhead();

                if (commandToken.Kind == TokenKind.GotoKeyword || commandToken.Kind == TokenKind.CallKeyword)
                {
                    Consume();
                    var identifiers = new List<Token>();

                    do
                    {
                        var token = LookAhead();
                        if (token.Kind == TokenKind.Identifier) identifiers.AddIfNonNull(Consume());
                        else
                        {
                            NoteError(ErrorMessages.Identifier, LookAhead());
                            if (token.Kind == TokenKind.Linebreak || token.Kind == TokenKind.Eof) break;
                        }
                    }
                    while (TryConsume(TokenKind.CommaSymbol));

                    if (commandToken.Kind == TokenKind.GotoKeyword)
                    {
                        return new OnGotoStatement(
                            ConsumeComment(), onToken, indexIdentifierToken, commandToken, identifiers);
                    }
                    else if (commandToken.Kind == TokenKind.CallKeyword)
                    {
                        return new OnFunctionCall(
                            ConsumeComment(), onToken, indexIdentifierToken, commandToken, identifiers);
                    }
                }
            }

            NoteError(ErrorMessages.OnGotoCall);
            return null;
        }

        /// <summary>
        /// Parse an exit statement.
        /// </summary>
        /// <returns>An exit statement or else null.</returns>
        private ExitStatement ParseExit(Token exitToken)
        {
            LookAhead(); // We must make sure the comment one this line after thge command will be available.
            return new ExitStatement(ConsumeComment(), exitToken);
        }

        /// <summary>
        /// Parse a return statement.
        /// </summary>
        /// <returns>A return statement or else null.</returns>
        private ReturnStatement ParseReturn(Token returnToken)
        {
            var expression = LookAhead().Kind != TokenKind.Linebreak ? ParseExpression() : null;

            return new ReturnStatement(ConsumeComment(), returnToken, expression);
        }

        /// <summary>
        /// Parse an assignment statement.
        /// </summary>
        /// <returns>An assignment statement or else null.</returns>
        private AssignmentStatement ParseAssignment(Token identifierToken, Token equalToken)
        {
            var expression = ParseExpression();

            return new AssignmentStatement(ConsumeComment(), identifierToken, equalToken, expression);
        }

        /// <summary>
        /// Parse a symbol declaration.
        /// </summary>
        /// <returns>A symbol declaration or else null.</returns>
        private SymbolDeclaration? ParseSymbolDeclaration(Token symbolToken)
        {
            if (TryConsume(TokenKind.Identifier, out var nameIdentifier, ErrorMessages.Identifier))
            {
                TryConsume(TokenKind.EqualSymbol, out var equalToken, ErrorMessages.AssignmentSymbol);
                if (TryConsume(TokenKind.Identifier, out var contentIdentifier, ErrorMessages.Identifier))
                {
                    var identifierExpression = _identifier.ParseIdentifier(contentIdentifier);
                    return new SymbolDeclaration(ConsumeComment(),
                                    symbolToken, nameIdentifier, equalToken, identifierExpression);
                }
            }
            return null;
        }

        /// <summary>
        /// Parse a constant declaration.
        /// </summary>
        /// <returns>A constant declaration or else null.</returns>
        private ConstantDeclaration? ParseConstantDeclaration(Token constToken)
        {
            if (TryConsume(TokenKind.Identifier, out var nameIdentifier, ErrorMessages.AssignmentStart))
            {
                if (TryConsume(TokenKind.EqualSymbol, out var equalToken, ErrorMessages.AssignmentSymbol))
                {
                    var expression = ParseExpression();

                    if (expression is Literal)
                    {
                        return new ConstantDeclaration(ConsumeComment(), constToken, nameIdentifier, equalToken, expression as Literal);
                    }
                    NoteError(expression == null ? ErrorMessages.ConstantNonNull : ErrorMessages.ConstantLiteral);
                }
            }

            ConsumeUntil(TokenKind.Linebreak);
            return null;
        }

        /// <summary>
        /// Parse a variable declaration.
        /// </summary>
        /// <returns>A variable declaration or else null.</returns>
        private VariableDeclaration? ParseVariableDeclaration(Token dataTypeToken)
        {
            var dataType = DataType.CreateNonVoid(dataTypeToken) ?? DataType.CreateVoid(dataTypeToken);
            var subDeclarations = new List<VariableSubDeclaration>();

            while (true)
            {
                subDeclarations.AddIfNonNull(ParseVariableSubDeclaration());

                var token = LookAhead();

                switch (token.Kind)
                {
                    case TokenKind.CommaSymbol:
                        Consume();
                        continue;
                    case TokenKind.Linebreak:
                        break;
                    default:
                        NoteError(ErrorMessages.VariableDeclaration, token);
                        break;
                }
                break;
            }

            if (subDeclarations.Count > 0)
            {
                return new VariableDeclaration(ConsumeComment(), dataType, subDeclarations);
            }

            ConsumeUntil(TokenKind.Linebreak);
            return null;
        }

        private VariableSubDeclaration? ParseVariableSubDeclaration()
        {
            if (TryConsume(TokenKind.Identifier, out var nameIdentifier))
            {
                int elementCount = -1;

                if (TryConsume(TokenKind.BracketLeft))
                {
                    if (TryConsume(TokenKind.IntegerLiteral, out var arraySizeToken,
                            ErrorMessages.VariableDeclarationArraySize))
                    {
                        elementCount = arraySizeToken.Lexeme.ParseInt32();
                    }
                    TryConsume(TokenKind.BracketRight);
                }
                var expression = TryConsume(TokenKind.EqualSymbol) ? ParseExpression() : null;

                if (elementCount > -1)
                {
                    return new VariableArraySubDeclaration(nameIdentifier, elementCount, nameIdentifier, expression);
                }
                return new VariableSubDeclaration(nameIdentifier, expression, nameIdentifier);
            }
            return null;
        }
    }
}
