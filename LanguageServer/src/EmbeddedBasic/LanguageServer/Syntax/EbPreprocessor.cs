using System.Collections;
using System.Collections.Generic;
using EmbeddedBasic.LanguageServer.Syntax.Nodes;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions.Literals;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Directives;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Parselets;

namespace EmbeddedBasic.LanguageServer.Syntax
{
    /// <summary>
    /// The preprocessor for `Embedded BASIC`.
    /// </summary>
    public sealed class EbPreprocessor : Parser, ITokenProvider
    {
        private readonly IdentifierParselet _identifier;

        public SourceFile CurrentSourceFile => TokenProvider.CurrentSourceFile;

        /// <summary>
        /// The number of currently active nestings.
        /// </summary>
        private int _activeNestings = 0;

        object IEnumerator.Current => Current;

        public Token Current => _current!;

        private Token? _current = null;

        /// <summary>
        /// Create a preprocessor for `Embedded BASIC`.
        /// </summary>
        public EbPreprocessor() : base(new EbLexer())
        {
            _identifier = new(this);
        }

        public void Dispose() => TokenProvider.Dispose();

        public void Reset() => TokenProvider.Reset();

        public bool Reset(SourceFile sourceFile) => TokenProvider.Reset(sourceFile);

        public bool IncludeSource(SourceFile sourceFile) => TokenProvider.IncludeSource(sourceFile);

        public bool MoveNext()
        {
            Token token;

            while ((token = Consume()).Kind != TokenKind.Eof)
            {
                switch (token.Kind)
                {
                    case TokenKind.Identifier: if (TryParseMacroUsage(token)) continue; else break;
                    case TokenKind.RegionPPKeyword: continue;
                    case TokenKind.EndRegionPPKeyword: continue;
                    case TokenKind.ImportPPKeyword: ParseImport(token); continue;
                    case TokenKind.IncludePPKeyword: ParseInclude(token); continue;
                    case TokenKind.DefinePPKeyword: ParseDefine(token); continue;
                    case TokenKind.UndefinePPKeyword: ParseUndefine(token); continue;
                    case TokenKind.IfdefPPKeyword: ParseIfdef(token); continue;
                    case TokenKind.IfndefPPKeyword: ParseIfdef(token, negate: true); continue;
                    case TokenKind.ElsePPKeyword: ParseElse(token, branchMayNotPassThrough: true); continue;
                    case TokenKind.EndifPPKeyword: ParseEndif(token); continue;
                }
                break;
            }

            _current = token;
            return token.Kind != TokenKind.Eof;
        }

        /// <summary>
        /// Try to parse a macro usage.
        /// </summary>
        /// <param name="identifierToken">The identifier token of the possible macro name.</param>
        private bool TryParseMacroUsage(Token identifierToken)
        {
            if (SyntaxTree.TryGetSymbol<DefineDirective>(identifierToken.Lexeme, out var define))
            {
                // Parse the comma-separated arguments until we hit ")".
                var argsTokens = new List<List<Token>>();
                var lastToken = identifierToken;

                // Gather the arguments.
                if (TryConsume(TokenKind.ParenthesisLeft))
                {
                    var tokens = new List<Token>();
                    while (true)
                    {
                        var token = Consume();

                        switch (token.Kind)
                        {
                            case TokenKind.Identifier: if (!TryParseMacroUsage(token)) tokens.Add(token); continue;
                            case TokenKind.CommaSymbol: argsTokens.Add(tokens); tokens = new(); continue;
                            case TokenKind.ParenthesisRight: lastToken = token; break;
                            default: tokens.Add(token); continue;
                        }
                        break;
                    }
                    argsTokens.Add(tokens);
                }

                if (argsTokens.Count < define.ParameterIdentifiers.Count)
                {
                    NoteError(ErrorMessages.MacroUsageArgsTooFew, identifierToken);
                }
                else if (argsTokens.Count > define.ParameterIdentifiers.Count)
                {
                    NoteError(ErrorMessages.MacroUsageArgsTooMany, identifierToken);
                }

                foreach (var contentToken in define.ContentTokens)
                {
                    if (contentToken.Kind == TokenKind.Identifier &&
                        define.TryGetParameterIndex(contentToken, out var parameterIndex) &&
                        parameterIndex < argsTokens.Count)
                    {
                        var argTokens = argsTokens[parameterIndex];
                        foreach (var argToken in argTokens) InsertToken(argToken);
                    }
                    else InsertToken(contentToken);
                }

                return true;
            }
            return false;
        }

        /// <summary>
        /// Parse an import directive.
        /// </summary>
        /// <returns>An import directive or else null.</returns>
        private void ParseImport(Token import)
        {
            if (TryConsume(TokenKind.Identifier, out var identifierToken))
            {
                var identifier = _identifier.ParseIdentifier(identifierToken);

                if (TryConsume(TokenKind.FromPPKeyword, out var fromToken))
                {
                    if (TryConsume(TokenKind.StringLiteral, out var stringToken, ErrorMessages.ImportFrom))
                    {
                        TryConsume(TokenKind.Linebreak);
                        var importDirective = new ImportDirective(
                            ConsumeComment(), import, identifier, new StringLiteral(stringToken));
                        TokenProvider.IncludeSource(importDirective.ToSourceLocation(TokenProvider.CurrentSourceFile));
                    }
                }
                else
                {
                    TryConsume(TokenKind.Linebreak);
                    var importDirective = new ImportDirective(ConsumeComment(), import, identifier);
                    TokenProvider.IncludeSource(importDirective.ToSourceLocation(TokenProvider.CurrentSourceFile));
                }
            }
        }

        private void ParseInclude(Token includeToken)
        {
            if (TryConsume(TokenKind.StringLiteral, out var stringToken, ErrorMessages.IncludePath))
            {
                var includeDirective = new IncludeDirective(ConsumeComment(), includeToken, new StringLiteral(stringToken));
                TokenProvider.IncludeSource(includeDirective.ToSourceFile(TokenProvider.CurrentSourceFile));
            }
        }

        private void ParseDefine(Token defineToken)
        {
            var comment = ConsumeComment();
            if (TryConsume(TokenKind.Identifier, out var macroIdentifier, ErrorMessages.DefineName))
            {

                var parameters = new List<Token>();
                var contents = new List<Token>();

                if (TryConsume(TokenKind.ParenthesisLeft))
                {
                    do
                    {
                        if (TryConsume(TokenKind.Identifier, out var parameterIdentifier, ErrorMessages.DefineParameterName))
                        {
                            parameters.Add(parameterIdentifier);
                        }
                    } while (TryConsume(TokenKind.CommaSymbol));

                    TryConsume(TokenKind.ParenthesisRight, ErrorMessages.DefineClosingParenthesis);
                }

                while (TryConsumeAnyBut(TokenKind.Linebreak, out var token)) contents.Add(token);

                TryConsume(TokenKind.Linebreak);
                SyntaxTree.AddSymbol(new DefineDirective(comment, defineToken, macroIdentifier, parameters, contents));
            }
        }

        private void ParseUndefine(Token undefineToken)
        {
            if (TryConsume(TokenKind.Identifier, out var identifier, ErrorMessages.UndefineName))
            {
                TryConsume(TokenKind.Linebreak);
                SyntaxTree.RemoveSymbol(identifier.Lexeme);
            }
        }

        private void ParseIfdef(Token token, bool negate = false)
        {
            if (TryConsume(TokenKind.Identifier, out var identifier, ErrorMessages.PPIfDef))
            {
                TryConsume(TokenKind.Linebreak);
                _activeNestings++;

                if (SyntaxTree.SymbolExists(identifier.Lexeme) == negate)
                {
                    // This branch may not pass through.
                    for (int inactiveNestings = 0; ;)
                    {
                        switch ((token = Consume()).Kind)
                        {
                            case TokenKind.IfdefPPKeyword:
                            case TokenKind.IfndefPPKeyword:
                                inactiveNestings++;
                                continue;
                            case TokenKind.ElsePPKeyword:
                                if (inactiveNestings > 0) continue;
                                else ParseElse(token, branchMayNotPassThrough: false);
                                break;
                            case TokenKind.EndifPPKeyword:
                                if (inactiveNestings > 0)
                                {
                                    inactiveNestings--;
                                    continue;
                                }
                                else ParseEndif(token);
                                break;
                            default:
                                continue;
                        }
                        break;
                    }
                }
            }
        }

        private void ParseElse(Token token, bool branchMayNotPassThrough)
        {
            TryConsume(TokenKind.Linebreak);

            if (_activeNestings == 0) NoteError(ErrorMessages.PPIfElse);
            if (branchMayNotPassThrough)
            {
                // This branch may not pass through.
                for (int inactiveNestings = 0; ;)
                {
                    switch ((token = Consume()).Kind)
                    {
                        case TokenKind.IfdefPPKeyword:
                        case TokenKind.IfndefPPKeyword:
                            inactiveNestings++;
                            continue;
                        case TokenKind.ElsePPKeyword:
                            if (inactiveNestings > 0) continue;
                            else NoteError(ErrorMessages.PPIfElse);
                            continue;
                        case TokenKind.EndifPPKeyword:
                            if (inactiveNestings > 0)
                            {
                                inactiveNestings--;
                                continue;
                            }
                            else ParseEndif(token);
                            break;
                        default:
                            continue;
                    }
                    break;
                }
            }
        }

        private void ParseEndif(Token token)
        {
            TryConsume(TokenKind.Linebreak);

            if (_activeNestings > 0) _activeNestings--;
            else NoteError(ErrorMessages.PPIfEnd);
        }
    }
}
