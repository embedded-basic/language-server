using EmbeddedBasic.LanguageServer.Syntax.Nodes;

namespace EmbeddedBasic.LanguageServer.Syntax
{
    /// <summary>
    /// The lexer for `Embedded BASIC`.
    /// </summary>
    public sealed class EbLexer : Lexer
    {
        /// <summary>
        /// Create a lexer for `Embedded BASIC`.
        /// </summary>
        public EbLexer()
        {
            AddKeyword("#include", TokenKind.IncludePPKeyword);
            AddKeyword("#define", TokenKind.DefinePPKeyword);
            AddKeyword("#undef", TokenKind.UndefinePPKeyword);
            AddKeyword("#ifdef", TokenKind.IfdefPPKeyword);
            AddKeyword("#ifndef", TokenKind.IfndefPPKeyword);
            AddKeyword("#else", TokenKind.ElsePPKeyword);
            AddKeyword("#endif", TokenKind.EndifPPKeyword);
            AddKeyword("#target", TokenKind.TargetPPKeyword);
            AddKeyword("#region", TokenKind.RegionPPKeyword);
            AddKeyword("#endregion", TokenKind.EndRegionPPKeyword);
            AddKeyword("import", TokenKind.ImportPPKeyword);
            AddKeyword("from", TokenKind.FromPPKeyword);

            AddKeyword("not", TokenKind.NotKeyword);
            AddKeyword("neg", TokenKind.NegKeyword);
            AddKeyword("mod", TokenKind.ModKeyword);
            AddKeyword("and", TokenKind.AndKeyword);
            AddKeyword("or", TokenKind.OrKeyword);
            AddKeyword("xor", TokenKind.XorKeyword);
            AddKeyword("true", TokenKind.TrueKeyword);
            AddKeyword("false", TokenKind.FalseKeyword);
            AddKeyword("high", TokenKind.HighKeyword);
            AddKeyword("low", TokenKind.LowKeyword);
            AddKeyword("bool", TokenKind.BoolKeyword);
            AddKeyword("char", TokenKind.CharKeyword);
            AddKeyword("byte", TokenKind.ByteKeyword);
            AddKeyword("word", TokenKind.WordKeyword);
            AddKeyword("short", TokenKind.ShortKeyword);
            AddKeyword("int", TokenKind.IntKeyword);
            AddKeyword("float", TokenKind.FloatKeyword);
            AddKeyword("symbol", TokenKind.SymbolKeyword);
            AddKeyword("const", TokenKind.ConstKeyword);
            AddKeyword("return", TokenKind.ReturnKeyword);
            AddKeyword("exit", TokenKind.ExitKeyword);
            AddKeyword("on", TokenKind.OnKeyword);
            AddKeyword("call", TokenKind.CallKeyword);
            AddKeyword("goto", TokenKind.GotoKeyword);
            AddKeyword("wait", TokenKind.WaitKeyword);
            AddKeyword("if", TokenKind.IfKeyword);
            AddKeyword("then", TokenKind.ThenKeyword);
            AddKeyword("elseif", TokenKind.ElseIfKeyword);
            AddKeyword("else", TokenKind.ElseKeyword);
            AddKeyword("endif", TokenKind.EndIfKeyword);
            AddKeyword("select", TokenKind.SelectKeyword);
            AddKeyword("case", TokenKind.CaseKeyword);
            AddKeyword("endselect", TokenKind.EndSelectKeyword);
            AddKeyword("loop", TokenKind.LoopKeyword);
            AddKeyword("endloop", TokenKind.EndLoopKeyword);
            AddKeyword("for", TokenKind.ForKeyword);
            AddKeyword("to", TokenKind.ToKeyword);
            AddKeyword("step", TokenKind.StepKeyword);
            AddKeyword("endfor", TokenKind.EndForKeyword);
            AddKeyword("while", TokenKind.WhileKeyword);
            AddKeyword("endwhile", TokenKind.EndWhileKeyword);
            AddKeyword("function", TokenKind.FunctionKeyword);
            AddKeyword("endfunction", TokenKind.EndFunctionKeyword);
        }

        /// <summary>
        /// Move to the next token.
        /// </summary>
        /// <returns>Could a token be found?</returns>
        public override bool MoveNext()
        {
            if (!IsEndOfFile())
            {
                int count;
                int offset;
                string lexeme;

                while (PeekNextChar(out char ch))
                {
                    // Linebreak -------------------------------------------- //
                    if (TrySetLinebreak(ch)) return true;
                    // Escaped-Linebreak ------------------------------------ //
                    else if (ch == '\\' && PeekNextChar(out ch))
                    {
                        Discard();
                        // Discard a linebreak or else any other char.
                        if (!TrySetLinebreak(ch, allowLinebreak: false)) Discard();
                    }
                    // Whitespace ------------------------------------------- //
                    else if (char.IsWhiteSpace(ch)) Discard();
                    // Dot -------------------------------------------------- //
                    else if (ch == '.') { Set(TokenKind.DotSymbol); return true; }
                    // Colon ------------------------------------------------ //
                    else if (ch == ':') { Set(TokenKind.ColonSymbol); return true; }
                    // Comma ------------------------------------------------ //
                    else if (ch == ',') { Set(TokenKind.CommaSymbol); return true; }
                    // Parenthesis ------------------------------------------ //
                    else if (ch == '(') { Set(TokenKind.ParenthesisLeft); return true; }
                    else if (ch == ')') { Set(TokenKind.ParenthesisRight); return true; }
                    // Brackets --------------------------------------------- //
                    else if (ch == '[') { Set(TokenKind.BracketLeft); return true; }
                    else if (ch == ']') { Set(TokenKind.BracketRight); return true; }
                    // Caret ------------------------------------------------ //
                    else if (ch == '^') { Set(TokenKind.CaretSymbol); return true; }
                    // Plus ------------------------------------------------- //
                    else if (ch == '+') { Set(TokenKind.PlusSymbol); return true; }
                    // Minus ------------------------------------------------ //
                    else if (ch == '-') { Set(TokenKind.MinusSymbol); return true; }
                    // Asterisk --------------------------------------------- //
                    else if (ch == '*') { Set(TokenKind.AsteriskSymbol); return true; }
                    // Slash/Comment ---------------------------------------- //
                    else if (ch == '/')
                    {
                        if (PeekNextChar(out ch, 1) && ch == '/')
                        {
                            offset = 1;
                            while (PeekNextChar(out ch, ++offset) && !(ch == '\r' || ch == '\n')) { }

                            AppendComment(offset);
                        }
                        else
                        {
                            Set(TokenKind.SlashSymbol);
                            return true;
                        }
                    }
                    // Less-Than* ------------------------------------------- //
                    else if (ch == '<')
                    {
                        if (PeekNextChar(out ch, 1))
                        {
                            if (ch == '<') Set(TokenKind.LessThanLessThanSymbol, 1);
                            else if (ch == '=') Set(TokenKind.LessThanOrEqualSymbol, 1);
                            else if (ch == '>') Set(TokenKind.LessThanGreaterThanSymbol, 1);
                            else Set(TokenKind.LessThanSymbol);
                        }
                        else Set(TokenKind.LessThanSymbol);

                        return true;
                    }
                    // Greater-Than* ---------------------------------------- //
                    else if (ch == '>')
                    {
                        if (PeekNextChar(out ch, 1))
                        {
                            if (ch == '>') Set(TokenKind.GreaterThanGreaterThanSymbol, 1);
                            else if (ch == '=') Set(TokenKind.GreaterThanOrEqualSymbol, 1);
                            else Set(TokenKind.GreaterThanSymbol);
                        }
                        else Set(TokenKind.GreaterThanSymbol);

                        return true;
                    }
                    // Equal* ----------------------------------------------- //
                    else if (ch == '=')
                    {
                        if (PeekNextChar(out ch, 1) && ch == '=') Set(TokenKind.EqualEqualSymbol, 1);
                        else Set(TokenKind.EqualSymbol);

                        return true;
                    }
                    // Bin-Number ------------------------------------------- //
                    else if (ch == '%')
                    {
                        offset = 0;

                        while (PeekNextChar(out ch, ++offset) && (ch == '0' || ch == '1')) { }

                        Set(TokenKind.BinNumberLiteral, offset);
                        return true;
                    }
                    // Hex-Number ------------------------------------------- //
                    else if (ch == '$')
                    {
                        offset = 0;

                        while (PeekNextChar(out ch, ++offset))
                        {
                            switch (ch)
                            {
                                case 'a':
                                case 'b':
                                case 'c':
                                case 'd':
                                case 'e':
                                case 'f':
                                case 'A':
                                case 'B':
                                case 'C':
                                case 'D':
                                case 'E':
                                case 'F':
                                    continue;
                            }
                            if (char.IsDigit(ch)) continue;
                            break;
                        }

                        Set(TokenKind.HexNumberLiteral, offset);
                        return true;
                    }
                    // Integer/Decimal/Date/Time ---------------------------- //
                    else if (char.IsDigit(ch))
                    {
                        offset = 0;
                        string? errorMessage = null;

                        while (PeekNextChar(out ch, ++offset) && char.IsDigit(ch)) { }

                        if (ch == '.')
                        {
                            // A dot was encountered: decimal | date
                            var firstDotOffset = offset;

                            while (PeekNextChar(out ch, ++offset) && char.IsDigit(ch)) { }

                            // The first dot came after two digits and a second dot was encountered: date
                            if (ch == '.' && firstDotOffset == 2)
                            {
                                if (offset == 5)
                                {
                                    // It still may be a date
                                    while (PeekNextChar(out ch, ++offset) && char.IsDigit(ch)) { }

                                    if (offset == 10)
                                    {
                                        Set(TokenKind.DateLiteral, offset);
                                        return true;
                                    }
                                    else errorMessage = ErrorMessages.DateLiteralYear;
                                }
                                else errorMessage = ErrorMessages.DateLiteralMonth;
                            }
                            else
                            {
                                Set(TokenKind.DecimalLiteral, offset);
                                return true;
                            }
                        }
                        else if (ch == ':')
                        {
                            // A colon was encountered: time.
                            if (offset == 2)
                            {
                                while (PeekNextChar(out ch, ++offset) && char.IsDigit(ch)) { }

                                // The first colon came after two digits and a second colon was encountered
                                if (ch == ':')
                                {
                                    if (offset == 5)
                                    {
                                        while (PeekNextChar(out ch, ++offset) && char.IsDigit(ch)) { }

                                        if (offset == 8)
                                        {
                                            Set(TokenKind.TimeLiteral, offset);
                                            return true;
                                        }
                                        else errorMessage = ErrorMessages.TimeLiteralSeconds;
                                    }
                                    else errorMessage = ErrorMessages.TimeLiteralMinutes;
                                }
                                else errorMessage = ErrorMessages.TimeLiteralSecondSeparator;
                            }
                            else errorMessage = ErrorMessages.TimeLiteralHours;
                        }
                        else
                        {
                            Set(TokenKind.IntegerLiteral, offset);
                            return true;
                        }

                        Discard(offset, errorMessage);
                    }
                    // Character -------------------------------------------- //
                    else if (ch == '\'')
                    {
                        offset = 0;
                        count = 0;

                        while (SeekNextChar(out ch, ++offset) && ch != '\'')
                        {
                            count++;
                            if (ch == '\\') offset++;
                        }

                        Set(TokenKind.CharLiteral, ++offset);

                        if (count == 0) NoteError(Current, ErrorMessages.CharacterLiteralMissing);
                        else if (count > 1) NoteError(Current, ErrorMessages.CharacterLiteralString);

                        return true;
                    }
                    // String ----------------------------------------------- //
                    else if (ch == '\"')
                    {
                        offset = 0;

                        while (SeekNextChar(out ch, ++offset) && ch != '\"') if (ch == '\\') offset++;

                        Set(TokenKind.StringLiteral, ++offset);
                        return true;
                    }
                    // Preprocessor-Directive/Device-Name/-ID --------------- //
                    else if (ch == '#')
                    {
                        offset = 1;

                        if (SeekNextChar(out ch, offset))
                        {
                            if (char.IsDigit(ch))
                            {
                                Set(TokenKind.DeviceIdLiteral, ++offset);
                                return true;
                            }
                            else if (char.IsLetter(ch))
                            {
                                while (PeekNextChar(out ch, ++offset) && char.IsLetterOrDigit(ch)) { }

                                if (TrySetKeyword(offset, out lexeme)) ResetComment();
                                else Set(TokenKind.DeviceNameLiteral, offset, lexeme);
                                return true;
                            }
                        }
                        Discard(offset, ErrorMessages.HashSymbol);
                    }
                    // Keyword/Identifier ----------------------------------- //
                    else if (char.IsLetter(ch) || ch == '_')
                    {
                        offset = 0;

                        while (PeekNextChar(out ch, ++offset) && (char.IsLetterOrDigit(ch) || ch == '_')) { }

                        if (!TrySetKeyword(offset, out lexeme)) Set(TokenKind.Identifier, offset, lexeme);
                        return true;
                    }
                    // Errors ----------------------------------------------- //
                    else
                    {
                        offset = 0;

                        // All characters that do not trigger the beginning of
                        // a lexeme represent an error and must be discarded.
                        while (PeekNextChar(out ch, ++offset) &&
                            !(char.IsWhiteSpace(ch) || char.IsLetter(ch) || char.IsDigit(ch)))
                        {
                            switch (ch)
                            {
                                case '\r':
                                case '\n':
                                case '\\':
                                case '.':
                                case ':':
                                case ',':
                                case '(':
                                case ')':
                                case '[':
                                case ']':
                                case '^':
                                case '+':
                                case '-':
                                case '*':
                                case '/':
                                case '<':
                                case '>':
                                case '=':
                                case '%':
                                case '$':
                                case '\'':
                                case '\"':
                                case '#':
                                case '_':
                                    break;
                                default:
                                    continue;
                            }
                            break;
                        }

                        Discard(offset, ErrorMessages.UnknownLexeme);
                    }
                    // -------------------------------------------------- //
                }
            }

            Set(TokenKind.Eof, 0);
            return false;
        }
    }
}
