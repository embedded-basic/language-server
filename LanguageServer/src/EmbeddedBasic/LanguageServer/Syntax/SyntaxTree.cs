using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using EmbeddedBasic.Util;
using EmbeddedBasic.LanguageServer.Syntax.Nodes;

namespace EmbeddedBasic.LanguageServer.Syntax
{
    /// <summary>
    /// A syntax tree.
    /// </summary>
    public abstract class SyntaxTree
    {
        public SourceFile SourceFile { get; }

        public RootNode Root { get; }

        // private readonly SyntaxNodeSet _allSubNodes = new();

        // public SyntaxNodeSet.ReadOnly AllSubNodes => _allSubNodes.AsReadOnly();

        protected readonly Dictionary<string, IReadOnlyList<ErrorMessage>> _errorMessagesByLocation =
            new Dictionary<string, IReadOnlyList<ErrorMessage>>();

        /// <summary>
        /// The error messages by the path of the files where they occurred in.
        /// </summary>
        public IReadOnlyDictionary<string, IReadOnlyList<ErrorMessage>> ErrorMessagesByLocation
            => _errorMessagesByLocation;

        /// <summary>
        /// All available symbols.
        /// </summary>
        public abstract IEnumerable<SyntaxNode> Symbols { get; }

        protected SyntaxTree(RootNode root)
        {
            SourceFile = root.SourceFile;
            Root = root;
        }

        /// <summary>
        /// Add an error message.
        /// </summary>
        /// <param name="node">The node where the error occurred.</param>
        /// <param name="message">The text of the error message.</param>
        public void AddErrorMessage(SyntaxNode node, string message)
        {
            if (!_errorMessagesByLocation.TryGetValue(node.FilePath, out var errorMessages))
            {
                errorMessages = new List<ErrorMessage>();
                _errorMessagesByLocation[node.FilePath] = errorMessages;
            }
            ((List<ErrorMessage>)errorMessages).Add(new ErrorMessage(node, message));
        }

        /// <summary>
        /// Add an error message.
        /// </summary>
        /// <param name="filePath">The source file path for which the error occurred.</param>
        /// <param name="message">The text of the error message.</param>
        public void AddErrorMessage(string filePath, string message)
        {
            if (!_errorMessagesByLocation.TryGetValue(filePath, out var errorMessages))
            {
                errorMessages = new List<ErrorMessage>();
                _errorMessagesByLocation[filePath] = errorMessages;
            }
            ((List<ErrorMessage>)errorMessages).Add(new ErrorMessage(filePath, message));
        }

        /// <summary>
        /// Add an error message.
        /// </summary>
        /// <param name="position">The position where the error occurred.</param>
        /// <param name="message">The text of the error message.</param>
        /// <param name="args">The arguments for the message.</param>
        public void AddErrorMessageFormat(SyntaxNode node, string message, params object[] args)
            => AddErrorMessage(node, string.Format(message, args));

        public string GetErrorsAsText()
        {
            var output = new StringBuilder();
            if (ErrorMessagesByLocation.Count == 0)
            {
                output.AppendLine(ErrorMessages.None);
            }
            else
            {
                foreach (var location in ErrorMessagesByLocation.Keys)
                {
                    if (ErrorMessagesByLocation[location].Count > 0)
                    {
                        foreach (var errorMessage in ErrorMessagesByLocation[location])
                        {
                            output.AppendLine(errorMessage.ToString());
                        }
                    }
                }
            }
            return output.ToString();
        }

        public void WriteErrorLog(string dirPath, string filename)
        {
            IO.EnsureDirectory(dirPath);
            File.WriteAllText(PathHelper.Join(dirPath, filename), GetErrorsAsText(), Encoding.UTF8);
        }

        /// <summary>
        /// Does a symbol with the given name exist?
        /// </summary>
        /// <param name="name">The name of the symbol to look for.</param>
        /// <returns>Does the symbol exist?</returns>
        public abstract bool SymbolExists(string name);

        /// <summary>
        /// Removes the symbol with the given name, if it exists.
        /// </summary>
        /// <param name="name">The name of the symbol to remove.</param>
        /// <returns>Did the symbol exist?</returns>
        public abstract bool RemoveSymbol(string name);

        /// <summary>
        /// Add a symbol.
        /// </summary>
        /// <param name="node">The node that corresponds to a symbol.</param>
        public abstract void AddSymbol(SyntaxNode node);

        /// <summary>
        /// Try to get a symbol by its name.
        /// </summary>
        /// <param name="name">The name of the symbol to get.</param>
        /// <param name="symbol">The symbol or null.</param>
        /// <returns>Does a symbol with the given name exist?</returns>
        public abstract bool TryGetSymbol<N>(string name, [MaybeNullWhen(false)] out N symbol) where N : SyntaxNode;

        public bool TryGetToken(int absolute, [MaybeNullWhen(false)] out Token token)
            => Root.TryGetToken(absolute, out token);

        public bool TryGetToken(int line, int character, [MaybeNullWhen(false)] out Token token)
        {
            if (SourceFile.TextLines.TryGetAbsolute(line, character, out var absolute) &&
                Root.TryGetToken(absolute, out token))
            {
                return true;
            }
            token = null;
            return false;
        }

        public bool TryGetToken(LinePosition linePosition, [MaybeNullWhen(false)] out Token token)
            => TryGetToken(linePosition.Line, linePosition.Character, out token);
    }
}
