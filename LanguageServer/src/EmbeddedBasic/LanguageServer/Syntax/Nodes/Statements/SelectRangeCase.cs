using System.Collections.Generic;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Statements
{
    public sealed class SelectRangeCase : SelectDefaultCase
    {
        public readonly Expression? FromValue;
        public readonly Expression? ToValue;

        public SelectRangeCase(
            Token startToken,
            Expression? fromValue,
            Expression? toValue,
            IReadOnlyList<Statement> statements) :
            base(startToken, statements)
        {
            FromValue = fromValue;
            ToValue = toValue;
        }

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitSelectRangeCase(this);

        public override string ToString() => $"SelectRangeCase: {Statements.Count} Statement(s)";
    }
}
