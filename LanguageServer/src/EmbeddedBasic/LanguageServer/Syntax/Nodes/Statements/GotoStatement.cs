namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Statements
{
    public sealed class GotoStatement : JumpStatement
    {
        public readonly string Label;

        public GotoStatement(string comment, Token startToken, Token labelIdentifier) :
            base(comment, new SyntaxNodeList().Add(startToken).Add(labelIdentifier))
        {
            Label = labelIdentifier.Lexeme;
        }

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitGotoStatement(this);

        public override string ToString() => $"GotoStatement ({Label})";
    }
}
