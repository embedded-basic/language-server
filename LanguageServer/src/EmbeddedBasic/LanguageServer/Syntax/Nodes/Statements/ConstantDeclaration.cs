using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions.Literals;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Statements
{
    public sealed class ConstantDeclaration : AssignmentStatement, ISymbolDeclaration
    {
        public Literal? Literal => Value as Literal;

        // public DataType DataType => Value.;

        public ConstantDeclaration(
            string comment,
            Token constToken,
            Token nameIdentifier,
            Token equalToken,
            Literal? literal) :
            base(comment, nameIdentifier, equalToken, literal, new SyntaxNodeList().Add(constToken))
        {
        }

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitConstantDeclaration(this);

        public override string ToString() => $"ConstantDeclaration: `{Name}` \"{Literal?.Value}\" {ToFileRangeString()}";
    }
}
