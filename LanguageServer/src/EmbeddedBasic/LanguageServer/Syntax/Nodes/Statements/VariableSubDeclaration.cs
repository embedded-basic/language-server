using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Statements
{
    public class VariableSubDeclaration : SyntaxNode, IDeclaration
    {
        public VariableDeclaration? Main { get; set; } = null;

        public string Comment => Main?.Comment ?? string.Empty;

        public string Name { get; private set; }

        public DataType DataType => Main?.DataType!;

        public Expression? Value { get; private set; }

        public VariableSubDeclaration(
            Token nameIdentifier,
            Expression? value,
            Token endToken) :
            base(new SyntaxNodeList().Add(nameIdentifier).Add(endToken))
        {
            Name = nameIdentifier.Lexeme;
            Value = value;
        }

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitVariableSubDeclaration(this);

        public override string ToString() => $"VariableSubDeclaration `{Name}` {ToFileRangeString()}";
    }
}
