using System.Collections.Generic;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Statements
{
    public class WhileLoop : Loop
    {
        public readonly Expression? Condition;

        public WhileLoop(
            string comment,
            Token startToken,
            Token endToken,
            Expression? condition,
            IReadOnlyList<Statement> statements) :
            base(comment, startToken, endToken, statements)
        {
            Condition = condition;
        }

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitWhileLoop(this);

        public override string ToString() => $"WhileLoop: {Statements.Count} Statement(s)";
    }
}
