namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Statements
{
    public class LabelStatement : Statement
    {
        public readonly string Label;

        public LabelStatement(string comment, Token labelIdentifier, Token endToken) :
            base(comment, new SyntaxNodeList().Add(labelIdentifier).Add(endToken))
        {
            Label = labelIdentifier.Lexeme;
        }

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitLabelStatement(this);

        public override string ToString() => $"LabelStatement ({Label})";
    }
}
