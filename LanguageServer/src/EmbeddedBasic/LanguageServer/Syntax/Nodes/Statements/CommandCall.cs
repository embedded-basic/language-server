using System.Collections.Generic;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Statements
{
    public sealed class CommandCall : Statement
    {
        public readonly string Name;

        public readonly IReadOnlyList<Expression> Arguments;

        public CommandCall(string comment, Token identifier, IReadOnlyList<Expression> arguments) :
            base(comment, new SyntaxNodeList().Add(identifier).AddRange(arguments))
        {
            Name = identifier.Lexeme;
            Arguments = arguments;
        }

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitCommandCall(this);

        public override string ToString() => $"CommandCall ({Name})";
    }
}
