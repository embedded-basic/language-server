using System.Collections.Generic;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Statements
{
    public class Loop : Statement
    {
        public IReadOnlyList<Statement> Statements { get; private set; }

        public Loop(string comment, IReadOnlyList<Statement> statements) :
            base(comment, new SyntaxNodeList().AddRange(statements))
        {
            Statements = statements;
        }

        public Loop(
            string comment,
            Token startToken,
            Token endToken,
            IReadOnlyList<Statement> statements) :
            base(comment, new SyntaxNodeList().Add(startToken).Add(endToken).AddRange(statements))
        {
            Statements = statements;
        }

        protected Loop(
            string comment,
            IReadOnlyList<Statement> statements,
            SyntaxNodeList childNodes) :
            base(comment, childNodes)
        {
            Statements = statements;
        }

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitLoop(this);

        public override string ToString() => $"Loop: {Statements.Count} Statement(s)";
    }
}
