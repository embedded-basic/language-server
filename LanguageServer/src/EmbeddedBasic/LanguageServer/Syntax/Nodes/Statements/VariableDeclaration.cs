using System.Collections.Generic;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Statements
{
    public sealed class VariableDeclaration : Statement
    {
        public DataType DataType { get; private set; }

        public IReadOnlyList<VariableSubDeclaration> SubDeclarations { get; private set; }

        public VariableDeclaration(
            string comment,
            DataType dataType,
            IReadOnlyList<VariableSubDeclaration> subDeclarations) :
            base(
                comment,
                new SyntaxNodeList().Add(dataType).AddRange(subDeclarations))
        {
            DataType = dataType;
            SubDeclarations = subDeclarations;
            foreach (var subDeclaration in subDeclarations)
            {
                subDeclaration.Main = this;
            }
        }

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitVariableDeclaration(this);

        public override string ToString() => $"VariableDeclaration (\"{DataType}\")";
    }
}
