namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Statements
{
    public abstract class Statement : TopLevelNode
    {
        protected Statement(string comment, SyntaxNodeList childNodes) : base(comment, childNodes) { }

        protected Statement(string comment, Token token) : base(comment, token) { }
    }
}
