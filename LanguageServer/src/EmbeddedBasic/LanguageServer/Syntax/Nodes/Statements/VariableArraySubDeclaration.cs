using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Statements
{
    public sealed class VariableArraySubDeclaration : VariableSubDeclaration, IArrayDeclaration
    {
        public int ElementCount { get; }

        public VariableArraySubDeclaration(
            Token nameIdentifier,
            int elementCount,
            Token endToken,
            Expression? value) :
            base(nameIdentifier, value, endToken)
        {
            ElementCount = elementCount;
        }

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitVariableArraySubDeclaration(this);

        public override string ToString() => $"VariableArraySubDeclaration `{Name}[{ElementCount}]` ({ToRangeString()})";
    }
}
