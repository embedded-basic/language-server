using System.Collections.Generic;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Statements
{
    public class Block : SyntaxNode
    {
        public IReadOnlyList<Statement> Statements { get; private set; }

        protected Block(IReadOnlyList<Statement> statements, SyntaxNodeList childNodes) :
            base(childNodes)
        {
            Statements = statements;
        }

        public Block(Token startToken, IReadOnlyList<Statement> statements) :
            this(statements, new SyntaxNodeList().AddRange(statements))
        {
        }

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitBlock(this);

        public override string ToString() => $"Block ({Statements.Count} Statement(s))";
    }
}
