namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Statements
{
    public abstract class JumpStatement : Statement
    {
        protected JumpStatement(
            string comment,
            SyntaxNodeList childNodes) :
            base(comment, childNodes)
        {
        }

        protected JumpStatement(string comment, Token token) :
            base(comment, token)
        {
        }
    }
}
