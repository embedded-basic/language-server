using System;
using EmbeddedBasic.Util;
using EmbeddedBasic.LanguageServer.Syntax;
using EmbeddedBasic.LanguageServer.Syntax.Nodes;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions.Literals;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Statements
{
    public sealed class SymbolDeclaration : Statement, ISymbolDeclaration
    {
        public string Name { get; }

        public IdentifierExpression Identifier { get; }

        public SymbolDeclaration(
            string comment,
            Token symbolToken,
            Token nameIdentifier,
            Token? equalToken,
            IdentifierExpression identifierExpression) :
            base(comment, new SyntaxNodeList()
                                .Add(symbolToken).Add(nameIdentifier)
                                .Add(equalToken).Add(identifierExpression))
        {
            Name = nameIdentifier.Lexeme;
            Identifier = identifierExpression;
        }

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitSymbolDeclaration(this);

        public override string ToString() => $"SymbolDeclaration: `{Name}` \"{Identifier.FullName}\" {ToFileRangeString()}";
    }
}
