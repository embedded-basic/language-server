namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Statements
{
    public sealed class ExitStatement : JumpStatement
    {
        public ExitStatement(string comment, Token token) : base(comment, token) { }

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitExitStatement(this);

        public override string ToString() => $"ExitStatement";
    }
}
