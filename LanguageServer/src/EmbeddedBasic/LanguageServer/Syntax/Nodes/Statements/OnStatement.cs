namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Statements
{
    public abstract class OnStatement : Statement
    {
        public readonly string IndexName;

        protected OnStatement(string comment, Token indexIdentifier, SyntaxNodeList childNodes) :
            base(comment, childNodes)
        {
            IndexName = indexIdentifier.Lexeme;
        }
    }
}
