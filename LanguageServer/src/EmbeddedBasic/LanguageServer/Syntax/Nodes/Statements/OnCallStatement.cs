using System.Collections.Generic;
using System.Linq;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Statements
{
    public sealed class OnFunctionCall : OnStatement
    {
        public readonly IReadOnlyList<string> Names;

        public OnFunctionCall(
            string comment,
            Token startToken,
            Token indexIdentifier,
            Token callToken,
            IReadOnlyList<Token> nameIdentifiers) :
            base(
                comment,
                indexIdentifier,
                new SyntaxNodeList().Add(startToken).Add(indexIdentifier).Add(callToken).AddRange(nameIdentifiers))
        {
            Names = nameIdentifiers.Select((t, s) => t.Lexeme).ToArray();
        }

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitOnFunctionCall(this);

        public override string ToString() => $"OnFunctionCall ({IndexName})";
    }
}
