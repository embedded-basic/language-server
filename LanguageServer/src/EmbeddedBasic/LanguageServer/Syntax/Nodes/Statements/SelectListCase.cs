using System.Collections.Generic;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Statements
{
    public sealed class SelectListCase : SelectDefaultCase
    {
        public readonly IReadOnlyList<Expression> Conditions;

        public SelectListCase(
            Token startToken,
            IReadOnlyList<Expression> conditions,
            IReadOnlyList<Statement> statements) :
            base(startToken, statements)
        {
            Conditions = conditions;
        }

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitSelectListCase(this);

        public override string ToString() => $"SelectListCase: {Statements.Count} Statement(s)";
    }
}
