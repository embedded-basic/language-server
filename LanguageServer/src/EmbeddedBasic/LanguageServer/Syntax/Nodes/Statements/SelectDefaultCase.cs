using System.Collections.Generic;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Statements
{
    public class SelectDefaultCase : Block
    {
        public SelectDefaultCase(Token startToken, IReadOnlyList<Statement> statements) :
            base(startToken, statements)
        {
        }

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitSelectDefaultCase(this);

        public override string ToString() => $"SelectDefaultCase: {Statements.Count} Statement(s)";
    }
}
