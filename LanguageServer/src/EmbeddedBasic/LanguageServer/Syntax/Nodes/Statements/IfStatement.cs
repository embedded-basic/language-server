using System.Collections.Generic;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Statements
{
    public class IfStatement : Statement
    {
        public readonly IReadOnlyList<IfBranch> IfBranches;

        public readonly Block? ElseBranch;

        public IfStatement(
            Token startToken,
            Token? endToken,
            IReadOnlyList<IfBranch> ifBranches,
            Block? elseBranch,
            string? comment = null) :
            base(
                comment ?? string.Empty,
                new SyntaxNodeList().Add(startToken).Add(endToken).AddRange(ifBranches).Add(elseBranch))
        {
            IfBranches = ifBranches;
            ElseBranch = elseBranch;
        }

        public IfStatement(
            Token startToken,
            IfBranch ifBranch,
            string? comment = null) :
            base(
                comment ?? string.Empty,
                new SyntaxNodeList().Add(startToken).Add(ifBranch))
        {
            IfBranches = new[] { ifBranch };
            ElseBranch = null;
        }

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitIfStatement(this);

        public override string ToString() => $"IfStatement ({IfBranches.Count} branches)";
    }
}
