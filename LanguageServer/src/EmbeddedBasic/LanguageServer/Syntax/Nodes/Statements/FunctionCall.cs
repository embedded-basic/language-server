using System.Collections.Generic;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Statements
{
    public sealed class FunctionCall : Statement
    {
        public string Name => Expression.Name.FullName;

        public IReadOnlyList<Expression> Arguments => Expression.Arguments;

        public readonly CallExpression Expression;

        public FunctionCall(string comment, CallExpression expression) :
            base(comment, new SyntaxNodeList().Add(expression))
        {
            Expression = expression;
        }

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitFunctionCall(this);

        public override string ToString() => $"FunctionCall ({Name})";
    }
}
