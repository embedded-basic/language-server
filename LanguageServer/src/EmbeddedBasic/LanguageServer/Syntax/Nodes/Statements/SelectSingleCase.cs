using System.Collections.Generic;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Statements
{
    public sealed class SelectSingleCase : SelectDefaultCase
    {
        public readonly Expression? Condition;

        public SelectSingleCase(
            Token startToken,
            Expression? condition,
            IReadOnlyList<Statement> statements) :
            base(startToken, statements)
        {
            Condition = condition;
        }

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitSelectSingleCase(this);

        public override string ToString() => $"SelectSingleCase: {Statements.Count} Statement(s)";
    }
}
