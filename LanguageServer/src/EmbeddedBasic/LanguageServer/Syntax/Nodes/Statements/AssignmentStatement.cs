using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Statements
{
    /// <summary>
    /// An assignment statement like "a = b".
    /// </summary>
    public class AssignmentStatement : Statement
    {
        public string Name { get; }

        public Expression? Value { get; }

        public AssignmentStatement(
            string comment,
            Token nameIdentifier,
            Token equalToken,
            Expression? value,
            SyntaxNodeList? childNodes = null) :
            base(comment, (childNodes ?? new SyntaxNodeList()).Add(nameIdentifier).Add(equalToken).Add(value))
        {
            Name = nameIdentifier.Lexeme;
            Value = value;
        }

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitAssignmentStatement(this);

        public override string ToString() => $"AssignmentStatement (\"{Name}\")";
    }
}
