using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions.Literals;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Statements
{
    public sealed class WaitStatement : Statement
    {
        public readonly Expression Condition;

        public readonly IntegerLiteral? Timeout;

        public readonly JumpStatement? JumpStatement;

        public WaitStatement(
            string comment,
            Token waitToken,
            Expression condition,
            IntegerLiteral? timeout = null,
            JumpStatement? jumpStatement = null) :
            base(comment, new SyntaxNodeList().Add(waitToken).Add(condition).Add(timeout).Add(jumpStatement))
        {
            Condition = condition;
            Timeout = timeout;
            JumpStatement = jumpStatement;
        }

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitWaitStatement(this);

        public override string ToString() => $"WaitStatement";
    }
}
