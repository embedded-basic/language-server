using System.Collections.Generic;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Statements
{
    public class IfBranch : Block
    {
        public readonly Expression? Condition;

        public IfBranch(
            Token startToken,
            SyntaxNode endNode,
            Expression? condition,
            IReadOnlyList<Statement> statements) :
            base(statements, new SyntaxNodeList().Add(startToken).Add(endNode).Add(condition).AddRange(statements))
        {
            Condition = condition;
        }

        public IfBranch(
            Token startToken,
            Expression? condition,
            IReadOnlyList<Statement> statements) :
            base(statements, new SyntaxNodeList().Add(startToken).Add(condition).AddRange(statements))
        {
            Condition = condition;
        }

        public IfBranch(
            Token startToken,
            SyntaxNode endNode,
            Expression? condition,
            params Statement[] statements) :
            this(startToken, endNode, condition, (IReadOnlyList<Statement>)statements)
        {
        }

        public IfBranch(
            Token startToken,
            Expression? condition,
            params Statement[] statements) :
            this(startToken, condition, (IReadOnlyList<Statement>)statements)
        {
        }

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitIfBranch(this);

        public override string ToString() => $"IfBranch";
    }
}
