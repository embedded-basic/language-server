using System.Collections.Generic;
using System.Linq;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Statements
{
    public sealed class OnGotoStatement : OnStatement
    {
        public readonly IReadOnlyList<string> Labels;

        public OnGotoStatement(
            string comment,
            Token startToken,
            Token indexIdentifier,
            Token gotoToken,
            IReadOnlyList<Token> labelIdentifiers) :
            base(comment, indexIdentifier, new SyntaxNodeList().
                                                Add(startToken).Add(indexIdentifier).
                                                Add(gotoToken).AddRange(labelIdentifiers))
        {
            Labels = labelIdentifiers.Select((t, i) => t.Lexeme).ToArray();
        }

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitOnGotoStatement(this);

        public override string ToString() => $"OnGotoStatement ({IndexName})";
    }
}
