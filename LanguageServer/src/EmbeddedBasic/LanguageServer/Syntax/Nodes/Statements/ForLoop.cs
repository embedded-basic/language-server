using System.Collections.Generic;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Statements
{
    public class ForLoop : Loop
    {
        public readonly AssignmentStatement Assignment;

        public readonly Expression? Target;

        public readonly Expression? Step;

        public ForLoop(
            string comment,
            Token startToken,
            Token endToken,
            AssignmentStatement assignment,
            Expression? target,
            Expression? step,
            IReadOnlyList<Statement> statements) :
            base(comment, statements, new SyntaxNodeList().
                    Add(startToken).Add(endToken).AddRange(statements).
                    Add(assignment).Add(target).Add(step))
        {
            Assignment = assignment;
            Target = target;
            Step = step;
        }

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitForLoop(this);

        public override string ToString() => $"ForLoop: {Statements.Count} Statement(s)";
    }
}
