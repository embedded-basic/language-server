using System.Collections.Generic;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Statements
{
    public class SelectStatement : Statement
    {
        public readonly string Identifier;

        public readonly IReadOnlyList<SelectDefaultCase> Cases;

        public readonly SelectDefaultCase? DefaultCase;

        public SelectStatement(
            string comment,
            Token startToken,
            Token endToken,
            Token identifierToken,
            IReadOnlyList<SelectDefaultCase> cases,
            SelectDefaultCase? defaultCase) :
            base(comment, new SyntaxNodeList().
                            Add(startToken).Add(endToken).
                            Add(identifierToken).AddRange(cases).Add(defaultCase))
        {
            Identifier = identifierToken.Lexeme;
            Cases = cases;
            DefaultCase = defaultCase;
        }

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitSelectStatement(this);

        public override string ToString()
            => @$"SelectStatement `{Identifier}`, {Cases.Count
                    + (DefaultCase == null ? 0 : 1)} Cases {ToFileRangeString()}";
    }
}
