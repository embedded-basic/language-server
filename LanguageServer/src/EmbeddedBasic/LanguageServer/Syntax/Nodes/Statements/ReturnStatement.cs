using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Statements
{
    public sealed class ReturnStatement : JumpStatement
    {
        public readonly Expression? Expression;

        public ReturnStatement(string comment, Token returnKeyword, Expression? expression = null) :
            base(comment, new SyntaxNodeList().Add(returnKeyword).Add(expression))
        {
            Expression = expression;
        }

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitReturnStatement(this);

        public override string ToString() => $"ReturnStatement";
    }
}
