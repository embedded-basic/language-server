using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes
{
    /// <summary>
    /// An abstract syntax tree node.
    /// </summary>
    public abstract class SyntaxNode : IComparable<SyntaxNode>, IComparable<LinePosition>, IComparable<int>
    {
        public sealed class IdentityComparer : IComparer<SyntaxNode>
        {
            public static readonly IdentityComparer Instance = new();

            private IdentityComparer() { }

            public int Compare(SyntaxNode? x, SyntaxNode? y)
            {
                if (object.ReferenceEquals(x, null)) return object.ReferenceEquals(y, null) ? 0 : -1;
                if (object.ReferenceEquals(y, null)) return 1;

                return x.GetHashCode().CompareTo(y.GetHashCode());
            }
        }

        /// <summary>
        /// All child nodes.
        /// </summary>
        protected readonly SyntaxNodeList _childNodes;

        /// <summary>
        /// Child nodes for the initial source file accessible by their position.
        /// </summary>
        private readonly PositionalSyntaxNodeList _childNodesByPosition;

        /// <summary>
        /// All child nodes.
        /// </summary>
        public SyntaxNodeList.ReadOnly ChildNodes => _childNodes.AsReadOnly();

        /// <summary>
        /// The syntax tree to which this node belongs.
        /// </summary>
        public SyntaxTree Tree { get; protected set; }

        /// <summary>
        /// The source file to which this node refers.
        /// </summary>
        public SourceFile SourceFile { get; private set; }

        /// <summary>
        /// The path of the file to which this range refers.
        /// </summary>
        public string FilePath => SourceFile.FilePath;

        /// <summary>
        /// The name of the file to which this range refers.
        /// </summary>
        public string FileName => SourceFile.FileName;

        /// <summary>
        /// The path of the file to which this range refers.
        /// </summary>
        public TextSpan TextSpan { get; protected set; }

        /// <summary>
        /// The absolute start position of the node inside the source text.
        /// </summary>
        public int Start => TextSpan.Start;

        /// <summary>
        /// The absolute end position of the node inside the source text.
        /// </summary>
        public int End => TextSpan.End;

        /// <summary>
        /// The parent node for this node, or null.
        /// </summary>
        public SyntaxNode? ParentNode { get; internal set; }

        /// <summary>
        /// Create a syntax node with one child node (token).
        /// </summary>
        /// <param name="tree">The syntax tree to which this node belongs.</param>
        /// <param name="sourceFile">The source file to which this node belongs.</param>
        /// <param name="textSpan">The text span of this node in the source text.</param>
        protected SyntaxNode(SyntaxTree tree, SourceFile sourceFile, TextSpan textSpan = default)
        {
            _childNodes = new();
            _childNodesByPosition = new(this);

            Tree = tree;
            SourceFile = sourceFile;
            TextSpan = textSpan;
        }

        /// <summary>
        /// Create a syntax node with one child node (token).
        /// </summary>
        /// <param name="token">The token as only child node.</param>
        protected SyntaxNode(Token token)
        {
            _childNodes = new();
            _childNodesByPosition = new(this);

            Tree = token.Tree;
            SourceFile = token.SourceFile;
            TextSpan = token.TextSpan;
        }

        /// <summary>
        /// Create a syntax node with child nodes.
        /// </summary>
        /// <remarks>
        /// The child nodes should already be sorted in descendant order
        /// according to their source text positions.
        /// This will minimize run-time overhead.
        /// </remarks>
        /// <param name="childNodes">The child nodes. At least one child must be present.</param>
        protected SyntaxNode(SyntaxNodeList childNodes)
        {
            _childNodes = childNodes;
            _childNodesByPosition = new(this);

            var firstChild = _childNodes.First();
            Tree = firstChild.Tree;
            RegisterChildNodes();

            SourceFile = firstChild.SourceFile;
            TextSpan = new(firstChild.Start, _childNodes.Last().End);
        }

        protected void RegisterChildNodes()
        {
            foreach (var childNode in _childNodes)
            {
                childNode.ParentNode = this;

                if (Tree.SourceFile == childNode.SourceFile)
                {
                    _childNodesByPosition.Add(childNode);
                }
            }
        }

        /// <summary>
        /// Try to get a token related to this node by an absolute position in the source text.
        /// </summary>
        /// <param name="absolute">The absolute position in the source text where the token is expected to be.</param>
        /// <returns>Could a token be found at the given position?</returns>
        public virtual bool TryGetToken(int absolute, [MaybeNullWhen(false)] out Token token)
        {
            if (_childNodesByPosition.TryGetValue(absolute, out var childNode) &&
                childNode.TryGetToken(absolute, out token))
            {
                return true;
            }
            token = null;
            return false;
        }

        /// <summary>
        /// Accept a visitor to the node.
        /// </summary>
        /// <param name="visitor">The visitor to accept.</param>
        protected internal abstract void Accept(SyntaxNodeVisitor visitor);

        public int CompareTo(SyntaxNode? other)
            => other == null ? 1 : TextSpan.CompareTo(other.TextSpan);

        public int CompareTo(LinePosition other)
            => SourceFile.TextLines.TryGetAbsolute(other, out var absolute) ? TextSpan.CompareTo(absolute) : 1;

        public int CompareTo(int other) => TextSpan.CompareTo(other);

        public string ToRangeString()
        {
            if (SourceFile.TextLines.TryGetLinePosition(Start, out var start) &&
                SourceFile.TextLines.TryGetLinePosition(End, out var end))
            {
                return $"{Start}/{start.Line}/{start.Character} -> {End}/{end.Line}/{end.Character}";
            }
            return $"{Start} -> {End}";
        }

        public string ToFileRangeString() => $"(\"{FileName}\": {ToRangeString()})";

        public override string ToString() => ToFileRangeString();
    }
}
