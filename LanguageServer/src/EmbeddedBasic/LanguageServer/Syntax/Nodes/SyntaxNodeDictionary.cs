using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes
{
    public class SyntaxNodeDictionary<T> : Dictionary<string, T>
        where T : SyntaxNode
    {
        public class ReadOnly : ReadOnlyDictionary<string, T>
        {
            private SyntaxNodeDictionary<T> _nodeDict;

            public ReadOnly(SyntaxNodeDictionary<T> nodeDict) : base(nodeDict) { _nodeDict = nodeDict; }
        }

        private ReadOnly? _readOnly = null;

        public ReadOnly AsReadOnly()
        {
            if (_readOnly != null) return _readOnly;
            return _readOnly = new(this);
        }

        public SyntaxNodeDictionary() { }

        public void Add<TValue>(TValue node) where TValue : T, ISymbolDeclaration => Add(node.Name, node);
    }
}
