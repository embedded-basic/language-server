using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes
{
    /// <summary>
    /// A declaration of an identifier. For instance a constant, variable or function.
    /// </summary>
    public interface IDeclaration : ISymbolDeclaration
    {
        DataType DataType { get; }
    }
}
