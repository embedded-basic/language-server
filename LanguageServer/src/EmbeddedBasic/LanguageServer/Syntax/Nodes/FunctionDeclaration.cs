using System.Collections.Generic;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Statements;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes
{
    public sealed class FunctionDeclaration : TopLevelNode, IDeclaration
    {
        public string Name { get; }

        DataType IDeclaration.DataType => ReturnType;

        public DataType ReturnType { get; }

        public IReadOnlyList<FunctionParameterDeclaration> Parameters { get; }

        public IReadOnlyList<Statement> Statements { get; }

        public FunctionDeclaration(
            string comment,
            Token startToken,
            Token endToken,
            Token nameIdentifier,
            DataType returnType,
            IReadOnlyList<FunctionParameterDeclaration> parameters,
            IReadOnlyList<Statement> statements) :
            base(comment, new SyntaxNodeList().
                            Add(startToken).Add(endToken).
                            Add(nameIdentifier).AddRange(parameters).AddRange(statements))
        {
            Name = nameIdentifier.Lexeme;
            ReturnType = returnType;
            Parameters = parameters;
            Statements = statements;
        }

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitFunctionDeclaration(this);

        public override string ToString() => $"FunctionDeclaration `{Name}: {ReturnType.Kind}` ({ToRangeString()})";
    }
}
