namespace EmbeddedBasic.LanguageServer.Syntax.Nodes
{
    /// <summary>
    /// A declaration of an identifier. For instance a constant, variable or function.
    /// </summary>
    public interface ISymbolDeclaration
    {
        string Comment { get; }

        string Name { get; }
    }
}
