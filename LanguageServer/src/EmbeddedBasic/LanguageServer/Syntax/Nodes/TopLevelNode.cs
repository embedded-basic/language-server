namespace EmbeddedBasic.LanguageServer.Syntax.Nodes
{
    public abstract class TopLevelNode : CommentableNode
    {
        protected TopLevelNode(string comment, SyntaxNodeList childNodes) : base(comment, childNodes) { }

        protected TopLevelNode(string comment, Token token) :
            base(comment, new SyntaxNodeList().Add(token))
        {
        }
    }
}
