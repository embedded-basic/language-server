using System;
using System.Collections.Generic;
using System.Linq;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Directives
{
    public class DefineDirective : Directive, ISymbolDeclaration
    {
        private static readonly Token[] NoTokens = new Token[0];

        public string Name { get; private set; }

        private readonly Token[] _parameterIdentifiers;

        public IReadOnlyList<Token> ParameterIdentifiers => _parameterIdentifiers;

        // public readonly IReadOnlyList<string> Parameters;

        // public readonly IReadOnlyList<string> Parameters;

        public readonly IReadOnlyList<Token> ContentTokens;

        protected DefineDirective(
            string comment,
            Token defineToken,
            int end,
            Token nameIdentifier,
            IReadOnlyList<Token> parameterIdentifiers,
            IReadOnlyList<Token> contentTokens) :
            base(comment, new SyntaxNodeList().
                            Add(defineToken).Add(nameIdentifier).
                            AddRange(parameterIdentifiers).AddRange(contentTokens))
        {
            Name = nameIdentifier.Lexeme;
            _parameterIdentifiers = parameterIdentifiers.ToArray();
            ContentTokens = contentTokens;
        }

        public DefineDirective(
            string comment,
            Token defineToken,
            Token nameIdentifier,
            IReadOnlyList<Token> parameterIdentifiers,
            IReadOnlyList<Token> contentTokens) :
            this(
                comment,
                defineToken,
                contentTokens.Last()?.End ?? nameIdentifier.End,
                nameIdentifier,
                parameterIdentifiers,
                contentTokens)
        {
        }

        public DefineDirective(
            string comment,
            Token defineToken,
            Token nameIdentifier,
            IReadOnlyList<Token> contentTokens) :
            this(comment, defineToken, nameIdentifier, NoTokens, contentTokens)
        {
        }

        public DefineDirective(
            string comment,
            Token defineToken,
            Token nameIdentifier) :
            this(comment, defineToken, nameIdentifier.End, nameIdentifier, NoTokens, NoTokens)
        {
        }

        public bool HasParameter(string name)
            => Array.FindIndex(_parameterIdentifiers, t => t.Lexeme == name) >= 0;

        public bool HasParameter(Token nameToken)
            => Array.FindIndex(_parameterIdentifiers, t => t.Lexeme == nameToken.Lexeme) >= 0;

        public bool TryGetParameterIndex(Token nameToken, out int index)
            => (index = Array.FindIndex(_parameterIdentifiers, t => t.Lexeme == nameToken.Lexeme)) >= 0;

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitDefineDirective(this);

        public override string ToString() => $"DefineDirective: {Name} ({ToRangeString()})";
    }
}
