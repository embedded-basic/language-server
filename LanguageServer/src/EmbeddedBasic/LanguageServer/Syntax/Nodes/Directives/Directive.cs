namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Directives
{
    public abstract class Directive : TopLevelNode
    {
        protected Directive(
            string comment,
            SyntaxNodeList childNodes) :
            base(comment, childNodes)
        {
        }
    }
}
