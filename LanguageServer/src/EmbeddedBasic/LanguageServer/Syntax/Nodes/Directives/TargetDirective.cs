using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Directives
{
    public class TargetDirective : Directive
    {
        public readonly IdentifierExpression Feature;

        public readonly Expression Value;

        public TargetDirective(string comment, Token targetToken, IdentifierExpression feature, Expression value) :
            base(comment, new SyntaxNodeList().Add(targetToken).Add(feature).Add(value))
        {
            Feature = feature;
            Value = value;
        }

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitTargetDirective(this);

        public override string ToString() => $"TargetDirective: {Feature.FullName}, {Value} ({ToRangeString()})";
    }
}
