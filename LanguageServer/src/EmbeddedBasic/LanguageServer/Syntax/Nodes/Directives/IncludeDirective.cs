using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions.Literals;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Directives
{
    public class IncludeDirective : Directive
    {
        public readonly StringLiteral Path;

        public IncludeDirective(string comment, Token includeToken, StringLiteral path) :
            base(comment, new SyntaxNodeList().Add(includeToken).Add(path))
        {
            Path = path;
        }

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitIncludeDirective(this);

        public SourceFile ToSourceFile(SourceFile? refFile = null)
            => SourceFile.FromInclude(Path.StringValue, refFile);

        public override string ToString() => $"IncludeDirective: \"{Path.StringValue}\" ({ToRangeString()})";
    }
}
