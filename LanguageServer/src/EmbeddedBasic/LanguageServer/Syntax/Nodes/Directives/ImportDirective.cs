using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions.Literals;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Directives
{
    public class ImportDirective : Directive
    {
        public readonly IdentifierExpression Identifier;

        public readonly StringLiteral? Path;

        public ImportDirective(string comment, Token importToken, IdentifierExpression identifier, StringLiteral path) :
            base(comment, new SyntaxNodeList().Add(importToken).Add(identifier).Add(path))
        {
            Identifier = identifier;
            Path = path;
        }

        public ImportDirective(string comment, Token importToken, IdentifierExpression identifier) :
            base(comment, new SyntaxNodeList().Add(importToken).Add(identifier))
        {
            Identifier = identifier;
            Path = null;
        }

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitImportDirective(this);

        public SourceFile ToSourceLocation(SourceFile? refFile = null)
            => SourceFile.FromImport(Path?.StringValue, Identifier.FullName, refFile);

        public override string ToString()
            => $"ImportDirective: {Identifier.FullName}" +
                    (Path == null ? "" : $" from \"{Path.StringValue}\"") +
                    $" ({ToRangeString()})";
    }
}
