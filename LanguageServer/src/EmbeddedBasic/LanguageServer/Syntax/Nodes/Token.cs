using System.Diagnostics.CodeAnalysis;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes
{
    /// <summary>
    /// A token.
    /// </summary>
    public sealed class Token : SyntaxNode
    {
        public readonly TokenKind Kind;

        public readonly string Lexeme;

        public Token(SyntaxTree tree, TokenKind kind, SourceFile sourceFile, int start, int end, string lexeme) :
            base(tree, sourceFile, new TextSpan(start, end))
        {
            Kind = kind;
            Lexeme = lexeme;
        }

        /// <summary>
        /// Try to get this token if its covers the given absolute position in the source text.
        /// </summary>
        /// <param name="absolute">The absolute position in the source text to match.</param>
        /// <returns>Does this token cover the given position?</returns>
        public override bool TryGetToken(int absolute, [MaybeNullWhen(false)] out Token token)
        {
            if (this.CompareTo(absolute) == 0)
            {
                token = this;
                return true;
            }
            token = null;
            return false;
        }

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitToken(this);

        public string LexemeAsString() => Kind switch
        {
            TokenKind.StringLiteral => Lexeme.Substring(1, Lexeme.Length - 2),
            _ => Lexeme
        };

        public string GetLexemeDescription()
        {
            var lexeme = Lexeme.Replace("\n", "");
            if (!string.IsNullOrEmpty(lexeme)) return $"\"{lexeme}\"";
            return ErrorMessages.GetString(Kind);
        }

        public override string ToString()
            => $"Token: {Kind}, {GetLexemeDescription()} ({ToRangeString()})";
    }
}
