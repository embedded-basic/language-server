namespace EmbeddedBasic.LanguageServer.Syntax.Nodes
{
    /// <summary>
    /// The kind of token.
    /// </summary>
    public enum TokenKind
    {
        /// <summary>
        /// Error.
        /// </summary>
        Error,
        /// <summary>
        /// End of file.
        /// </summary>
        Eof,
        /// <summary>
        /// A line break.
        /// </summary>
        Linebreak = 101,
        /// <summary>
        /// An identifier.
        /// </summary>
        Identifier,

        // Symbols ---------------------------------------------------------- //

        /// <summary>
        /// The symbol `.`.
        /// </summary>
        DotSymbol = 201,
        /// <summary>
        /// The symbol `:`.
        /// </summary>
        ColonSymbol,
        /// <summary>
        /// The symbol `,`.
        /// </summary>
        CommaSymbol,
        /// <summary>
        /// The symbol `+`.
        /// </summary>
        PlusSymbol,
        /// <summary>
        /// The symbol `++`.
        /// </summary>
        PlusPlusSymbol,
        /// <summary>
        /// The symbol `-`.
        /// </summary>
        MinusSymbol,
        /// <summary>
        /// The symbol `*`.
        /// </summary>
        AsteriskSymbol,
        /// <summary>
        /// The symbol `/`.
        /// </summary>
        SlashSymbol,
        /// <summary>
        /// The symbol `^`.
        /// </summary>
        CaretSymbol,
        /// <summary>
        /// The symbol `<`.
        /// </summary>
        LessThanSymbol,
        /// <summary>
        /// The symbol `<<`.
        /// </summary>
        LessThanLessThanSymbol,
        /// <summary>
        /// The symbol `<=`.
        /// </summary>
        LessThanOrEqualSymbol,
        /// <summary>
        /// The symbol `<>`.
        /// </summary>
        LessThanGreaterThanSymbol,
        /// <summary>
        /// The symbol `>`.
        /// </summary>
        GreaterThanSymbol,
        /// <summary>
        /// The symbol `>=`.
        /// </summary>
        GreaterThanOrEqualSymbol,
        /// <summary>
        /// The symbol `>>`.
        /// </summary>
        GreaterThanGreaterThanSymbol,
        /// <summary>
        /// The symbol `=`.
        /// </summary>
        EqualSymbol,
        /// <summary>
        /// The symbol `==`.
        /// </summary>
        EqualEqualSymbol,
        /// <summary>
        /// The symbol `(`.
        /// </summary>
        ParenthesisLeft,
        /// <summary>
        /// The symbol `)`.
        /// </summary>
        ParenthesisRight,
        /// <summary>
        /// The symbol `[`.
        /// </summary>
        BracketLeft,
        /// <summary>
        /// The symbol `]`.
        /// </summary>
        BracketRight,

        // Preprocessor-Keywords -------------------------------------------- //

        /// <summary>
        /// Preprocessor keyword `#include`.
        /// </summary>
        IncludePPKeyword = 301,
        /// <summary>
        /// Preprocessor keyword `#define`.
        /// </summary>
        DefinePPKeyword,
        /// <summary>
        /// Preprocessor keyword `#undefine`.
        /// </summary>
        UndefinePPKeyword,
        /// <summary>
        /// Preprocessor keyword `#ifdef`.
        /// </summary>
        IfdefPPKeyword,
        /// <summary>
        /// Preprocessor keyword `#ifndef`.
        /// </summary>
        IfndefPPKeyword,
        /// <summary>
        /// Preprocessor keyword `#else`.
        /// </summary>
        ElsePPKeyword,
        /// <summary>
        /// Preprocessor keyword `#endif`.
        /// </summary>
        EndifPPKeyword,
        /// <summary>
        /// Preprocessor keyword `#target`.
        /// </summary>
        TargetPPKeyword,
        /// <summary>
        /// Preprocessor keyword `#region`.
        /// </summary>
        RegionPPKeyword,
        /// <summary>
        /// Preprocessor keyword `#endregion`.
        /// </summary>
        EndRegionPPKeyword,
        /// <summary>
        /// Preprocessor keyword `import`.
        /// </summary>
        ImportPPKeyword,
        /// <summary>
        /// Preprocessor keyword `from`.
        /// </summary>
        FromPPKeyword,

        // Keywords --------------------------------------------------------- //

        /// <summary>
        /// Keyword `not`.
        /// </summary>
        NotKeyword = 401,
        /// <summary>
        /// Keyword `neg`.
        /// </summary>
        NegKeyword,
        /// <summary>
        /// Keyword `mod`.
        /// </summary>
        ModKeyword,
        /// <summary>
        /// Keyword `and`.
        /// </summary>
        AndKeyword,
        /// <summary>
        /// Keyword `or`.
        /// </summary>
        OrKeyword,
        /// <summary>
        /// Keyword `xor`.
        /// </summary>
        XorKeyword,
        /// <summary>
        /// Keyword `true`.
        /// </summary>
        TrueKeyword,
        /// <summary>
        /// Keyword `false`.
        /// </summary>
        FalseKeyword,
        /// <summary>
        /// Keyword `low`.
        /// </summary>
        LowKeyword,
        /// <summary>
        /// Keyword `high`.
        /// </summary>
        HighKeyword,
        /// <summary>
        /// Keyword `bool`.
        /// </summary>
        BoolKeyword,
        /// <summary>
        /// Keyword `char`.
        /// </summary>
        CharKeyword,
        /// <summary>
        /// Keyword `byte`.
        /// </summary>
        ByteKeyword,
        /// <summary>
        /// Keyword `word`.
        /// </summary>
        WordKeyword,
        /// <summary>
        /// Keyword `short`.
        /// </summary>
        ShortKeyword,
        /// <summary>
        /// Keyword `int`.
        /// </summary>
        IntKeyword,
        /// <summary>
        /// Keyword `float`.
        /// </summary>
        FloatKeyword,
        /// <summary>
        /// Keyword `symbol`.
        /// </summary>
        SymbolKeyword,
        /// <summary>
        /// Keyword `const`.
        /// </summary>
        ConstKeyword,
        /// <summary>
        /// Keyword `return`.
        /// </summary>
        ReturnKeyword,
        /// <summary>
        /// Keyword `on`.
        /// </summary>
        OnKeyword,
        /// <summary>
        /// Keyword `goto`.
        /// </summary>
        GotoKeyword,
        /// <summary>
        /// Keyword `wait`.
        /// </summary>
        WaitKeyword,
        /// <summary>
        /// Keyword `if`.
        /// </summary>
        IfKeyword,
        /// <summary>
        /// Keyword `then`.
        /// </summary>
        ThenKeyword,
        /// <summary>
        /// Keyword `else`.
        /// </summary>
        ElseKeyword,
        /// <summary>
        /// Keyword `elseif`.
        /// </summary>
        ElseIfKeyword,
        /// <summary>
        /// Keyword `endif`.
        /// </summary>
        EndIfKeyword,
        /// <summary>
        /// Keyword `select`.
        /// </summary>
        SelectKeyword,
        /// <summary>
        /// Keyword `case`.
        /// </summary>
        CaseKeyword,
        /// <summary>
        /// Keyword `to`.
        /// </summary>
        ToKeyword,
        /// <summary>
        /// Keyword `endselect`.
        /// </summary>
        EndSelectKeyword,
        /// <summary>
        /// Keyword `call`.
        /// </summary>
        CallKeyword,
        /// <summary>
        /// Keyword `exit`.
        /// </summary>
        ExitKeyword,
        /// <summary>
        /// Keyword `loop`.
        /// </summary>
        LoopKeyword,
        /// <summary>
        /// Keyword `endloop`.
        /// </summary>
        EndLoopKeyword,
        /// <summary>
        /// Keyword `while`.
        /// </summary>
        WhileKeyword,
        /// <summary>
        /// Keyword `endwhile`.
        /// </summary>
        EndWhileKeyword,
        /// <summary>
        /// Keyword `for`.
        /// </summary>
        ForKeyword,
        /// <summary>
        /// Keyword `step`.
        /// </summary>
        StepKeyword,
        /// <summary>
        /// Keyword `endfor`.
        /// </summary>
        EndForKeyword,
        /// <summary>
        /// Keyword `function`.
        /// </summary>
        FunctionKeyword,
        /// <summary>
        /// Keyword `endfunction`.
        /// </summary>
        EndFunctionKeyword,

        // Literals --------------------------------------------------------- //

        /// <summary>
        /// A binary number literal.
        /// </summary>
        BinNumberLiteral = 501,
        /// <summary>
        /// A hexadecimal number literal.
        /// </summary>
        HexNumberLiteral,
        /// <summary>
        /// An integer literal.
        /// </summary>
        IntegerLiteral,
        /// <summary>
        /// A decimal literal.
        /// </summary>
        DecimalLiteral,
        /// <summary>
        /// A date literal.
        /// </summary>
        DateLiteral,
        /// <summary>
        /// A time literal.
        /// </summary>
        TimeLiteral,
        /// <summary>
        /// A char literal.
        /// </summary>
        CharLiteral,
        /// <summary>
        /// A string literal.
        /// </summary>
        StringLiteral,
        /// <summary>
        /// A device name.
        /// </summary>
        DeviceNameLiteral,
        /// <summary>
        /// A device ID.
        /// </summary>
        DeviceIdLiteral
    }
}
