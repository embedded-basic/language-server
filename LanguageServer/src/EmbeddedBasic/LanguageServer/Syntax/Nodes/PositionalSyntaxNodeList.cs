using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using EmbeddedBasic.Util;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes
{
    public sealed class PositionalSyntaxNodeList : SortedArrayList<SyntaxNode>
    {
        [DebuggerStepThrough]
        public class ReadOnly : ReadOnlyCollection<SyntaxNode>
        {
            private PositionalSyntaxNodeList _parentNodeList;

            public ReadOnly(PositionalSyntaxNodeList nodeList) : base(nodeList) { _parentNodeList = nodeList; }

            /// <summary>
            /// Try to get the node covering the given position.
            /// </summary>
            /// <param name="position">The position to check.</param>
            /// <param name="node">The node or null.</param>
            /// <returns>Does a node cover the given position?</returns>
            public bool TryGetValue(int absolute, [MaybeNullWhen(false)] out SyntaxNode node)
                => _parentNodeList.TryGetValue(absolute, out node);

            /// <summary>
            /// Try to get the node covering the given position.
            /// </summary>
            /// <param name="line">The line of the position to check.</param>
            /// <param name="character">The character of the position to check.</param>
            /// <param name="node">The node or null.</param>
            /// <returns>Does a node cover the given position?</returns>
            public bool TryGetValue(int line, int character, [MaybeNullWhen(false)] out SyntaxNode node)
                => _parentNodeList.TryGetValue(line, character, out node);

            /// <summary>
            /// Try to get the node covering the given position.
            /// </summary>
            /// <param name="linePosition">The line position to check.</param>
            /// <param name="node">The node or null.</param>
            /// <returns>Does a node cover the given position?</returns>
            public bool TryGetValue(LinePosition position, [MaybeNullWhen(false)] out SyntaxNode node)
                => _parentNodeList.TryGetValue(position, out node);
        }

        private readonly SyntaxNode _parentNode;

        private ReadOnly? _readOnly = null;

        [DebuggerStepThrough]
        public new ReadOnly AsReadOnly()
        {
            if (_readOnly != null) return _readOnly;
            return _readOnly = new(this);
        }

        public PositionalSyntaxNodeList(SyntaxNode parentNode) => _parentNode = parentNode;

        /// <summary>
        /// Try to get the node covering the given position.
        /// </summary>
        /// <param name="position">The position to check.</param>
        /// <param name="node">The node or null.</param>
        /// <returns>Does a node cover the given position?</returns>
        public bool TryGetValue(int absolute, [MaybeNullWhen(false)] out SyntaxNode node)
            => _items.TryGetValue(_size, absolute, out node);

        /// <summary>
        /// Try to get the node covering the given position.
        /// </summary>
        /// <param name="line">The line of the position to check.</param>
        /// <param name="character">The character of the position to check.</param>
        /// <param name="node">The node or null.</param>
        /// <returns>Does a node cover the given position?</returns>
        public bool TryGetValue(int line, int character, [MaybeNullWhen(false)] out SyntaxNode node)
        {
            if (_parentNode.SourceFile.TextLines.TryGetAbsolute(line, character, out var absolute) &&
                TryGetValue(absolute, out node))
            {
                return true;
            }
            node = null;
            return false;
        }

        /// <summary>
        /// Try to get the node covering the given position.
        /// </summary>
        /// <param name="linePosition">The line position to check.</param>
        /// <param name="node">The node or null.</param>
        /// <returns>Does a node cover the given position?</returns>
        public bool TryGetValue(LinePosition linePosition, [MaybeNullWhen(false)] out SyntaxNode node)
            => TryGetValue(linePosition.Line, linePosition.Character, out node);
    }
}
