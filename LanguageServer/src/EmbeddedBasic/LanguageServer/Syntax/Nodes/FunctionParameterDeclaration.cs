using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes
{
    /// <summary>
    /// The declaration of a function parameter.
    /// </summary>
    public class FunctionParameterDeclaration : SyntaxNode, IArrayDeclaration
    {
        private FunctionDeclaration? Main { get; set; } = null;

        public string Comment => Main?.Comment ?? string.Empty;

        public string Name { get; private set; }

        public DataType DataType { get; private set; }

        public int ElementCount { get; }

        public FunctionParameterDeclaration(
            Token nameIdentifier,
            DataType dataType,
            int elementCount,
            int end) :
            base(new SyntaxNodeList().Add(dataType).Add(nameIdentifier))
        {
            Name = nameIdentifier.Lexeme;
            DataType = dataType;
            ElementCount = elementCount;
        }

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitFunctionParameterDeclaration(this);

        public override string ToString() => $"FunctionParameterDeclaration `{Name}` {ToFileRangeString()}";
    }
}
