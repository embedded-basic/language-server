using System.Collections.Generic;
using System.Collections.ObjectModel;
using EmbeddedBasic.Util;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes
{
    public sealed class SyntaxNodeList : SortedArrayList<SyntaxNode>
    {
        public class ReadOnly : ReadOnlyCollection<SyntaxNode>
        {
            public ReadOnly(SyntaxNodeList nodeList) : base(nodeList) { }
        }

        private ReadOnly? _readOnly = null;

        public new ReadOnly AsReadOnly()
        {
            if (_readOnly != null) return _readOnly;
            return _readOnly = new(this);
        }

        public SyntaxNodeList() : base() { }

        public new SyntaxNodeList Add(SyntaxNode? node)
        {
            // No duplicates allowed!
            if (node != null) base.Add(node);
            return this;
        }

        public SyntaxNodeList AddRange(IReadOnlyList<SyntaxNode?>? nodes)
        {
            if (nodes != null && nodes?.Count > 0)
            {
                // We assure that the first non-null node is not yet in the list
                // and assume that covers the others too. No duplicates allowed!
                int index = 0;
                SyntaxNode? node = null;

                while (index < nodes.Count)
                {
                    node = nodes[index++];
                    if (node != null) break;
                }

                if (node != null)
                {
                    base.Add(node);

                    while (index < nodes.Count)
                    {
                        node = nodes[index++];
                        if (node != null) base.Add(node);
                    }
                }
            }
            return this;
        }

        public new bool Contains(SyntaxNode node)
        {
            return base.Contains(node);
        }
    }
}
