namespace EmbeddedBasic.LanguageServer.Syntax.Nodes
{
    /// <summary>
    /// A declaration of an identifier that represents an array (constant or variable).
    /// </summary>
    public interface IArrayDeclaration : IDeclaration
    {
        /// <summary>
        /// The number of elements in the array. When less that two its not an array.
        /// </summary>
        int ElementCount { get; }
    }
}
