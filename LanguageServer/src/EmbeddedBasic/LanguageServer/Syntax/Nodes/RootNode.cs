using System.Collections.Generic;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes
{
    public sealed class RootNode : SyntaxNode
    {
        public IReadOnlyList<TopLevelNode> TopLevelNodes { get; }

        public RootNode(SourceFile sourceFile, IReadOnlyList<TopLevelNode> topLevelNodes) :
            base(null!, sourceFile)
        {
            TopLevelNodes = topLevelNodes;
        }

        public void SetEnd(SyntaxTree tree, int endPosition)
        {
            Tree = tree;
            TextSpan = TextSpan.WithEnd(endPosition);
            _childNodes.AddRange(TopLevelNodes);
            RegisterChildNodes();
            // tree.Finish();
        }

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitRoot(this);

        public override string ToString() => $"RootNode (\"{FileName}\")";
    }
}
