namespace EmbeddedBasic.LanguageServer.Syntax.Nodes
{
    public abstract class CommentableNode : SyntaxNode
    {
        public virtual string Comment { get; }

        protected CommentableNode(string comment, SyntaxNodeList childNodes) : base(childNodes)
        {
            Comment = comment;
        }

        protected CommentableNode(string comment, Token token) : base(token)
        {
            Comment = comment;
        }
    }
}
