namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions
{
    public enum BinaryOperatorKind
    {
        /// <summary>
        /// None
        /// </summary>
        None,
        /// <summary>
        /// Addition
        /// </summary>
        Add,
        /// <summary>
        /// Subtraction
        /// </summary>
        Subtract,
        /// <summary>
        /// Multiplication
        /// </summary>
        Multiply,
        /// <summary>
        /// Division
        /// </summary>
        Divide,
        /// <summary>
        /// Modulo (division rest)
        /// </summary>
        Modulo,
        /// <summary>
        /// Left shift (bitwise)
        /// </summary>
        ShiftLeft,
        /// <summary>
        /// Right shift (bitwise)
        /// </summary>
        ShiftRight,
        /// <summary>
        /// Logical/bitwise conjunction
        /// </summary>
        And,
        /// <summary>
        /// Boolean/bitwise disjunction
        /// </summary>
        Or,
        /// <summary>
        /// Boolean/bitwise exclusive disjunction
        /// </summary>
        Xor,
        /// <summary>
        /// Comparison: less than
        /// </summary>
        LessThan,
        /// <summary>
        /// Comparison: less than or equals
        /// </summary>
        LessThanOrEqualTo,
        /// <summary>
        /// Comparison: greater than
        /// </summary>
        GreaterThan,
        /// <summary>
        /// Comparison: greater than or equals
        /// </summary>
        GreaterThanOrEqualTo,
        /// <summary>
        /// Comparison: not equals
        /// </summary>
        NotEqualTo,
        /// <summary>
        /// Comparison: equals
        /// </summary>
        EqualTo
    }

    /// <summary>
    /// A binary infix expression like `2 + 3`.
    /// </summary>
    public class BinaryExpression : OperationExpression
    {
        public readonly BinaryOperatorKind Operator;

        public readonly Expression Operand1;

        public readonly Expression Operand2;

        public override OperatorPrecedence Precedence => OperatorToPrecedence(Operator);

        public BinaryExpression(Expression operand1, Token otor, Expression operand2) :
            base(new SyntaxNodeList().Add(operand1).Add(otor).Add(operand2))
        {
            Operator = TokenToOperator(otor);
            Operand1 = operand1;
            Operand2 = operand2;
        }

        private static BinaryOperatorKind TokenToOperator(Token token) => token.Kind switch
        {
            TokenKind.PlusSymbol => BinaryOperatorKind.Add,
            TokenKind.MinusSymbol => BinaryOperatorKind.Subtract,
            TokenKind.AsteriskSymbol => BinaryOperatorKind.Multiply,
            TokenKind.SlashSymbol => BinaryOperatorKind.Divide,
            TokenKind.ModKeyword => BinaryOperatorKind.Modulo,
            TokenKind.LessThanLessThanSymbol => BinaryOperatorKind.ShiftLeft,
            TokenKind.GreaterThanGreaterThanSymbol => BinaryOperatorKind.ShiftRight,
            TokenKind.AndKeyword => BinaryOperatorKind.And,
            TokenKind.OrKeyword => BinaryOperatorKind.Or,
            TokenKind.XorKeyword => BinaryOperatorKind.Xor,
            TokenKind.LessThanSymbol => BinaryOperatorKind.LessThan,
            TokenKind.LessThanOrEqualSymbol => BinaryOperatorKind.LessThanOrEqualTo,
            TokenKind.GreaterThanSymbol => BinaryOperatorKind.GreaterThan,
            TokenKind.GreaterThanOrEqualSymbol => BinaryOperatorKind.GreaterThanOrEqualTo,
            TokenKind.LessThanGreaterThanSymbol => BinaryOperatorKind.NotEqualTo,
            TokenKind.EqualSymbol => BinaryOperatorKind.EqualTo,
            _ => BinaryOperatorKind.None
        };

        private static OperatorPrecedence OperatorToPrecedence(BinaryOperatorKind kind) => kind switch
        {
            BinaryOperatorKind.Add => OperatorPrecedence.Sum,
            BinaryOperatorKind.Subtract => OperatorPrecedence.Sum,
            BinaryOperatorKind.Multiply => OperatorPrecedence.Product,
            BinaryOperatorKind.Divide => OperatorPrecedence.Product,
            BinaryOperatorKind.Modulo => OperatorPrecedence.Product,
            BinaryOperatorKind.ShiftLeft => OperatorPrecedence.Product,
            BinaryOperatorKind.ShiftRight => OperatorPrecedence.Product,
            BinaryOperatorKind.And => OperatorPrecedence.Product,
            BinaryOperatorKind.Or => OperatorPrecedence.Sum,
            BinaryOperatorKind.Xor => OperatorPrecedence.Sum,
            BinaryOperatorKind.LessThan => OperatorPrecedence.Comparison,
            BinaryOperatorKind.LessThanOrEqualTo => OperatorPrecedence.Comparison,
            BinaryOperatorKind.GreaterThan => OperatorPrecedence.Comparison,
            BinaryOperatorKind.GreaterThanOrEqualTo => OperatorPrecedence.Comparison,
            BinaryOperatorKind.NotEqualTo => OperatorPrecedence.Comparison,
            BinaryOperatorKind.EqualTo => OperatorPrecedence.Comparison,
            _ => OperatorPrecedence.None
        };

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitBinaryExpression(this);

        public override string ToString() => $"BinaryExpression ({Operator})";
    }
}
