namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions
{
    public enum PostfixOperatorKind
    {
        /// <summary>
        /// None
        /// </summary>
        None,
        /// <summary>
        /// Exponentiation
        /// </summary>
        Exponentiate
    }

    /// <summary>
    /// A unary postfix expression like `5!`.
    /// </summary>
    public class PostfixExpression : OperationExpression
    {
        public readonly PostfixOperatorKind Operator;
        public readonly Expression Operand;

        public override OperatorPrecedence Precedence => OperatorPrecedence.Postfix;

        public PostfixExpression(Token operatorToken, Expression operand) :
            base(new SyntaxNodeList().Add(operatorToken).Add(operand))
        {
            Operator = TokenToOperator(operatorToken);
            Operand = operand;
        }

        private static PostfixOperatorKind TokenToOperator(Token token) => token.Kind switch
        {
            TokenKind.CaretSymbol => PostfixOperatorKind.Exponentiate,
            _ => PostfixOperatorKind.None,
        };

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitPostfixExpression(this);

        public override string ToString() => $"PostfixExpression ({Operator})";
    }
}
