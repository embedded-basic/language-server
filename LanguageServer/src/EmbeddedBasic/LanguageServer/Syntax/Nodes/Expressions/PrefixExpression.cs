namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions
{
    public enum PrefixOperatorKind
    {
        /// <summary>
        /// None
        /// </summary>
        None,
        /// <summary>
        /// None
        /// </summary>
        Positive,
        /// <summary>
        /// Additive inverse (two's complement)
        /// </summary>
        NegateNumber,
        /// <summary>
        /// Logical complement
        /// </summary>
        NegateLogical,
        /// <summary>
        /// Bitwise complement (one's complement)
        /// </summary>
        NegateBitwise
    }

    /// <summary>
    /// A unary prefix expression like `-3`.
    /// </summary>
    public class PrefixExpression : OperationExpression
    {
        public readonly PrefixOperatorKind Operator;
        public readonly Expression Operand;

        public override OperatorPrecedence Precedence => OperatorPrecedence.Prefix;

        public PrefixExpression(Token operatorToken, Expression operand) :
            base(new SyntaxNodeList().Add(operatorToken).Add(operand))
        {
            Operator = TokenToOperator(operatorToken);
            Operand = operand;
        }

        private static PrefixOperatorKind TokenToOperator(Token token) => token.Kind switch
        {
            TokenKind.PlusSymbol => PrefixOperatorKind.Positive,
            TokenKind.MinusSymbol => PrefixOperatorKind.NegateNumber,
            TokenKind.NotKeyword => PrefixOperatorKind.NegateLogical,
            TokenKind.NegKeyword => PrefixOperatorKind.NegateBitwise,
            _ => PrefixOperatorKind.None,
        };

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitPrefixExpression(this);

        public override string ToString() => $"PrefixExpression `{Operator}` ({ToRangeString()})";
    }
}
