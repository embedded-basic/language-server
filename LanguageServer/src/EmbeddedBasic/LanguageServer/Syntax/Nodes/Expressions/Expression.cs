namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions
{
    public abstract class Expression : SyntaxNode
    {
        /// <summary>
        /// The operator precedence for this expression.
        /// As default we assume the highest precedence.
        /// </summary>
        public virtual OperatorPrecedence Precedence => OperatorPrecedence.Call;

        protected Expression(SyntaxNodeList childNodes) :
            base(childNodes)
        {
        }

        protected Expression(Token token) : base(token) { }

        /// <summary>
        /// Does the expression need grouping when used in the given expression?
        /// </summary>
        /// <param name="outerExpression">The expression containing this expression.</param>
        /// <returns>Does the expression need grouping?</returns>
        public bool NeedsGrouping(Expression outerExpression) => outerExpression.Precedence > Precedence;
    }
}
