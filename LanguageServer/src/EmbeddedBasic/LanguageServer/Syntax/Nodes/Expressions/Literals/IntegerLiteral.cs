using System.Diagnostics;
using EmbeddedBasic.Util;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Parselets;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions.Literals
{
    /// <summary>
    /// An integer literal.
    /// </summary>
    public class IntegerLiteral : Literal
    {
        public readonly int IntValue;

        public override object Value => IntValue;

        public IntegerLiteral(Token value) :
            base(value)
        {
            Debug.Assert(value.Kind == TokenKind.IntegerLiteral, "`TokenKind.IntegerLiteral` was expected");
            IntValue = value.Lexeme.ParseInt32();
        }

        public static readonly LiteralParselet.Info ParseletInfo =
            new(value => new IntegerLiteral(value), TokenKind.IntegerLiteral);

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitIntegerLiteral(this);

        public string ToCodeString() => IntValue.ToString();

        public override string ToString() => $"IntegerLiteral: \"{IntValue}\" ({ToRangeString()})";
    }
}
