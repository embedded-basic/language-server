using System.Diagnostics;
using System.Text;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Parselets;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions.Literals
{
    /// <summary>
    /// A string (character array) literal.
    /// </summary>
    public class StringLiteral : Literal
    {
        public readonly string StringValue;

        public override object Value => StringValue;

        public StringLiteral(Token stringToken) :
            base(stringToken)
        {
            Debug.Assert(stringToken.Kind == TokenKind.StringLiteral, "`TokenKind.StringLiteral` was expected");
            StringValue = stringToken.LexemeAsString();
        }

        private static string GetUnescaped(string str) => str.Substring(1, str.Length - 2);

        private static string GetEscaped(string str)
        {
            var builder = new StringBuilder(str);
            builder.Replace("\n", "\\n");
            builder.Replace("\r", "\\r");
            return builder.ToString();
        }

        public static readonly LiteralParselet.Info ParseletInfo =
            new(value => new StringLiteral(value), TokenKind.StringLiteral);

        public string GetEscapedStr() => GetEscaped(StringValue);

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitStringLiteral(this);

        public override string ToString() => $"StringLiteral (\"{StringValue}\")";
    }
}
