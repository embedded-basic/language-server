using System.Diagnostics;
using EmbeddedBasic.Util;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Parselets;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions.Literals
{
    /// <summary>
    /// A device id literal.
    /// </summary>
    public class DeviceIdLiteral : Literal
    {
        public readonly int DeviceId;

        public override object Value => $"#{DeviceId}";

        public DeviceIdLiteral(Token stringToken) :
            base(stringToken)
        {
            Debug.Assert(stringToken.Kind == TokenKind.DeviceIdLiteral, "`TokenKind.DeviceIdLiteral` was expected");
            DeviceId = stringToken.Lexeme.Substring(1).ParseInt32();
        }

        public static readonly LiteralParselet.Info ParseletInfo =
            new(value => new DeviceIdLiteral(value), TokenKind.DeviceIdLiteral);

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitDeviceIdLiteral(this);

        public override string ToString() => $"DeviceIdLiteral (\"{DeviceId}\")";
    }
}
