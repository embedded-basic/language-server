using System.Diagnostics;
using EmbeddedBasic.Util;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Parselets;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions.Literals
{
    /// <summary>
    /// A decimal literal.
    /// </summary>
    public class DecimalLiteral : Literal
    {
        public readonly float FloatValue;

        public override object Value => FloatValue;

        public DecimalLiteral(Token value) :
            base(value)
        {
            Debug.Assert(value.Kind == TokenKind.DecimalLiteral, "`TokenKind.DecimalLiteral` was expected");
            FloatValue = value.Lexeme.ParseFloat();
        }

        public static readonly LiteralParselet.Info ParseletInfo =
            new(value => new DecimalLiteral(value), TokenKind.DecimalLiteral);

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitDecimalLiteral(this);

        public string ToCodeString() => FloatValue.ToCode();

        public override string ToString() => $"DecimalLiteral: \"{ToCodeString()}\" ({ToRangeString()})";
    }
}
