using System.Diagnostics;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Parselets;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions.Literals
{
    /// <summary>
    /// A device name literal.
    /// </summary>
    public class DeviceNameLiteral : Literal
    {
        public readonly string DeviceName;

        public override object Value => $"#{DeviceName}";

        public DeviceNameLiteral(Token stringToken) :
            base(stringToken)
        {
            Debug.Assert(stringToken.Kind == TokenKind.DeviceNameLiteral, "`TokenKind.DeviceNameLiteral` was expected");
            DeviceName = stringToken.Lexeme.Substring(1);
        }

        public static readonly LiteralParselet.Info ParseletInfo =
            new(value => new DeviceNameLiteral(value), TokenKind.DeviceNameLiteral);

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitDeviceNameLiteral(this);

        public override string ToString() => $"DeviceNameLiteral (\"{DeviceName}\")";
    }
}
