using System.Diagnostics;
using EmbeddedBasic.Util;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Parselets;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions.Literals
{
    /// <summary>
    /// A character literal.
    /// </summary>
    public class CharLiteral : Literal
    {
        public readonly char CharValue;

        public override object Value => CharValue;

        public CharLiteral(Token value) :
            base(value)
        {
            Debug.Assert(value.Kind == TokenKind.CharLiteral, "`TokenKind.CharLiteral` was expected");
            CharValue = value.Lexeme.ParseChar();
        }

        public static readonly LiteralParselet.Info ParseletInfo =
            new(value => new CharLiteral(value), TokenKind.CharLiteral);

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitCharLiteral(this);

        public override string ToString() => $"CharLiteral: \"{CharValue}\" ({ToRangeString()})";
    }
}
