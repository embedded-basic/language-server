using System;
using System.Diagnostics;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Parselets;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions.Literals
{
    /// <summary>
    /// A hexadecimal number literal.
    /// </summary>
    public class HexNumberLiteral : BinNumberLiteral
    {
        public HexNumberLiteral(Token value) :
            base(value, FromCodeString(value.Lexeme.Substring(1)))
        {
            Debug.Assert(value.Kind == TokenKind.HexNumberLiteral, "`TokenKind.HexNumberLiteral` was expected");
        }

        public static readonly new LiteralParselet.Info ParseletInfo =
            new(value => new HexNumberLiteral(value), TokenKind.HexNumberLiteral);

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitHexNumberLiteral(this);

        private static int FromCodeString(string number) => Convert.ToInt32(number, 16);

        public override string ToCodeString() => "$" + Convert.ToString(IntValue, 16);

        public override string ToString() => $"HexNumberLiteral: \"{ToCodeString()}\" ({ToRangeString()})";
    }
}
