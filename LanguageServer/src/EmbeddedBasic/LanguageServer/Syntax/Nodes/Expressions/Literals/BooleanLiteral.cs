using EmbeddedBasic.Util;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Parselets;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions.Literals
{
    /// <summary>
    /// A boolean literal.
    /// </summary>
    public class BooleanLiteral : Literal
    {
        public readonly bool BoolValue;

        public override object Value => BoolValue;

        public BooleanLiteral(Token value) :
            base(value)
        {
            BoolValue = value.Lexeme.ParseBool();
        }

        public static readonly LiteralParselet.Info ParseletInfo =
            new(value => new BooleanLiteral(value), TokenKind.TrueKeyword, TokenKind.FalseKeyword);

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitBooleanLiteral(this);

        public string ToCodeString() => BoolValue ? "true" : "false";

        public override string ToString() => $"BooleanLiteral: \"{ToCodeString()}\" ({ToRangeString()})";
    }
}
