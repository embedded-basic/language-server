using System;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Parselets;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions.Literals
{
    /// <summary>
    /// A single bit literal.
    /// </summary>
    public class BitLiteral : Literal
    {
        public readonly bool BoolValue;

        public override object Value => BoolValue;

        public BitLiteral(Token value) :
            base(value)
        {
            BoolValue = FromCodeString(value.Lexeme);
        }

        public static readonly LiteralParselet.Info ParseletInfo =
            new(value => new BitLiteral(value), TokenKind.HighKeyword, TokenKind.LowKeyword);

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitBitLiteral(this);

        private static bool FromCodeString(string bit) => bit switch
        {
            "high" => true,
            "low" => false,
            _ => throw new Exception("Single bit string not \"high\" nor \"low\"")
        };

        public virtual string ToCodeString() => BoolValue ? "high" : "low";

        public override string ToString() => $"BitLiteral: \"{ToCodeString()}\" ({ToRangeString()})";
    }
}
