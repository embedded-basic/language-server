using System;
using System.Diagnostics;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Parselets;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions.Literals
{
    /// <summary>
    /// A date literal.
    /// </summary>
    public class DateLiteral : Literal
    {
        public const string FormatString = "dd.MM.yyyy";

        /// <summary>
        /// A date timestamp
        /// </summary>
        public readonly DateTime DateValue;

        public override object Value => ToCodeString();

        public DateLiteral(Token value) :
            base(value)
        {
            Debug.Assert(value.Kind == TokenKind.DateLiteral, "`TokenKind.DateLiteral` was expected");
            DateValue = DateTime.ParseExact(value.Lexeme, FormatString, null);
        }

        public static readonly LiteralParselet.Info ParseletInfo =
            new(value => new DateLiteral(value), TokenKind.DateLiteral);

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitDateLiteral(this);

        public string ToCodeString() => DateValue.ToString(FormatString);

        public override string ToString() => $"DateLiteral: \"{ToCodeString()}\" ({ToRangeString()})";
    }
}
