using System;
using System.Diagnostics;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Parselets;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions.Literals
{
    /// <summary>
    /// A time literal.
    /// </summary>
    public class TimeLiteral : Literal
    {
        public const string FormatString = @"hh\:mm\:ss";

        public readonly TimeSpan TimeValue;

        public override object Value => TimeValue;

        public TimeLiteral(Token value) :
            base(value)
        {
            Debug.Assert(value.Kind == TokenKind.TimeLiteral, "`TokenKind.TimeLiteral` was expected");
            TimeValue = TimeSpan.ParseExact(value.Lexeme, FormatString, null);
        }

        public static readonly LiteralParselet.Info ParseletInfo =
            new(value => new TimeLiteral(value), TokenKind.TimeLiteral);

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitTimeLiteral(this);

        public string ToCodeString() => TimeValue.ToString(FormatString);

        public override string ToString() => $"TimeLiteral: \"{TimeValue}\" ({ToRangeString()})";
    }
}
