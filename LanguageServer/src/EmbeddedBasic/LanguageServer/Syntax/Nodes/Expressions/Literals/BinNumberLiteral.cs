using System;
using System.Diagnostics;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Parselets;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions.Literals
{
    /// <summary>
    /// A binary number literal.
    /// </summary>
    public class BinNumberLiteral : Literal
    {
        public readonly int IntValue;

        public override object Value => IntValue;

        protected BinNumberLiteral(Token value, int intValue) :
            base(value)
        {
            IntValue = intValue;
        }

        public static readonly LiteralParselet.Info ParseletInfo =
            new(value => new BinNumberLiteral(value), TokenKind.BinNumberLiteral);

        public BinNumberLiteral(Token value) :
            this(value, FromCodeString(value.Lexeme.Substring(1)))
        {
            Debug.Assert(value.Kind == TokenKind.BinNumberLiteral, "`TokenKind.BinNumberLiteral` was expected");
        }

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitBinNumberLiteral(this);

        private static int FromCodeString(string number) => Convert.ToInt32(number, 2);

        public virtual string ToCodeString() => "%" + Convert.ToString(IntValue, 2);

        public override string ToString() => $"BinNumberLiteral: \"{ToCodeString()}\" ({ToRangeString()})";
    }
}
