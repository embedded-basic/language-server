namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions.Literals
{
    /// <summary>
    /// A literal expression.
    /// </summary>
    public abstract class Literal : Expression
    {
        public abstract object Value { get; }

        protected Literal(Token token) : base(token) { }
    }
}
