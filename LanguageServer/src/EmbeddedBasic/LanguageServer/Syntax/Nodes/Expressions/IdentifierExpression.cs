using System.Collections.Generic;
using System.Linq;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions
{
    /// <summary>
    /// The name of a function, variable or similar.
    /// </summary>
    public sealed class IdentifierExpression : Expression
    {
        public readonly IReadOnlyList<string> Names;

        public string FullName => string.Join(".", Names);

        private IdentifierExpression(Token startToken, IReadOnlyList<Token> identifiers) :
            base(new SyntaxNodeList().Add(startToken).AddRange(identifiers))
        {
            var names = new string[identifiers.Count];

            for (int i = 0; i < identifiers.Count; i++)
            {
                names[i] = identifiers[i].Lexeme;
            }

            Names = names;
        }

        public IdentifierExpression(IReadOnlyList<Token> identifiers) : this(identifiers.First(), identifiers) { }

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitIdentifierExpression(this);

        public override string ToString() => $"IdentifierExpression `{FullName}` {ToFileRangeString()}";
    }
}
