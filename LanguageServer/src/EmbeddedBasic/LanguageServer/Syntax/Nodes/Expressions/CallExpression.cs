using System.Collections.Generic;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions
{
    public class CallExpression : Expression
    {
        public readonly IdentifierExpression Name;

        public readonly IReadOnlyList<Expression> Arguments;

        public CallExpression(IdentifierExpression name, Token lastToken, IReadOnlyList<Expression> arguments) :
            base(new SyntaxNodeList().Add(name).AddRange(arguments).Add(lastToken))
        {
            Name = name;
            Arguments = arguments;
        }

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitCallExpression(this);

        public override string ToString() => $"CallExpression ({Name.FullName})";
    }
}
