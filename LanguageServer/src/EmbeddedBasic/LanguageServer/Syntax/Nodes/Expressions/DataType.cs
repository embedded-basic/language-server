namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions
{
    public enum DataTypeKind
    {
        Void,
        Bool,
        Char,
        Byte,
        Word,
        Short,
        Int,
        Float
    }

    public sealed class DataType : SyntaxNode
    {
        public readonly DataTypeKind Kind;

        private DataType(DataTypeKind kind, Token token) :
            base(token)
        {
            Kind = kind;
        }

        public static DataType? CreateNonVoid(Token dataType)
        {
            switch (dataType.Kind)
            {
                case TokenKind.BoolKeyword: return new DataType(DataTypeKind.Bool, dataType);
                case TokenKind.ByteKeyword: return new DataType(DataTypeKind.Byte, dataType);
                case TokenKind.CharKeyword: return new DataType(DataTypeKind.Char, dataType);
                case TokenKind.FloatKeyword: return new DataType(DataTypeKind.Float, dataType);
                case TokenKind.IntKeyword: return new DataType(DataTypeKind.Int, dataType);
                case TokenKind.ShortKeyword: return new DataType(DataTypeKind.Short, dataType);
                case TokenKind.WordKeyword: return new DataType(DataTypeKind.Word, dataType);
                default: break;
            };
            return null;
        }

        public static DataType CreateVoid(Token token) => new DataType(DataTypeKind.Void, token);

        public static DataType Create(Token token)
        {
            var dataType = CreateNonVoid(token);
            if (dataType == null)
            {
                dataType = new(DataTypeKind.Void, token);
            }
            return dataType;
        }

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitDataType(this);

        public override string ToString() => $"DataType ({Kind})";
    }
}
