using EmbeddedBasic.Util;

namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions
{
    public class ArrayIndexingExpression : Expression
    {
        public readonly IdentifierExpression Name;

        public readonly Expression Index;

        public ArrayIndexingExpression(IdentifierExpression name, SyntaxNode lastNode, Expression index) :
            base(new SyntaxNodeList().Add(name).Add(index).Add(lastNode))
        {
            Name = name;
            Index = index;
        }

        protected internal override void Accept(SyntaxNodeVisitor visitor) => visitor.VisitArrayIndexingExpression(this);

        public override string ToString() => $"ArrayIndexingExpression ({Name.FullName})";
    }
}
