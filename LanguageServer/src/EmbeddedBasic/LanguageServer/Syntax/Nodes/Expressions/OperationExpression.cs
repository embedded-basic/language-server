namespace EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions
{
    public abstract class OperationExpression : Expression
    {
        protected OperationExpression(SyntaxNodeList childNodes) :
            base(childNodes)
        {
        }

        protected OperationExpression(Token token) : base(token) { }
    }
}
