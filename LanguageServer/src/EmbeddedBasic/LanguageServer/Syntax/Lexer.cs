using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using EmbeddedBasic.LanguageServer.Syntax.Nodes;

namespace EmbeddedBasic.LanguageServer.Syntax
{
    public abstract partial class Lexer : ITokenProvider
    {
        public SyntaxTree SyntaxTree { get; set; } = null!;

        private readonly Dictionary<int, Dictionary<string, TokenKind>> _keywordsByLength =
            new Dictionary<int, Dictionary<string, TokenKind>>();

        public SourceFile CurrentSourceFile => _currentSourceState?.SourceFile!;

        protected SourceFile.State _currentSourceState;

        private Token _currentToken;

        public Token Current => _currentToken;

        object IEnumerator.Current => _currentToken;

        private bool _lastTokenWasLinebreak = false;

        private StringBuilder _comment = new();

        protected Lexer()
        {
            _currentSourceState = null!;
            _currentToken = null!;
        }

        protected void AddKeyword(string keyword, TokenKind kind)
        {
            if (!_keywordsByLength.TryGetValue(keyword.Length, out var keywords))
            {
                keywords = new();
                _keywordsByLength[keyword.Length] = keywords;
            }
            keywords.Add(keyword, kind);
        }

        public void Reset() => _currentSourceState.Reset();

        /// <summary>
        /// Reset the token provider for processing of the given source.
        /// </summary>
        /// <param name="sourceFile">The location of the source to use.</param>
        /// <returns>Was the reset successful?</returns>
        [MemberNotNullWhen(true, "_currentSourceState")]
        public bool Reset(SourceFile sourceFile)
        {
            if (!sourceFile.FileExists) return false;
            _currentSourceState = new(SyntaxTree, sourceFile);
            return true;
        }

        /// <summary>
        /// Include the given source for processing.
        /// </summary>
        /// <param name="sourceFile">The source file to use.</param>
        /// <returns>Was the reset successful?</returns>
        public bool IncludeSource(SourceFile sourceFile)
        {
            if (!sourceFile.FileExists) return false;
            _currentSourceState = new(SyntaxTree, sourceFile, _currentSourceState);
            return true;
        }

        public void Dispose() { }

        /// <summary>
        /// Append the currently accumulated comment.
        /// </summary>
        /// <param name="length">The length of text from the source to append the comment.</param>
        protected void AppendComment(int length)
        {
            var comment = GetLexeme(length).TrimStart('/', ' ');
            if (!string.IsNullOrWhiteSpace(comment))
            {
                if (_comment.Length > 0) _comment.AppendLine();
                _comment.Append(comment);
            }
            _lastTokenWasLinebreak = false;
            // Comment tokens must all be discarded.
            _currentSourceState.Discard(length);
        }

        /// <summary>
        /// Reset the currently accumulated comment.
        /// </summary>
        /// <param name="length">The length of text from the source to append the comment.</param>
        protected void ResetComment()
        {
            if (_lastTokenWasLinebreak) _comment.Clear();
            _lastTokenWasLinebreak = true;
        }

        /// <summary>
        /// Consume the currently accumulated comment.
        /// </summary>
        /// <returns>The consumed comment.</returns>
        public string ConsumeComment()
        {
            var comment = _comment.ToString();
            _comment.Clear();
            return comment;
        }

        protected void Discard(int length = 1) => _currentSourceState.Discard(length);

        protected void Discard(int length, string errorMessage)
        {
            _currentToken = _currentSourceState.Set(TokenKind.Error, length, GetLexeme(length));
            if (string.IsNullOrEmpty(_currentToken.Lexeme)) NoteError(_currentToken, errorMessage, length);
            else NoteError(_currentToken, errorMessage, length, $"\"{_currentToken.Lexeme}\"");
        }

        protected void Set(TokenKind kind, int length, string lexeme)
        {
            _lastTokenWasLinebreak = false;
            _currentToken = _currentSourceState.Set(kind, length, lexeme);
        }

        protected void Set(TokenKind kind, int length = 1)
        {
            _lastTokenWasLinebreak = false;
            _currentToken = _currentSourceState.Set(kind, length, _currentSourceState.GetLexeme(length));
        }

        protected void DiscardLinebreak(int length = 1) => _currentSourceState.DiscardLinebreak(length);

        protected void SetLinebreak(TokenKind kind, int length, string lexeme)
        {
            ResetComment();
            _currentToken = _currentSourceState.SetLinebreak(kind, length, lexeme);
        }

        protected void SetLinebreak(TokenKind kind, int length = 1)
        {
            ResetComment();
            _currentToken = _currentSourceState.SetLinebreak(kind, length, _currentSourceState.GetLexeme(length));
        }

        protected bool TrySetLinebreak(char ch, bool allowLinebreak = true)
        {
            // Carriage-Return/[Newline] ---------------------------- //
            if (ch == '\r')
            {
                int offset = 1;

                if (PeekNextChar(out ch, offset) && ch == '\n') offset++;

                if (allowLinebreak) SetLinebreak(TokenKind.Linebreak, offset);
                else Discard(offset);
                return true;
            }
            // Newline ---------------------------------------------- //
            else if (ch == '\n')
            {
                if (allowLinebreak) SetLinebreak(TokenKind.Linebreak);
                else Discard();
                return true;
            }
            // -------------------------------------------------- //

            return false;
        }

        protected string GetLexeme(int length) => _currentSourceState.GetLexeme(length);

        protected void NoteError(SyntaxNode node, string message, int endOffset = 1)
            => SyntaxTree.AddErrorMessage(node, message);

        protected void NoteError(SyntaxNode node, string message, int endOffset, string lexeme)
            => SyntaxTree.AddErrorMessageFormat(node, message, lexeme);

        protected bool TrySetKeyword(int offset, out string lexeme)
        {
            lexeme = GetLexeme(offset);

            if (_keywordsByLength.TryGetValue(lexeme.Length, out var keywords) &&
                keywords.TryGetValue(lexeme, out var keyword))
            {
                Set(keyword, offset, lexeme);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Peeks the next char.
        /// </summary>
        /// <param name="ch">The char to find.</param>
        /// <param name="offset">The position offset of the char to find.</param>
        /// <returns></returns>
        protected bool PeekNextChar(out char ch, int offset = 0)
        {
            int index = _currentSourceState.Absolute + offset;

            if (index < _currentSourceState.Source.Length)
            {
                ch = _currentSourceState.Source[index];
                return true;
            }

            ch = default;
            return false;
        }

        /// <summary>
        /// Seeks the next char. It will count as an error if there is no next char.
        /// </summary>
        /// <param name="ch">The char to find.</param>
        /// <param name="offset">The position offset of the char to find.</param>
        /// <returns></returns>
        protected bool SeekNextChar(out char ch, int offset = 0)
        {
            if (PeekNextChar(out ch, offset)) return true;
            if (_currentSourceState.OuterState == null)
            {
                Set(TokenKind.Eof, 0);
                NoteError(Current, ErrorMessages.EndOfFile, offset);
            }
            return false;
        }

        /// <summary>
        /// Has the end of the source file been reached?
        /// </summary>
        /// <returns>Has the end of the source file been reached?</returns>
        protected bool IsEndOfFile()
        {
            for (; ; _currentSourceState = _currentSourceState.OuterState)
            {
                if (_currentSourceState.Absolute < _currentSourceState.Source.Length) return false;
                if (_currentSourceState.OuterState == null) break;
            }

            return true;
        }

        /// <summary>
        /// Move to the next token.
        /// </summary>
        /// <returns>Could a token be found?</returns>
        [MemberNotNullWhen(true, "_currentToken")]
        public abstract bool MoveNext();
    }
}
