using System.Collections.Generic;
using EmbeddedBasic.LanguageServer.Syntax.Nodes;

namespace EmbeddedBasic.LanguageServer.Syntax
{
    /// <summary>
    /// A provider of tokens from a source text file.
    /// </summary>
    public interface ITokenProvider : IEnumerator<Token>
    {
        /// <summary>
        /// The syntax tree to build.
        /// </summary>
        SyntaxTree SyntaxTree { get; set; }

        /// <summary>
        /// The current source file.
        /// </summary>
        SourceFile CurrentSourceFile { get; }

        /// <summary>
        /// Reset the token provider for processing of the given source.
        /// </summary>
        /// <param name="sourceFile">The source file to use.</param>
        /// <param name="source">The source to use.</param>
        /// <returns>Was the reset successful?</returns>
        bool Reset(SourceFile sourceFile);

        /// <summary>
        /// Include the given source for processing.
        /// </summary>
        /// <param name="sourceFile">The source file to use.</param>
        /// <returns>Was the reset successful?</returns>
        bool IncludeSource(SourceFile sourceFile);

        /// <summary>
        /// Consume the currently accumulated comment.
        /// </summary>
        /// <returns>The consumed comment.</returns>
        string ConsumeComment();
    }
}
