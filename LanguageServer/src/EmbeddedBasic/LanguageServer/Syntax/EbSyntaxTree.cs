using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using EmbeddedBasic.LanguageServer.Syntax.Nodes;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Directives;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Statements;

namespace EmbeddedBasic.LanguageServer.Syntax
{
    /// <summary>
    /// An `Embedded BASIC` syntax tree.
    /// </summary>
    public sealed class EbSyntaxTree : SyntaxTree
    {
        private readonly Dictionary<string, DefineDirective> _defines = new();

        public IReadOnlyCollection<DefineDirective> Defines => _defines.Values;

        public override IEnumerable<SyntaxNode> Symbols
        {
            get
            {
                foreach (var define in _defines.Values)
                {
                    yield return define;
                }
            }
        }

        public EbSyntaxTree(RootNode root) : base(root) { }

        /// <summary>
        /// Does a symbol with the given name exist?
        /// </summary>
        /// <param name="name">The name of the symbol to look for.</param>
        /// <returns>Does the symbol exist?</returns>
        public override bool SymbolExists(string name) => _defines.ContainsKey(name);

        /// <summary>
        /// Removes the symbol with the given name, if it exists.
        /// </summary>
        /// <param name="name">The name of the symbol to remove.</param>
        /// <returns>Did the symbol exist?</returns>
        public override bool RemoveSymbol(string name) => _defines.Remove(name);

        /// <summary>
        /// Add a symbol.
        /// </summary>
        /// <param name="node">The node that corresponds to a symbol.</param>
        public override void AddSymbol(SyntaxNode node)
        {
            switch (node)
            {
                case DefineDirective define: _defines.Add(define.Name, define); break;
            }
        }

        /// <summary>
        /// Try to get a symbol by its name.
        /// </summary>
        /// <param name="name">The name of the symbol to get.</param>
        /// <param name="symbol">The symbol or null.</param>
        /// <returns>Does a symbol with the given name exist?</returns>
        public override bool TryGetSymbol<N>(string name, [MaybeNullWhen(false)] out N symbol)
        {
            if (typeof(N) == typeof(DefineDirective) &&
                _defines.TryGetValue(name, out var macro))
            {
                symbol = (macro as N)!;
                return true;
            }
            symbol = null;
            return false;
        }
    }
}
