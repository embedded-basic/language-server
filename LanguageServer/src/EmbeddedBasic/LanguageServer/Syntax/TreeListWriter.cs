using System.IO;
using System.Text;
using System.Collections.Generic;
using EmbeddedBasic.Util;
using EmbeddedBasic.LanguageServer.Syntax.Nodes;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Directives;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions.Literals;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Statements;

namespace EmbeddedBasic.LanguageServer.Syntax
{
    public sealed class TreeListWriter : SyntaxNodeVisitor
    {
        private StringBuilder _output = new();//"> ");

        private int _indent = 0;

        public TreeListWriter() { }

        public string GetText(EbSyntaxTree tree)
        {
            this.Visit(tree.Root);
            return _output.ToString();
        }

        public void WriteFile(EbSyntaxTree tree, string dirpath)
        {
            this.Visit(tree.Root);
            var treeText = _output.ToString();
            var filepath = PathHelper.Join(dirpath, tree.SourceFile.FileName + ".tree");
            IO.EnsureDirectory(dirpath);
            File.WriteAllText(filepath, treeText, Encoding.UTF8);
        }

        private void WriteIndent()
        {
            for (int i = 0; i < _indent; i++)
            {
                // _output.Append("~-  ");
                _output.Append("\t");
            }
        }

        private void IndentWriteLine(SyntaxNode? node)
        {
            if (node != null)
            {
                WriteIndent();
                _output.AppendLine(node.ToString());
            }
        }

        private void Write(SyntaxNode? node) => IndentWriteLine(node);

        private void Write(SyntaxNode? node, IReadOnlyList<SyntaxNode?> subNodes)
        {
            IndentWriteLine(node);
            _indent++;
            this.Visit(subNodes);
            _indent--;
        }

        private void Write(SyntaxNode? node, params SyntaxNode?[] subNodes)
            => Write(node, (IReadOnlyList<SyntaxNode?>)subNodes);

        private void Write(SyntaxNode? node, SyntaxNode? subNode1, IReadOnlyList<SyntaxNode?> subNodes)
        {
            IndentWriteLine(node);
            _indent++;
            this.Visit(subNode1);
            this.Visit(subNodes);
            _indent--;
        }

        private void Write(SyntaxNode? node, SyntaxNode? subNode1, SyntaxNode? subNode2, IReadOnlyList<SyntaxNode?> subNodes)
        {
            IndentWriteLine(node);
            _indent++;
            this.Visit(subNode1);
            this.Visit(subNode2);
            this.Visit(subNodes);
            _indent--;
        }

        private void Write(SyntaxNode? node, IReadOnlyList<SyntaxNode?> subNodes, SyntaxNode? subNode)
        {
            IndentWriteLine(node);
            _indent++;
            this.Visit(subNodes);
            this.Visit(subNode);
            _indent--;
        }

        private void Write(SyntaxNode? node, IReadOnlyList<SyntaxNode?> subNodes1, IReadOnlyList<SyntaxNode?> subNodes2)
        {
            IndentWriteLine(node);
            _indent++;
            this.Visit(subNodes1);
            this.Visit(subNodes2);
            _indent--;
        }

        protected internal override void VisitDataType(DataType dataType) => Write(dataType);

        protected internal override void VisitBitLiteral(BitLiteral bitLiteral) => Write(bitLiteral);

        protected internal override void VisitBooleanLiteral(BooleanLiteral boolLiteral) => Write(boolLiteral);

        protected internal override void VisitBinNumberLiteral(BinNumberLiteral binNumberLiteral) => Write(binNumberLiteral);

        protected internal override void VisitHexNumberLiteral(HexNumberLiteral hexNumberLiteral) => Write(hexNumberLiteral);

        protected internal override void VisitIntegerLiteral(IntegerLiteral integerLiteral) => Write(integerLiteral);

        protected internal override void VisitDecimalLiteral(DecimalLiteral decimalLiteral) => Write(decimalLiteral);

        protected internal override void VisitDateLiteral(DateLiteral dateLiteral) => Write(dateLiteral);

        protected internal override void VisitTimeLiteral(TimeLiteral timeLiteral) => Write(timeLiteral);

        protected internal override void VisitCharLiteral(CharLiteral charLiteral) => Write(charLiteral);

        protected internal override void VisitStringLiteral(StringLiteral stringLiteral) => Write(stringLiteral);

        protected internal override void VisitDeviceNameLiteral(DeviceNameLiteral deviceNameLiteral) => Write(deviceNameLiteral);

        protected internal override void VisitDeviceIdLiteral(DeviceIdLiteral deviceIdLiteral) => Write(deviceIdLiteral);

        protected internal override void VisitIdentifierExpression(IdentifierExpression identifier) => Write(identifier);

        protected internal override void VisitPostfixExpression(PostfixExpression postfixExpression) => Write(postfixExpression);

        protected internal override void VisitPrefixExpression(PrefixExpression prefixExpression)
            => Write(prefixExpression, prefixExpression.Operand);

        protected internal override void VisitBinaryExpression(BinaryExpression binaryExpression)
            => Write(binaryExpression, binaryExpression.Operand1, binaryExpression.Operand2);

        protected internal override void VisitCallExpression(CallExpression callExpression)
            => Write(callExpression, callExpression.Arguments);

        protected internal override void VisitArrayIndexingExpression(ArrayIndexingExpression arrayIndexingExpression)
            => Write(arrayIndexingExpression, arrayIndexingExpression.Index);

        protected internal override void VisitVariableDeclaration(VariableDeclaration variableDeclaration)
            => Write(variableDeclaration, variableDeclaration.DataType, variableDeclaration.SubDeclarations);

        protected internal override void VisitVariableSubDeclaration(VariableSubDeclaration variableSubDeclaration)
            => Write(variableSubDeclaration, variableSubDeclaration.DataType, variableSubDeclaration.Value);

        protected internal override void VisitVariableArraySubDeclaration(VariableArraySubDeclaration variableArraySubDeclaration)
            => Write(variableArraySubDeclaration, variableArraySubDeclaration.DataType, variableArraySubDeclaration.Value);

        protected internal override void VisitConstantDeclaration(ConstantDeclaration constantDeclaration)
            => Write(constantDeclaration, constantDeclaration.Value);

        protected internal override void VisitImportDirective(ImportDirective importDirective) => Write(importDirective);

        protected internal override void VisitIncludeDirective(IncludeDirective includeDirective) => Write(includeDirective);

        protected internal override void VisitTargetDirective(TargetDirective targetDirective) => Write(targetDirective);

        protected internal override void VisitDefineDirective(DefineDirective defineDirective) => Write(defineDirective);

        protected internal override void VisitBlock(Block block) => Write(block, block.Statements);

        protected internal override void VisitLoop(Loop loop) => Write(loop, loop.Statements);

        protected internal override void VisitWhileLoop(WhileLoop whileLoop)
            => Write(whileLoop, whileLoop.Condition, whileLoop.Statements);

        protected internal override void VisitForLoop(ForLoop forLoop)
            => Write(forLoop, forLoop.Target, forLoop.Step, forLoop.Statements);

        protected internal override void VisitIfStatement(IfStatement ifStatement)
            => Write(ifStatement, ifStatement.IfBranches, ifStatement.ElseBranch);

        protected internal override void VisitIfBranch(IfBranch ifBranch)
            => Write(ifBranch, ifBranch.Condition, ifBranch.Statements);

        protected internal override void VisitSelectStatement(SelectStatement selectStatement)
            => Write(selectStatement, selectStatement.Cases, selectStatement.DefaultCase);

        protected internal override void VisitSelectDefaultCase(SelectDefaultCase defaultCase)
            => Write(defaultCase, defaultCase.Statements);

        protected internal override void VisitSelectSingleCase(SelectSingleCase singleCase)
            => Write(singleCase, singleCase.Condition, singleCase.Statements);

        protected internal override void VisitSelectListCase(SelectListCase listCase)
            => Write(listCase, listCase.Conditions, listCase.Statements);

        protected internal override void VisitSelectRangeCase(SelectRangeCase rangeCase)
            => Write(rangeCase, rangeCase.FromValue, rangeCase.ToValue, rangeCase.Statements);

        protected internal override void VisitLabelStatement(LabelStatement labelStatement)
            => Write(labelStatement);

        protected internal override void VisitReturnStatement(ReturnStatement returnStatement)
            => Write(returnStatement, returnStatement.Expression);

        protected internal override void VisitExitStatement(ExitStatement exitStatement) => Write(exitStatement);

        protected internal override void VisitGotoStatement(GotoStatement gotoStatement) => Write(gotoStatement);

        protected internal override void VisitWaitStatement(WaitStatement waitStatement)
            => Write(waitStatement, waitStatement.Condition, waitStatement.Timeout, waitStatement.JumpStatement);

        protected internal override void VisitOnGotoStatement(OnGotoStatement onGotoStatement) => Write(onGotoStatement);

        protected internal override void VisitOnFunctionCall(OnFunctionCall onFunctionCall) => Write(onFunctionCall);

        protected internal override void VisitFunctionCall(FunctionCall callStatement)
            => Write(callStatement, callStatement.Expression);

        protected internal override void VisitAssignmentStatement(AssignmentStatement assignment)
            => Write(assignment, assignment.Value);

        protected internal override void VisitCommandCall(CommandCall builtinStatement)
            => Write(builtinStatement, builtinStatement.Arguments);

        protected internal override void VisitFunctionDeclaration(FunctionDeclaration functionDeclaration)
            => Write(functionDeclaration, functionDeclaration.Parameters, functionDeclaration.Statements);

        protected internal override void VisitFunctionParameterDeclaration(FunctionParameterDeclaration parameterDeclaration) =>
            Write(parameterDeclaration, parameterDeclaration.DataType);

        protected internal override void VisitRoot(RootNode root)
            => Write(root, root.TopLevelNodes);
    }
}
