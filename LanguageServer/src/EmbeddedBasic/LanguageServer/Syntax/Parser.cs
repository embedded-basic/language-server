using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;
using EmbeddedBasic.LanguageServer.Syntax.Nodes;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Expressions;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Parselets;

namespace EmbeddedBasic.LanguageServer.Syntax
{
    public abstract class Parser
    {
        private SyntaxTree? _syntaxTree = null!;

        public SyntaxTree SyntaxTree
        {
            get => _syntaxTree!;
            set => TokenProvider.SyntaxTree = _syntaxTree = value;
        }

        protected ITokenProvider TokenProvider { get; }

        private readonly List<Token> _read = new();

        private readonly Dictionary<TokenKind, PrefixParselet> _prefixParselets =
            new Dictionary<TokenKind, PrefixParselet>();

        private readonly Dictionary<TokenKind, InfixParselet> _infixParselets =
            new Dictionary<TokenKind, InfixParselet>();

        protected Parser(ITokenProvider tokenProvider) => TokenProvider = tokenProvider;

        /// <summary>
        /// Is the appearance of the given token kind allowed?
        /// </summary>
        /// <param name="kind">The token kind to check for allowance.</param>
        /// <returns>Is the appearance of the given token allowed?</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected virtual bool IsAllowed(TokenKind kind) => true;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected void InsertToken(Token token)
        {
            if (IsAllowed(token.Kind)) _read.Add(token);
        }

        public void NoteError(string message)
            => SyntaxTree.AddErrorMessage(TokenProvider.Current, message);

        public void NoteError(string message, Token token)
            => SyntaxTree.AddErrorMessageFormat(token, message, token.GetLexemeDescription());

        /// <summary>
        /// Registers a prefix parselet.
        /// </summary>
        protected void Register(TokenKind token, PrefixParselet parselet) => _prefixParselets[token] = parselet;

        /// <summary>
        /// Registers an infix parselet.
        /// </summary>
        protected void Register(TokenKind token, InfixParselet parselet) => _infixParselets[token] = parselet;

        /// <summary>
        /// Registers literal parselets.
        /// </summary>
        protected void RegisterLiterals(params LiteralParselet.Info[] infos)
        {
            foreach (var info in infos)
            {
                var literalParselet = new LiteralParselet(this, info.Factory);
                foreach (var kind in info.Kinds)
                {
                    _prefixParselets[kind] = literalParselet;
                }
            }
        }

        /// <summary>
        /// Registers a postfix unary operator parselet for the given token and precedence.
        /// </summary>
        protected void RegisterPostfix<E>(E precedence, params TokenKind[] tokens) where E : Enum
        {
            var parselet = new PostfixOperatorParselet(this, Convert.ToInt32(precedence));
            foreach (var token in tokens) Register(token, parselet);
        }

        /// <summary>
        /// Registers a prefix unary operator parselet for the given token and precedence.
        /// </summary>
        protected void RegisterPrefix<E>(E precedence, params TokenKind[] tokens) where E : Enum
        {
            var parselet = new PrefixOperatorParselet(this, Convert.ToInt32(precedence));
            foreach (var token in tokens) Register(token, parselet);
        }

        /// <summary>
        /// Registers a left-associative binary operator parselet for the given token and precedence.
        /// </summary>
        protected void RegisterInfixLeft<E>(E precedence, params TokenKind[] tokens) where E : Enum
        {
            var parselet = new BinaryOperatorParselet(this, Convert.ToInt32(precedence), false);
            foreach (var token in tokens) Register(token, parselet);
        }

        /// <summary>
        /// Registers a right-associative binary operator parselet for the given token and precedence.
        /// </summary>
        protected void RegisterInfixRight<E>(E precedence, params TokenKind[] tokens) where E : Enum
        {
            var parselet = new BinaryOperatorParselet(this, Convert.ToInt32(precedence), true);
            foreach (var token in tokens) Register(token, parselet);
        }

        protected internal Expression? ParseExpression() => ParseExpression(0);

        protected internal bool ParseExpression([MaybeNullWhen(false)] out Expression expression)
        {
            expression = ParseExpression(0);
            return expression != null;
        }

        internal Expression? ParseExpression(int precedence)
        {
            var token = LookAhead();

            if (_prefixParselets.TryGetValue(token.Kind, out var prefix)) Consume();
            else NoteError(ErrorMessages.ExpressionStart, token);

            var left = prefix?.Parse(token);

            while (TryConsume(precedence, out token, out var infix)) left = infix.Parse(left, token);

            return left;
        }

        private bool TryConsume(int precedence, out Token token, [MaybeNullWhen(false)] out InfixParselet infix)
        {
            token = LookAhead();

            if (precedence < (_infixParselets.TryGetValue(token.Kind, out infix) ? infix.Precedence : 0))
            {
                Consume();
                if (infix != null) return true;
            }

            return false;
        }

        /// <summary>
        /// Consume the currently accumulated comment.
        /// </summary>
        /// <returns>The consumed comment.</returns>
        public string ConsumeComment() => TokenProvider.ConsumeComment();

        /// <summary>
        /// Look for an upcoming token.
        /// </summary>
        /// <param name="distance">The distance to look ahead.</param>
        /// <returns>The upcoming token.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected internal Token LookAhead(int distance = 0)
        {
            // Read in as many as needed.
            while (distance >= _read.Count)
            {
                TokenProvider.MoveNext();
                var token = TokenProvider.Current;
                if (IsAllowed(token.Kind)) _read.Add(token);
            }

            // Get the queued token.
            return _read[distance];
        }

        /// <summary>
        /// Try to consume an upcoming token matching the given kind.
        /// </summary>
        protected internal bool TryConsume(TokenKind kind)
        {
            if (LookAhead(0).Kind == kind)
            {
                _read.RemoveAt(0);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Try to consume an upcoming token matching the given kind.
        /// </summary>
        /// <param name="errorMessage">The error message in case the token couldn't be found.</param>
        /// <returns>Could the token be consumed?</returns>
        protected internal bool TryConsume(TokenKind kind, string errorMessage)
        {
            if (LookAhead(0).Kind == kind)
            {
                _read.RemoveAt(0);
                return true;
            }
            NoteError(errorMessage);
            return false;
        }

        /// <summary>
        /// Try to consume an upcoming token not matching the given kind.
        /// </summary>
        /// <param name="kind">The kind of token not to consume.</param>
        protected internal bool TryConsumeAnyBut(TokenKind kind)
        {
            if (LookAhead(0).Kind != kind)
            {
                _read.RemoveAt(0);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Try to consume an upcoming token not matching the given kind.
        /// </summary>
        /// <param name="kind1">The first kind of token not to consume.</param>
        /// <param name="kind2">The second kind of token not to consume.</param>
        protected internal bool TryConsumeAnyBut(TokenKind kind1, TokenKind kind2)
        {
            var token = LookAhead(0);
            if (token.Kind != kind1 && token.Kind != kind2)
            {
                _read.RemoveAt(0);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Try to consume an expected token.
        /// </summary>
        /// <param name="expected">The token that is expected.</param>
        /// <param name="token">The token that was expected.</param>
        /// <returns>Could the token be consumed?</returns>
        protected internal bool TryConsume(TokenKind expected, [MaybeNullWhen(false)] out Token token)
        {
            if ((token = LookAhead(0)).Kind == expected)
            {
                _read.RemoveAt(0);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Try to consume an expected token.
        /// </summary>
        /// <param name="expected">The token that is expected.</param>
        /// <param name="token">The token that was expected.</param>
        /// <param name="errorMessage">The error message in case the token couldn't be found.</param>
        /// <returns>Could the token be consumed?</returns>
        protected internal bool TryConsume(TokenKind expected, [MaybeNullWhen(false)] out Token token, string errorMessage)
        {
            if ((token = LookAhead(0)).Kind == expected)
            {
                _read.RemoveAt(0);
                return true;
            }

            NoteError(errorMessage, token);
            return false;
        }

        /// <summary>
        /// Try to consume an expected token.
        /// </summary>
        /// <param name="expected">The token that is expected.</param>
        /// <param name="token">The token that was expected.</param>
        /// <returns>Could the token be consumed?</returns>
        protected internal bool TryConsumeAnyBut(TokenKind expected, [MaybeNullWhen(false)] out Token token)
        {
            if ((token = LookAhead(0)).Kind == expected) return false;

            _read.RemoveAt(0);
            return true;
        }

        /// <summary>
        /// Consume the current token.
        /// </summary>
        /// <returns>The current token.</returns>
        protected Token Consume()
        {
            LookAhead(0);
            Token token = _read[0];
            _read.RemoveAt(0);
            return token;
        }

        /// <summary>
        /// Try to consume all upcoming tokens matching the given kind.
        /// </summary>
        protected internal void ConsumeWhile(TokenKind kind)
        {
            while (LookAhead(0).Kind == kind) _read.RemoveAt(0);
        }

        /// <summary>
        /// Consume all upcoming tokens until the given kind occurrs.
        /// </summary>
        /// <param name="kind">The kind of token</param>
        protected internal void ConsumeUntil(TokenKind kind)
        {
            while (LookAhead(0).Kind != kind) _read.RemoveAt(0);
        }

        /// <summary>
        /// Consume all upcoming tokens until the given kind occurrs.
        /// </summary>
        /// <param name="kind1">The first kind of token</param>
        /// <param name="kind2">The second kind of token</param>
        protected internal void ConsumeUntil(TokenKind kind1, TokenKind kind2)
        {
            while (true)
            {
                var token = LookAhead(0);
                if (token.Kind == kind1 || token.Kind == kind2) break;
                _read.RemoveAt(0);
            }
        }

        /// <summary>
        /// Consume all upcoming tokens until the given kind occurrs.
        /// </summary>
        /// <param name="kind">The kind of token</param>
        /// <param name="errorMessage">The error message in case the token couldn't be found.</param>
        protected internal void ConsumeUntil(TokenKind kind, string errorMessage)
        {
            Token? firstTokenRemoved = null;

            while (LookAhead(0).Kind != kind)
            {
                if (firstTokenRemoved == null) firstTokenRemoved = _read[0];
                _read.RemoveAt(0);
            }
            if (firstTokenRemoved != null) NoteError(errorMessage, firstTokenRemoved);
        }

        /// <summary>
        /// Consume all upcoming tokens until one the given kinds occurrs.
        /// </summary>
        /// <param name="kind1">The first kind of token.</param>
        /// <param name="kind2">The second kind of token.</param>
        /// <param name="errorMessage">The error message in case another token was found.</param>
        protected internal void ConsumeUntil(TokenKind kind1, TokenKind kind2, string errorMessage)
        {
            Token? firstTokenRemoved = null;

            var token = LookAhead(0);
            while (token.Kind != kind1 && token.Kind != kind2)
            {
                if (firstTokenRemoved == null) firstTokenRemoved = _read[0];
                _read.RemoveAt(0);
            }
            if (firstTokenRemoved != null) NoteError(errorMessage, firstTokenRemoved);
        }

        /// <summary>
        /// Consume all upcoming tokens until the given kind occurrs and including the token of that kind.
        /// </summary>
        /// <param name="kind">The kind of token</param>
        protected internal void ConsumeUntilIncluding(TokenKind kind)
        {
            while (LookAhead(0).Kind != kind) _read.RemoveAt(0);
            _read.RemoveAt(0);
        }
    }
}
