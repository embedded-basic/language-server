using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using EmbeddedBasic.LanguageServer.Syntax;
using EmbeddedBasic.LanguageServer.Syntax.Nodes;
using EmbeddedBasic.LanguageServer.Syntax.Nodes.Statements;
using EmbeddedBasic.LanguageServer.Semantics.Nodes;

namespace EmbeddedBasic.LanguageServer.Semantics
{
    public sealed partial class SemanticAnalyzer : SyntaxNodeVisitor
    {
        private readonly Stack<Scope> _scopes = new();

        public SemanticAnalyzer() { }

        /// <summary>
        /// Analyze a syntax tree to produce a semantic tree.
        /// </summary>
        /// <param name="syntaxTree">The syntax tree to analyze.</param>
        /// <returns>The produced semantic tree.</returns>
        public EbSemanticTree Analyze(EbSyntaxTree syntaxTree)
        {
            Module module = new(syntaxTree.SourceFile.FilePath, syntaxTree.Root);
            EbSemanticTree semanticTree = new(syntaxTree, module);
            _scopes.Push(module);
            this.Visit(syntaxTree.Root);
            _scopes.Clear();
            semanticTree.Finish();
            return semanticTree;
        }

        protected internal override void VisitVariableSubDeclaration(VariableSubDeclaration variableSubDeclaration)
            => _scopes.Peek().Add(new Variable(variableSubDeclaration.Name, variableSubDeclaration));

        protected internal override void VisitVariableArraySubDeclaration(VariableArraySubDeclaration variableArraySubDeclaration)
            => _scopes.Peek().Add(new Variable(variableArraySubDeclaration.Name, variableArraySubDeclaration));

        protected internal override void VisitConstantDeclaration(ConstantDeclaration constantDeclaration)
            => _scopes.Peek().Add(new Constant(constantDeclaration.Name, constantDeclaration));

        protected internal override void VisitFunctionDeclaration(FunctionDeclaration functionDeclaration)
        {
            var function = new Function(functionDeclaration.Name, functionDeclaration);
            _scopes.Peek().Add(function);
            _scopes.Push(function);
            this.VisitChilds(functionDeclaration);
            _scopes.Pop();
        }

        protected internal override void VisitFunctionParameterDeclaration(FunctionParameterDeclaration parameterDeclaration)
            => _scopes.Peek().Add(new FunctionParameter(parameterDeclaration.Name, parameterDeclaration));
    }
}
