using System;
using System.Collections.Generic;
using EmbeddedBasic.LanguageServer.Syntax.Nodes;

namespace EmbeddedBasic.LanguageServer.Semantics.Nodes
{
    /// <summary>
    /// A semantic scope.
    /// </summary>
    public abstract class Scope : SemanticNode
    {
        private List<Constant> _constants = new();

        public IReadOnlyList<Constant> Constants { get; }

        private List<Variable> _variables = new();

        public IReadOnlyList<Variable> Variables { get; }

        public Scope(string name, SyntaxNode syntaxNode) : base(name, syntaxNode)
        {
            Constants = _constants.AsReadOnly();
            Variables = _variables.AsReadOnly();
        }

        internal virtual void Add(Function node) => throw new InvalidOperationException();

        internal virtual void Add(FunctionParameter node) => throw new InvalidOperationException();

        internal void Add(Constant node)
        {
            _constants.Add(node);
            AddChild(node, ErrorMessages.IdentifierAlreadyDefined);
        }

        internal void Add(Variable node)
        {
            _variables.Add(node);
            AddChild(node, ErrorMessages.IdentifierAlreadyDefined);
        }
    }
}
