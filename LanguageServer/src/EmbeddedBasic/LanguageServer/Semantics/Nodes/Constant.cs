using EmbeddedBasic.LanguageServer.Syntax.Nodes.Statements;

namespace EmbeddedBasic.LanguageServer.Semantics.Nodes
{
    /// <summary>
    /// A constant.
    /// </summary>
    public sealed class Constant : SemanticNode
    {
        public ConstantDeclaration Declaration { get; }

        public Constant(string name, ConstantDeclaration syntaxNode) : base(name, syntaxNode)
        {
            Declaration = syntaxNode;
        }

        public override string ToString() => $"Constant `{Name}`";
    }
}
