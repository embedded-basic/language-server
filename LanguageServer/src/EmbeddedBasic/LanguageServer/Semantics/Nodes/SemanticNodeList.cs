using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using EmbeddedBasic.Util;

namespace EmbeddedBasic.LanguageServer.Semantics.Nodes
{
    public sealed class SemanticNodeList : SortedArrayList<SemanticNode>
    {
        public class ReadOnly : ReadOnlyCollection<SemanticNode>
        {
            public readonly SemanticNodeList _list;

            public ReadOnly(SemanticNodeList list) : base(list) { _list = list; }

            public bool TryGetValue(string name, [MaybeNullWhen(false)] out SemanticNode node)
                => _list.TryGetValue(name, out node);
        }

        private ReadOnly? _readOnly = null;

        public new ReadOnly AsReadOnly()
        {
            if (_readOnly != null) return _readOnly;
            return _readOnly = new(this);
        }

        public new SemanticNodeList Add(SemanticNode? node)
        {
            if (node != null) base.Add(node);
            return this;
        }

        public bool TryAdd(SemanticNode? node)
            => node != null && base.AddInternal(node) >= 0;

        public SemanticNodeList AddRange(IReadOnlyList<SemanticNode?>? nodes)
        {
            if (nodes != null && nodes?.Count > 0)
            {
                // We assure that the first non-null node is not yet in the list
                // and assume that covers the others too. No duplicates allowed!
                int index = 0;
                SemanticNode? node = null;

                while (index < nodes.Count)
                {
                    node = nodes[index++];
                    if (node != null) break;
                }

                if (node != null)
                {
                    base.Add(node);

                    while (index < nodes.Count)
                    {
                        node = nodes[index++];
                        if (node != null) base.Add(node);
                    }
                }
            }
            return this;
        }

        public bool TryGetValue(string name, [MaybeNullWhen(false)] out SemanticNode node)
            => _items.TryGetValue(_size, name, out node);
    }
}
