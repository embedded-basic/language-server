using EmbeddedBasic.LanguageServer.Syntax.Nodes;

namespace EmbeddedBasic.LanguageServer.Semantics.Nodes
{
    /// <summary>
    /// A function parameter.
    /// </summary>
    public sealed class FunctionParameter : SemanticNode
    {
        public FunctionParameterDeclaration Declaration { get; }

        public FunctionParameter(string name, FunctionParameterDeclaration syntaxNode) : base(name, syntaxNode)
        {
            Declaration = syntaxNode;
        }

        public override string ToString() => $"FunctionParameter `{Name}`";
    }
}
