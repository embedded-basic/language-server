using System.Collections.Generic;
using EmbeddedBasic.LanguageServer.Syntax.Nodes;

namespace EmbeddedBasic.LanguageServer.Semantics.Nodes
{
    public sealed class Module : Scope
    {
        public RootNode Declaration { get; }

        private List<Function> _functions = new();

        public IReadOnlyList<Function> Functions { get; }

        public Module(string name, RootNode syntaxNode) : base(name, syntaxNode)
        {
            Declaration = syntaxNode;
            Functions = _functions.AsReadOnly();
        }

        internal override void Add(Function node)
        {
            _functions.Add(node);
            AddChild(node, ErrorMessages.IdentifierAlreadyDefined);
        }

        public override string ToString() => $"Module `{Name}`";
    }
}
