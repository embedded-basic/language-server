using System;
using EmbeddedBasic.LanguageServer.Syntax.Nodes;

namespace EmbeddedBasic.LanguageServer.Semantics.Nodes
{
    public abstract class SemanticNode : IComparable<SemanticNode>, IComparable<string>
    {
        /// <summary>
        /// All child nodes.
        /// </summary>
        protected readonly SemanticNodeList _childNodes;

        /// <summary>
        /// All child nodes.
        /// </summary>
        public SemanticNodeList.ReadOnly ChildNodes => _childNodes.AsReadOnly();

        /// <summary>
        /// The name of this node.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// The syntax node for this semantic node.
        /// </summary>
        public SyntaxNode SyntaxNode { get; internal set; }

        /// <summary>
        /// The parent node for this node, or null.
        /// </summary>
        public SemanticNode? ParentNode { get; internal set; }

        public SemanticNode(string name, SyntaxNode syntaxNode)
        {
            _childNodes = new(); // Only scopes and derived are expected to have child nodes.
            Name = name;
            SyntaxNode = syntaxNode;
        }

        protected void AddChild(SemanticNode node, string errorMessage)
        {
            if (_childNodes.TryAdd(node)) node.ParentNode = this;
            else SyntaxNode.Tree.AddErrorMessage(node.SyntaxNode, errorMessage);
        }

        public int CompareTo(SemanticNode? other)
            => other != null ? Name.CompareTo(other.Name) : 1;

        public int CompareTo(string? other)
            => other != null ? Name.CompareTo(other) : 1;
    }
}
