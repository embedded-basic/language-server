using System.Collections.Generic;
using EmbeddedBasic.LanguageServer.Syntax.Nodes;

namespace EmbeddedBasic.LanguageServer.Semantics.Nodes
{
    /// <summary>
    /// A function.
    /// </summary>
    public sealed class Function : Scope
    {
        public FunctionDeclaration Declaration { get; }

        private List<FunctionParameter> _parameters = new();

        public IReadOnlyList<FunctionParameter> Parameters { get; }

        public Function(string name, FunctionDeclaration syntaxNode) : base(name, syntaxNode)
        {
            Declaration = syntaxNode;
            Parameters = _parameters.AsReadOnly();
        }

        internal override void Add(FunctionParameter node)
        {
            _parameters.Add(node);
            AddChild(node, ErrorMessages.IdentifierAlreadyDefined);
        }

        public override string ToString() => $"Function `{Name}`";
    }
}
