using EmbeddedBasic.LanguageServer.Syntax.Nodes.Statements;

namespace EmbeddedBasic.LanguageServer.Semantics.Nodes
{
    /// <summary>
    /// A variable.
    /// </summary>
    public sealed class Variable : SemanticNode
    {
        public VariableSubDeclaration Declaration { get; }

        public Variable(string name, VariableSubDeclaration syntaxNode) : base(name, syntaxNode)
        {
            Declaration = syntaxNode;
        }

        public override string ToString() => $"Variable `{Name}`";
    }
}
