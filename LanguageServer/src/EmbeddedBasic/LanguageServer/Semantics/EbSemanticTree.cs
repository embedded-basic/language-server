using EmbeddedBasic.LanguageServer.Syntax;
using EmbeddedBasic.LanguageServer.Semantics.Nodes;

namespace EmbeddedBasic.LanguageServer.Semantics
{
    /// <summary>
    /// An `Embedded BASIC` semantic tree.
    /// </summary>
    public sealed class EbSemanticTree : SemanticTree
    {
        public EbSemanticTree(EbSyntaxTree syntaxTree, Module module) : base(syntaxTree, module) { }
    }
}
