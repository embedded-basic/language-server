using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using EmbeddedBasic.LanguageServer.Syntax;
using EmbeddedBasic.LanguageServer.Syntax.Nodes;
using EmbeddedBasic.LanguageServer.Semantics.Nodes;

namespace EmbeddedBasic.LanguageServer.Semantics
{
    /// <summary>
    /// A semantic tree.
    /// </summary>
    public abstract class SemanticTree
    {
        public SyntaxTree SyntaxTree { get; }

        public Module Module { get; }

        /// <summary>
        /// All semantic nodes in this tree accessible by their syntax nodes.
        /// </summary>
        /// <remarks>
        /// For now it uses not only tokens but also complex syntax nodes.
        /// </remarks>
        private readonly Dictionary<SyntaxNode, SemanticNode> _nodeDictionary = new();

        public SemanticTree(SyntaxTree syntaxTree, Module module)
        {
            SyntaxTree = syntaxTree;
            Module = module;
        }

        internal void Finish() => Finish(Module);

        private void Finish(SemanticNode semanticNode)
        {
            // We fill the `_nodeDictionary` to make all semantic nodes searchable by their syntax nodes.

            foreach (var semanticChildNode in semanticNode.ChildNodes) Finish(semanticChildNode);

            var syntaxNodes = new Stack<SyntaxNode>();

            if (!_nodeDictionary.ContainsKey(semanticNode.SyntaxNode))
            {
                syntaxNodes.Push(semanticNode.SyntaxNode);

                while (syntaxNodes.TryPop(out var syntaxNode))
                {
                    _nodeDictionary.Add(syntaxNode, semanticNode);

                    foreach (var syntaxChildNode in syntaxNode.ChildNodes)
                    {
                        if (!_nodeDictionary.ContainsKey(syntaxChildNode)) syntaxNodes.Push(syntaxChildNode);
                    }
                }
            }
        }

        /// <summary>
        /// Try to get a semantic node by one its syntax nodes.
        /// </summary>
        /// <param name="syntaxNode">The syntax node for which to find the semantic node.</param>
        /// <param name="semanticNode">The semantic node that contains the given syntax node.</param>
        /// <returns>Could a matching semantic node be found?</returns>
        public bool TryGetNode(SyntaxNode syntaxNode, [MaybeNullWhen(false)] out SemanticNode semanticNode)
            => _nodeDictionary.TryGetValue(syntaxNode, out semanticNode);

        /// <summary>
        /// Try to get a declaration (semantic node) by a line position in the source code.
        /// </summary>
        /// <param name="linePosition">Line and position in the source code.</param>
        /// <param name="token">The token at the given position that lead to the declaration.</param>
        /// <param name="declaration">The semantic node that stands for the declaration that the token refers to.</param>
        /// <returns>Could a matching declaration be found?</returns>
        public bool TryGetDeclaration(
            LinePosition linePosition,
            [MaybeNullWhen(false)] out Token token,
            [MaybeNullWhen(false)] out SemanticNode declaration)
            => TryGetDeclaration(linePosition, out token, out declaration);

        /// <summary>
        /// Try to get a declaration (semantic node) by a line position in the source code.
        /// </summary>
        /// <param name="line">The number of the line in the source code.</param>
        /// <param name="character">The position on the line in the source code.</param>
        /// <param name="token">The token at the given position that lead to the declaration.</param>
        /// <param name="declaration">The semantic node that stands for the declaration that the token refers to.</param>
        /// <returns>Could a matching declaration be found?</returns>
        public bool TryGetDeclaration(
            int line,
            int character,
            [MaybeNullWhen(false)] out Token token,
            [MaybeNullWhen(false)] out SemanticNode declaration)
        {
            if (SyntaxTree.TryGetToken(line, character, out token) &&
                TryGetDeclaration(token, out declaration))
            {
                return true;
            }
            declaration = null;
            return false;
        }

        /// <summary>
        /// Try to get a declaration (semantic node) by a line position in the source code.
        /// </summary>
        /// <param name="line">The number of the line in the source code.</param>
        /// <param name="character">The position on the line in the source code.</param>
        /// <param name="declaration">The semantic node that stands for the declaration that the token refers to.</param>
        /// <returns>Could a matching declaration be found?</returns>
        public bool TryGetDeclaration(int line, int character, [MaybeNullWhen(false)] out SemanticNode declaration)
        {
            if (SyntaxTree.TryGetToken(line, character, out var token) &&
                TryGetDeclaration(token, out declaration))
            {
                return true;
            }
            declaration = null;
            return false;
        }

        /// <summary>
        /// Try to get a declaration (semantic node) by a line position in the source code.
        /// </summary>
        /// <param name="linePosition">Line and position in the source code.</param>
        /// <param name="declaration">The semantic node that stands for the declaration that the token refers to.</param>
        /// <returns>Could a matching declaration be found?</returns>
        public bool TryGetDeclaration(LinePosition linePosition, [MaybeNullWhen(false)] out SemanticNode declaration)
            => TryGetDeclaration(linePosition.Line, linePosition.Character, out declaration);

        /// <summary>
        /// Try to get a declaration (semantic node) by a referring token.
        /// </summary>
        /// <param name="token">The token for which to find the declaration.</param>
        /// <param name="declaration">The semantic node that stands for the declaration that the token refers to.</param>
        /// <returns>Could a matching declaration be found?</returns>
        public bool TryGetDeclaration(Token token, [MaybeNullWhen(false)] out SemanticNode declaration)
        {
            // For now only identifiers may be used.
            if (token.Kind == TokenKind.Identifier)
            {
                var name = token.Lexeme;
                SemanticNode? node;

                if (_nodeDictionary.TryGetValue(token.ParentNode!, out node))
                {
                    // We look through the scopes beginning by the one nearest to the given token.
                    for (; node != null; node = node.ParentNode)
                    {
                        if (node.ChildNodes.TryGetValue(name, out declaration)) return true;
                    }
                }
            }
            declaration = null;
            return false;
        }
    }
}
