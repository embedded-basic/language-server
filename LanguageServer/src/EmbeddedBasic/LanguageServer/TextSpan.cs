using System;

namespace EmbeddedBasic.LanguageServer
{
    public readonly struct TextSpan : IComparable<TextSpan>, IComparable<int>
    {
        /// <summary>
        /// The absolute start position of the text span inside the source text.
        /// </summary>
        public readonly int Start;

        /// <summary>
        /// The absolute end position of the text span inside the source text.
        /// </summary>
        public readonly int End;

        /// <summary>
        /// Constructs a copy of the given text position.
        /// </summary>
        /// <param name="line">The number of the line.</param>
        /// <param name="character">The number of the character on the line.</param>
        public TextSpan(int start, int end)
        {
            Start = start;
            End = end;
        }

        public TextSpan WithEnd(int end) => new TextSpan(Start, end);

        public int CompareTo(TextSpan other)
        {
            if (this.Start > other.End) return 1;
            if (this.End <= other.Start) return -1;
            // Overlapping of spans will count as equality.
            // That may not happen for tokens.
            return 0;
        }

        public int CompareTo(int other)
        {
            if (this.Start > other) return 1;
            if (this.End <= other) return -1;
            return 0;
        }
    }
}
