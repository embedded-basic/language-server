using EmbeddedBasic.LanguageServer.Syntax.Nodes;

namespace EmbeddedBasic.LanguageServer
{
    /// <summary>
    /// A error message.
    /// </summary>
    public readonly struct ErrorMessage
    {
        /// <summary>
        /// The node where the error occurred.
        /// </summary>
        public readonly string FilePath;

        /// <summary>
        /// The node where the error occurred.
        /// </summary>
        public readonly SyntaxNode? Node;

        /// <summary>
        /// The message.
        /// </summary>
        public readonly string Message;

        public ErrorMessage(SyntaxNode node, string message)
        {
            FilePath = node.FilePath;
            Node = node;
            Message = message;
        }

        public ErrorMessage(string filePath, string message)
        {
            FilePath = filePath;
            Node = null;
            Message = message;
        }

        public override string ToString()
            => (Node != null ? $"\"{Node.FileName}\" ({Node.ToRangeString()})" : "\"" + FilePath + "\"") +
                ": " + Message;
    }
}
