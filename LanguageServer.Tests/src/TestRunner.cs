// -----------------------------------------------------------------------------
// Copyright (c) .NET Foundation and Contributors
// All Rights Reserved

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------
// Base code from "https://github.com/xunit/samples.xunit/blob/main/TestRunner/Program.cs".
// -----------------------------------------------------------------------------

using System;
using System.Collections.Concurrent;
using System.Threading;
using Xunit.Runners;

namespace EmbeddedBasic.LanguageServer.Tests
{
    /// <summary>
    /// Separate entry point as helper for test debug with "netcoredbg".
    /// </summary>
    /// <remarks>
    /// Needed when using <a href="https://vscodium.com/">VS-Codium (OSS-version of VS-Code)</a> with the
    /// <a href="https://github.com/muhammadsammy/free-omnisharp-vscode">OSS-extension "muhammad-sammy.csharp"</a>,
    /// and the <a href="https://github.com/Samsung/netcoredbg">OSS-Debugger "netcoredbg"</a>.
    /// </remarks>
    static class TestRunner
    {
        // We use `_consoleLock` because messages can arrive in parallel,
        // so we want to make sure we get consistent console output.
        static object _consoleLock = new();

        // Use an event to know when we're done
        static ManualResetEvent _finished = new(false);

        // Start out assuming success; we'll set this to 1 if we get a failed test
        static int _result = 0;

        static ConcurrentQueue<string> _outputLines = new();

        static int Main(string[] args)
        {
            if (args.Length > 1)
            {
                Console.WriteLine("usage: <TestRunner> [typeName]");
                return 2;
            }

            var testAssembly = typeof(TestRunner).Assembly.Location;
            var typeName = args.Length == 1 ? args[0] : null;

            using (var runner = AssemblyRunner.WithoutAppDomain(testAssembly))
            {
                runner.OnDiscoveryComplete = OnDiscoveryComplete;
                runner.OnExecutionComplete = OnExecutionComplete;
                runner.OnTestFailed = OnTestFailed;
                runner.OnTestSkipped = OnTestSkipped;
                runner.OnTestOutput = OnTestOutput;

                runner.Start(typeName);

                _finished.WaitOne();
                _finished.Dispose();

                if (_outputLines.Count > 0)
                {
                    Console.WriteLine("--------------------<Collected test output below>--------------------");

                    foreach (var outputLine in _outputLines)
                    {
                        Console.Write(outputLine);
                    }
                }

                return _result;
            }
        }

        static void OnDiscoveryComplete(DiscoveryCompleteInfo info)
        {
            lock (_consoleLock)
            {
                Console.Write($"Running {info.TestCasesToRun} of {info.TestCasesDiscovered} tests...");
            }
        }

        static void OnExecutionComplete(ExecutionCompleteInfo info)
        {
            lock (_consoleLock)
            {
                var testWord = info.TotalTests == 0 ? "no test" : (info.TotalTests == 1 ? "test" : "tests");

                Console.ForegroundColor = info.TestsFailed == 0 ? ConsoleColor.Green : ConsoleColor.Red;
                Console.WriteLine($@" Finished: {info.TotalTests} {testWord} in {
                    Math.Round(info.ExecutionTime, 3)}s ({info.TestsFailed} failed, {info.TestsSkipped} skipped)");
                Console.ResetColor();
            }

            _finished.Set();
        }

        static void OnTestFailed(TestFailedInfo info)
        {
            lock (_consoleLock)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("[FAIL] {0}: {1}", info.TestDisplayName, info.ExceptionMessage);
                if (info.ExceptionStackTrace != null) Console.WriteLine(info.ExceptionStackTrace);
                Console.ResetColor();
            }

            _result = 1;
        }

        static void OnTestSkipped(TestSkippedInfo info)
        {
            lock (_consoleLock)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("[SKIP] {0}: {1}", info.TestDisplayName, info.SkipReason);
                Console.ResetColor();
            }
        }

        static void OnTestOutput(TestOutputInfo info) => _outputLines.Enqueue(info.Output);
    }
}
