using EmbeddedBasic.LanguageServer.Syntax;
using EmbeddedBasic.LanguageServer.Semantics;

namespace EmbeddedBasic.LanguageServer.Tests
{
    public sealed class ParserFixture
    {
        private readonly object _initLock = new();

        public readonly string InputDirpath = "../../../../src";

        public readonly string OutputDirpath = "../../../../log";

        public readonly string FileName = "test.bas";

        public readonly EbParser Parser = new();

        private EbSemanticTree? _semanticTree = null;

        private EbSyntaxTree? _syntaxTree = null;

        public EbSyntaxTree SyntaxTree
        {
            get
            {
                if (_syntaxTree == null) Init();
                return _syntaxTree!;
            }
        }

        public EbSemanticTree SemanticTree
        {
            get
            {
                if (_semanticTree == null) Init();
                return _semanticTree!;
            }
        }

        private void Init()
        {
            lock (_initLock)
            {
                if (_syntaxTree == null || _semanticTree == null)
                {
                    _syntaxTree = Parser.Parse(InputDirpath, FileName);
                    _semanticTree = new SemanticAnalyzer().Analyze(SyntaxTree);
                }
            }
        }
    }
}
