using System.Linq;
using Xunit;
using Xunit.Abstractions;
using EmbeddedBasic.LanguageServer.Syntax;

namespace EmbeddedBasic.LanguageServer.Tests
{
    [TestCaseOrderer("EmbeddedBasic.LanguageServer.Tests.MethodNameTestCaseOrderer", "LanguageServer.Tests")]
    public sealed class ParserTests : IClassFixture<ParserFixture>
    {
        private readonly ITestOutputHelper _output;

        private readonly ParserFixture _parserFixture;

        public ParserTests(ITestOutputHelper output, ParserFixture parserFixture)
        {
            _output = output;
            _parserFixture = parserFixture;
        }

        [Fact]
        public void Test1_Fixture()
        {
            Assert.NotNull(_parserFixture.SemanticTree);
        }

        [Theory]
        [InlineData((int)97, 24)]
        [InlineData((int)74, 18)]
        [InlineData((int)74, 32)]
        public void Test2_GotoDefinition(int line, int character)
        {
            if (_parserFixture.SemanticTree.TryGetDeclaration(line, character, out var token, out var node))
            {
                _output.WriteLine($"{token.ParentNode}\n\t{node.SyntaxNode}");
            }
            else Assert.False(true);
        }

        [Fact]
        public void Test3_ErrorLogWriting()
        {
            _parserFixture.SyntaxTree.WriteErrorLog(_parserFixture.OutputDirpath, _parserFixture.FileName + ".log");

            Assert.Equal(1, _parserFixture.SyntaxTree.ErrorMessagesByLocation.Count);
            var errorMessages = _parserFixture.SyntaxTree.ErrorMessagesByLocation.Values.First();
            Assert.Equal(1, errorMessages.Count);
            // Expected error: identifier already defined.
            Assert.Equal(new TextSpan(2916, 2917), errorMessages[0].Node!.TextSpan);
        }

        [Fact]
        public void Test4_CodeWriting()
        {
            new CodeWriter().WriteFile(
                _parserFixture.SyntaxTree,
                _parserFixture.OutputDirpath,
                fileName => fileName.Replace(".bas", ".code.bas"));
            // No assert, we just want to get the file for later inspection.
        }

        [Fact]
        public void Test5_TreeWriting()
        {
            new TreeListWriter().WriteFile(_parserFixture.SyntaxTree, _parserFixture.OutputDirpath);
            // No assert, we just want to get the file for later inspection.
        }
    }
}
